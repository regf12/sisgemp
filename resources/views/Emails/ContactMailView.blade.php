<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<style type="text/css">
		.img-chic{
			width: 100%;
		}
	</style>

	<header>
		
		<div class="row">
			<div class="col-sm-12">
				<div class="d-flex-justify-content-center align-items center">
					<img class="img-chic" src="{{ config('app.media_url').'/logo_sisgemp.png' }}" alt="">
				</div>
			</div>
		</div>
		
	</header>
	@if ($tipo == 'REQUEST')

    <div class="container mt-5">
			<div class="row">
				<div class="col-sm-6">
					<p class="text-justify">
						Ahora es parte de nuestra familia de emprendedores.
						Puede acceder a nuestro sitio a traves de la direccion: wwww.sisgemp.herokuapp.com
						utiliuzando el correo: <strong><u>{!!$mail!!}</u></strong>
						Clave: <strong><u>123456</u></strong>
					</p><br><br>		
				</div>
			</div>
		</div>

	@else

		<div class="container mt-5">
			<div class="row">
				<div class="col-sm-6">
					<p class="text-justify">
						Usted ha sido contactado por el usuario <strong><u>{!!$nombre!!}</u></strong> utiliuzando el correo <strong><u>{!!$mail!!}</u></strong>
					</p><br><br>		
				</div>
			</div>
		</div>

	@endif
		
	<div class="container mt-5">
		<span>Mensaje:</span><br><br>
		<p class="text-justiy">
			{!!$mensaje!!}
		</p>
	</div>

</body>
</html>