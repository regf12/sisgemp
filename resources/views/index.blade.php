@php
$config = [
    'appName' => config('app.name'),
    'locale' => $locale = app()->getLocale(),
    'locales' => config('app.locales'),
    'githubAuth' => config('services.github.client_id'),
];
@endphp

<script>
  window.Laravel = {!! json_encode([
    'csrfToken' => csrf_token(),
    'pusherKey' => config('broadcasting.connections.pusher.key'),
    'pusherCluster' => config('broadcasting.connections.pusher.options.cluster')
  ]) !!};
</script>

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>{{ config('app.name') }}</title>

  <link rel="shortcut icon" href="/favicon.ico" />
  
  <link rel="stylesheet" href="{{ mix('dist/css/app.css') }}">
</head>
<body>
  <div id="app"></div>

  {{-- Global configuration object --}}
  <script>
    window.config = @json($config);
  </script>

  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWMsxog2xVMxatgdNifeKbP0WbwhBEWe0&libraries=places"></script>

  {{-- Load the application scripts --}}
  <script src="{{ mix('dist/js/app.js') }}"></script>
</body>
</html>

<style>
  body{
    background-color: white;
    /*background-image: linear-gradient(to right bottom, #252524, #2c2c2a, #323330, #393a36, #3f413c);*/
    /*background-image: linear-gradient(15deg, #13547a 0%, #80d0c7 100%);*/
    /*background-image: linear-gradient(-225deg, #231557 0%, #44107A 29%, #FF1361 67%, #FFF800 100%);//INSTAGRAM
    */

    /*background-image: linear-gradient( 109.6deg,  rgba(0,0,0,1) 11.2%, rgba(247,30,30,1) 100.3% );*/
    /*background-color: #263238;*/
    /*background-color: #2979ff;*/
  /*background-image: url(../images/wood1Wallpaper.jpg);
  background-attachment: fixed;
  background-size: cover;
  background-repeat: no-repeat;*/
  /*background: rgb(27,35,42);
background: linear-gradient(90deg, rgba(27,35,42,1) 13%, rgba(81,135,142,1) 49%, rgba(68,84,87,1) 87%);*/
    /*background: #00c6ff;  
  background: -webkit-linear-gradient(to right, #0072ff, #00c6ff);  
  background: linear-gradient(to right, #0072ff, #00c6ff); */
  /*background: rgb(93,221,236);
  background: linear-gradient(90deg, rgba(93,221,236,1) 0%, rgba(93,221,236,1) 48%, rgba(17,127,149,1) 100%);*/
  }
</style>
