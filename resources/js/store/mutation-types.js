// auth.js
export const LOGOUT = 'LOGOUT';
export const SAVE_TOKEN = 'SAVE_TOKEN';
export const FETCH_USER = 'FETCH_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';

// lang.js
export const SET_LOCALE = 'SET_LOCALE';

// customs
export const UPDATE_LOADING = 'UPDATE_LOADING';

export const FETCH_DATA = 'FETCH_DATA';
export const FETCH_ITEM = 'FETCH_ITEM';
export const ADD_DATA = 'ADD_DATA';
export const UPDATE_DATA = 'UPDATE_DATA';
export const DELETE_DATA = 'DELETE_DATA';
/*export const FETCH_TYPE_OPERATIONS	= 'FETCH_TYPE_OPERATIONS';*/


