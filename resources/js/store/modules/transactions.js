// transacciones
import * as types from '../mutation-types';
import Cookies from 'js-cookie';
import axios from 'axios';

// state
export const state = {
  transaction: [],
  transactions: [],

  concepts: [],
}

// getters
export const getters = {
	transaction: state => state.transaction,
  transactions: state => state.transactions,
  concepts: state => state.concepts,
}

// mutations
export const mutations = {
  [types.FETCH_ITEM] (state, { transaction }) {
    state.transaction = transaction;
  },
  [types.FETCH_DATA] (state, { transactions }) {
    state.transactions = transactions;
  },
	FETCH_CONCEPTS (state, { concepts }) {
    state.concepts = concepts;
  },
  
}

// actions
export const actions = {
	async fetchTransactions ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/transactions`)
      .then(response =>{
        console.log('respuesta: ',response.data.data);
        commit(types.FETCH_DATA, { transactions: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchConcepts ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/concepts`)
      .then(response =>{
        console.log('respuesta: ',response.data.data);
        commit('FETCH_CONCEPTS', { concepts: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchTransaction ({ commit }, [idTransaction]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/transactions/${idTransaction}`)
      .then(response =>{
        console.log('respuesta: ',response.data.data);
        commit(types.FETCH_ITEM, { transaction: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async deleteTransaction ({ commit, dispatch }, [idTransaction]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.delete(`/api/transactions/${idTransaction}`)
      .then(response =>{
        dispatch(`fetchTransactions`);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async createTransaction ({ commit, dispatch }, [context,dataRequest]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.post(`/api/transactions`, dataRequest)
      .then(response =>{
        context.$success('Registro exitoso');
        context.$router.push({
          path: `/site/transactions` 
        });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },
  async updateTransaction ({ commit }, [context, data]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });
    
    await axios.put(`/api/transactions/${context.$route.params.id}`,data)
      .then(response =>{
        context.$success('Registro exitoso');
        context.$router.push({ 
          path: `/site/transactions` 
        });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

}


