// roles
import * as types from '../mutation-types';
import Cookies from 'js-cookie';
import axios from 'axios';

// state
export const state = {
  categories: [],
  items: [],
  itemsCategories: [],
}

// getters
export const getters = {
  categories: state => state.categories,
	items: state => state.items,
  itemsCategories: state => state.itemsCategories,
}

// mutations
export const mutations = {
	[types.FETCH_DATA] (state, { categories }) {
    state.categories = categories;
  },
  FETCH_ITEMS (state, { items }) {
    state.items = items;
  },
  FETCH_ITEMS_CATEGORIES (state, { itemsCategories }) {
    state.itemsCategories = itemsCategories;
  },
}

// actions
export const actions = {
	async fetchCategories ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get('/api/categories')
      .then(response =>{
      	console.log('respuesta: ',response.data);
        commit(types.FETCH_DATA, { categories: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchItems ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get('/api/items')
      .then(response =>{
        console.log('respuesta: ',response.data);
        commit('FETCH_ITEMS', { items: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchItemsCategories ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get('/api/items_categories')
      .then(response =>{
        console.log('respuesta: ',response.data);
        commit('FETCH_ITEMS_CATEGORIES', { itemsCategories: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  
}
