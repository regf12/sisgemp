// usuarios
import * as types from '../mutation-types';
import Cookies from 'js-cookie';
import axios from 'axios';

// state
export const state = {
  users: [],
  usersIds: [],
}

// getters
export const getters = {
	users: state => state.users,
	usersIds: state => state.usersIds,
}

// mutations
export const mutations = {
	[types.FETCH_DATA] (state, { users }) {
    state.users = users;
  },
  FETCH_USERS_ID (state, { usersIds }) {
    state.usersIds = usersIds;
  },
}

// actions
export const actions = {
	async fetchUsers ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get('/api/users')
      .then(response =>{
        commit(types.FETCH_DATA, { users: response.data.data });

        var ids = [];
      	for (var i = 0; i < response.data.data.length; i++) {
          if((response.data.data[i].id != 1) && (response.data.data[i].id != 2))
      		ids.push(response.data.data[i].id);
      	}

        commit('FETCH_USERS_ID', { usersIds: ids });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async deleteUser ({ commit, dispatch }, [idUser]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.delete(`/api/users/${idUser}`)
      .then(response =>{
        dispatch(`fetchUsers`);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },
}
