// emprendimientos
import * as types from '../mutation-types';
import Cookies from 'js-cookie';
import axios from 'axios';

// state
export const state = {
  group: [],
  groups: [],
}

// getters
export const getters = {
	group: state => state.group,
  groups: state => state.groups,
}

// mutations
export const mutations = {
  [types.FETCH_ITEM] (state, { group }) {
    state.group = group;
  },
	[types.FETCH_DATA] (state, { groups }) {
    state.groups = groups;
  },
  
}

// actions
export const actions = {
	async fetchGroups ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/groups`)
      .then(response =>{
        /*console.log('respuesta: ',response.data.data);*/
        commit(types.FETCH_DATA, { groups: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchGroup ({ commit }, [idGroup]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/groups/${idGroup}`)
      .then(response =>{
        /*console.log('respuesta: ',response.data.data);*/
        commit(types.FETCH_ITEM, { group: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },


}
