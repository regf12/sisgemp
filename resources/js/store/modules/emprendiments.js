// emprendimientos
import * as types from '../mutation-types';
import Cookies from 'js-cookie';
import axios from 'axios';

// state
export const state = {
  emprendiment: [],
  emprendiments: [],
  TotalDataEmprend: {},
  TotalDataGroup: {},
  categories: [],
}

// getters
export const getters = {
	emprendiment: state => state.emprendiment,
  emprendiments: state => state.emprendiments,
  TotalDataEmprend: state => state.TotalDataEmprend,
  TotalDataGroup: state => state.TotalDataGroup,
  categories: state => state.categories
}

// mutations
export const mutations = {
  [types.FETCH_ITEM] (state, { emprendiment }) {
    state.emprendiment = emprendiment;
  },
	[types.FETCH_DATA] (state, { emprendiments }) {
    state.emprendiments = emprendiments;
  },
  FETCH_TOTALEMPREND_DATA (state, { TotalDataEmprend }) {
    state.TotalDataEmprend = TotalDataEmprend;
  },
  FETCH_TOTALGROUP_DATA (state, { TotalDataGroup }) {
    state.TotalDataGroup = TotalDataGroup;
  },
  FETCH_CATEGORIES_DATA (state, { categories }) {
    state.categories = categories;
  },
  
}

// actions
export const actions = {
	async fetchEmprendiments ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/emprendiments`)
      .then(response =>{
        /*console.log('respuesta: ',response.data.data);*/
        commit(types.FETCH_DATA, { emprendiments: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchEmprendimentsEditEvent ({ commit },[id]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/emprendiments_edit_event/${id}`)
      .then(response =>{
        
        commit(types.FETCH_DATA, { emprendiments: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchStandEmprendiments ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/emprendiments/stand`)
      .then(response =>{
        console.log("STAND: ",response.data.data);
        commit(types.FETCH_DATA, { emprendiments: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

   async fetchCategories ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/emprendiments_categories`)
      .then(response =>{
        commit('FETCH_CATEGORIES_DATA', { categories: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchMyEmprendiment ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/my_emprend`)
      .then(response =>{
        /*console.log('respuesta: ',response.data.data);*/
        commit(types.FETCH_ITEM, { emprendiment: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchEmprendiment ({ commit }, [idEmprendiment]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/emprendiments/${idEmprendiment}`)
      .then(response =>{
        /*console.log('respuesta: ',response.data.data);*/
        commit(types.FETCH_ITEM, { emprendiment: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchEmprendimentPartners ({ commit }, id_emp) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });
    
    await axios.get(`/api/emprendiment_partners/${id_emp}`)
      .then(response =>{
        console.log('TOTALEMPREND: ',response.data.data);
        commit('FETCH_TOTALEMPREND_DATA', { TotalDataEmprend: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchGroupData ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });
    
    await axios.get(`/api/data_group`)
      .then(response =>{
        console.log("GROUP DATA: ", response.data.data);
        commit('FETCH_TOTALGROUP_DATA', { TotalDataGroup: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async deleteEmprendiment ({ commit, dispatch }, [idEmprendiment]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.delete(`/api/emprendiments/${idEmprendiment}`)
      .then(response =>{
        dispatch(`fetchEmprendiments`);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async suspend ({ commit, dispatch }, [context, idUser]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.put(`/api/suspend/${idUser}`)
      .then(response =>{
        dispatch(`fetchEmprendiments`);
        context.$success('Operacion exitosa!');
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async updateEmprendimentData({
    commit
  }, [context, data_emp, idEmp]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    await axios.put(`/api/emprendiments/${idEmp}`, data_emp)
      .then(response => {
        context.$success('Registro exitoso');
        context.$router.push({
          path: `/profile`
        });
      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },
}
