// productos
import * as types from '../mutation-types';
import Cookies from 'js-cookie';
import axios from 'axios';

// state
export const state = {
  historico_grupos: [],
  resumen_grupos: [],
  resumen_emprendimientos: [],
  resumen_eventos: [],
  resumen_transacciones: [],
  resumen_mensualidad: [],
  cantEmprendsCategories: [],
  AprovedRejected:[],
  CantProdCategories:[],
  ItemsGastos:[],
  EmprendimentAssistences:[],
}

// getters
export const getters = {
  historico_grupos: state => state.historico_grupos,
  resumen_grupos: state => state.resumen_grupos,
  resumen_emprendimientos: state => state.resumen_emprendimientos,
  resumen_eventos: state => state.resumen_eventos,
  resumen_transacciones: state => state.resumen_transacciones,
  resumen_mensualidad: state => state.resumen_mensualidad,
  cantEmprendsCategories: state => state.cantEmprendsCategories,
  AprovedRejected: state=> state.AprovedRejected,
  CantProdCategories: state=> state.CantProdCategories,
  ItemsGastos: state=> state.ItemsGastos,
  EmprendimentAssistences: state=> state.EmprendimentAssistences,
}

// mutations
export const mutations = {
  HISTORICO_GRUPO (state, { report }) {
    state.historico_grupos = report;
  },
  RESUMEN_GRUPO (state, { report }) {
    state.resumen_grupos = report;
  },
  RESUMEN_EMPRENDIMIENTOS (state, { report }) {
    state.resumen_emprendimientos = report;
  },
  RESUMEN_EVENTOS (state, { report }) {
    state.resumen_eventos = report;
  },
  RESUMEN_TRANSACCIONES (state, { report }) {
    state.resumen_transacciones = report;
  },
  RESUMEN_MENSUALIDAD (state, { report }) {
    state.resumen_mensualidad = report;
  },
  Cant_Emprends_Categories (state, { report }) {
    state.cantEmprendsCategories = report;
  },
  Aproved_Rejected (state, { report }) {
    state.AprovedRejected = report;
  },
  Cant_Prod_Categories (state, { report }) {
    state.CantProdCategories = report;
  },
  Cant_Items_Gastos (state, { report }) {
    state.ItemsGastos = report;
  },
  Emprends_Asistences (state, { report }) {
    state.EmprendimentAssistences = report;
  },
}

// actions
export const actions = {
	async fetchHistoricoGrupos ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/reports_historico_grupos`)
      .then(response =>{
        console.log('fetchHistoricoGrupos: ',response.data.data);
        commit('HISTORICO_GRUPO', { report: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchResumenGrupos ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/reports_resumen_grupos`)
      .then(response =>{
        console.log('fetchResumenGrupos: ',response.data.data);
        commit('RESUMEN_GRUPO', { report: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchResumenEmprendimientos ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/reports_resumen_emprendimientos`)
      .then(response =>{
        console.log('fetchResumenEmprendimientos: ',response.data.data);
        commit('RESUMEN_EMPRENDIMIENTOS', { report: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchResumenEventos ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/reports_resumen_eventos`)
      .then(response =>{
        console.log('fetchResumenEventos: ',response.data.data);
        commit('RESUMEN_EVENTOS', { report: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchResumenTransacciones ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/reports_resumen_transacciones`)
      .then(response =>{
        console.log('fetchResumenTransacciones: ',response.data.data);
        commit('RESUMEN_TRANSACCIONES', { report: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchResumenMensualidad ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/reports_resumen_mensualidad`)
      .then(response =>{
        console.log('fetchResumenMensualidad: ',response.data.data);
        commit('RESUMEN_MENSUALIDAD', { report: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchCantEmprendsCategories ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/reports_emprends_categories`)
      .then(response =>{
        console.log('Cantidad Emprends Categories: ',response.data.data);
        commit('Cant_Emprends_Categories', { report: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchPorcentAproved ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/reports_aproved_rejected`)
      .then(response =>{
        console.log('Aprobados y rechazados: ',response.data.data);
        commit('Aproved_Rejected', { report: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchCantProductsCategories ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/reports_products_categories`)
      .then(response =>{
        console.log('cantidad prod por categorias: ',response.data.data);
        commit('Cant_Prod_Categories', { report: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchItemsGastos ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/reports_items_gastos`)
      .then(response =>{
        console.log('Items gastos: ',response.data.data);
        commit('Cant_Items_Gastos', { report: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchEmprendsAssistences ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });
    console.log("ENTRO A LA CONSULTA");
    await axios.get(`/api/reports_emprendiment_assistences`)
      .then(response =>{
        console.log('Emprends_Asistences: ',response.data.data);
        commit('Emprends_Asistences', { report: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

}
