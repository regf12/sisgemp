// productos
import * as types from '../mutation-types';
import Cookies from 'js-cookie';
import axios from 'axios';

// state
export const state = {
  product: [],
  products: [],
}

// getters
export const getters = {
	product: state => state.product,
  products: state => state.products
  
}

// mutations
export const mutations = {
  [types.FETCH_ITEM] (state, { product }) {
    state.product = product;
  },
	[types.FETCH_DATA] (state, { products }) {
    state.products = products;
  }
}

// actions
export const actions = {
	async fetchStandProducts ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/products/stand`)
      .then(response =>{
        commit(types.FETCH_DATA, { products: response.data.data.reverse() });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchProducts ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/products`)
      .then(response =>{
        commit(types.FETCH_DATA, { products: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchProduct ({ commit }, [idProduct]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/products/${idProduct}`)
      .then(response =>{
        commit(types.FETCH_ITEM, { product: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async createProduct ({ commit }, [context,dataProduct]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

   await axios.post(`/api/products`, dataProduct)
      .then(response =>{
        console.log('respuesta: ',response.data.data);
        context.$success('Registro exitoso');
        context.$router.push({ 
          path: `/site/products` 
        });
        //dispatch(`fetchEvents`);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async updateProduct ({ commit }, [context,dataProduct]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });
    
   await axios.put(`/api/products/${context.$route.params.id}`, dataProduct)
      .then(response =>{
        console.log('respuesta: ',response.data.data);
        context.$success('Registro exitoso');
        context.$router.push({ 
          path: `/site/products` 
        });
        //dispatch(`fetchEvents`);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async changeStatus ({ commit }, [context,obj]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

   await axios.post(`/api/products/status_change`,obj)
      .then(response =>{
        
        context.$success('Registro exitoso');
        context.$router.push({ 
          path: `/site/products` 
        });
        //dispatch(`fetchEvents`);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },
  
  async deleteProduct ({ commit, dispatch }, [idProduct]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.delete(`/api/products/${idProduct}`)
      .then(response =>{
        dispatch(`fetchProducts`);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },
}
