// eventos
import * as types from '../mutation-types';
import Cookies from 'js-cookie';
import axios from 'axios';

// state
export const state = {

  //Single vars
  event: [],
  invitation: [],
  postulation: [],
  inscription: [],
  //Groups vars
  events: [],
  eventsStand: [],
  invitations: [],
  postulations: [],
  inscriptions: [],
  asistences: [],
  finalized_events: [],
  finalized_event: [],
  NextEventLanding: {},
  noInvitados: [],

}

// getters
export const getters = {

  //Single Consulte
  event: state => state.event,
  eventsStand: state => state.eventsStand,

  invitation: state => state.invitation,
  postulation: state => state.postulation,
  inscription: state => state.inscription,

  //Groups Consulte
  events: state => state.events,
  invitations: state => state.invitations,
  postulations: state => state.postulations,
  inscriptions: state => state.inscriptions,
  asistences: state => state.asistences,
  finalized_events: state => state.finalized_events,
  finalized_event: state => state.finalized_event,
  NextEventLanding: state => state.NextEventLanding,
  noInvitados: state => state.noInvitados,

}

// mutations
export const mutations = {
  [types.FETCH_ITEM](state, {
    event
  }) {
    state.event = event;
  },
  [types.FETCH_DATA](state, {
    events
  }) {
    state.events = events;
  },
  FETCH_ASISTENCES_DATA(state, {
    asistences
  }) {
    state.asistences = asistences;
  },
  FETCH_INSCRIPTIONS_DATA(state, {
    inscriptions
  }) {
    state.inscriptions = inscriptions;
  },
  FETCH_POSTULATIONS_DATA(state, {
    postulations
  }) {
    state.postulations = postulations;
  },
  FETCH_INSCRIPTION_DATA(state, {
    inscription
  }) {
    state.inscription = inscription;
  },
  FETCH_POSTULATION_DATA(state, {
    postulation
  }) {
    state.postulation = postulation;
  },
  FETCH_INVITATIONS_DATA(state, {
    invitations
  }) {
    state.invitations = invitations;
  },
  FETCH_INVITATION_DATA(state, {
    invitation
  }) {
    state.invitation = invitation;
  },
  FETCH_FINALIZED_DATA(state, {
    finalized_events
  }) {
    state.finalized_events = finalized_events;
  },
  FETCH_FINALIZEDEVENT_DATA(state, {
    finalized_event
  }) {
    state.finalized_event = finalized_event;
  },
  FETCH_EVENTSSTAND_DATA(state, {
    eventsStand
  }) {
    state.eventsStand = eventsStand;
  },
  FETCH_NEXTEVENT_DATA(state, {
    NextEventLanding
  }) {
    state.NextEventLanding = NextEventLanding;
  },

  FETCH_NoInvitados_DATA(state, {
    noInvitados
  }) {
    state.noInvitados = noInvitados;
  },
}

// actions
export const actions = {
  async fetchEvents({
    commit
  }) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    await axios.get(`/api/events`)
      .then(response => {
        commit(types.FETCH_DATA, {
          events: response.data.data
        });
      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },

  async fetchEventsStand({
    commit
  }) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    await axios.get(`/api/events/stand`)
      .then(response => {
        commit('FETCH_EVENTSSTAND_DATA', {
          eventsStand: response.data.data
        });
      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },

  async fetchEvent({
    commit
  }, [idEvent]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    await axios.get(`/api/events/${idEvent}`)
      .then(response => {
        commit(types.FETCH_ITEM, {
          event: response.data.data
        });
      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },

  async fetchFinalizedEvents({
    commit
  }) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    await axios.get(`/api/events_finalized`)
      .then(response => {
        /*console.log('fetchFinalizedEvents: ', response.data.data);*/
        commit('FETCH_FINALIZED_DATA', {
          finalized_events: response.data.data
        });
      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },

  async fetchFinalizedEvent({
    commit
  }, [id]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    await axios.get(`/api/events_finalized/${id}`)
      .then(response => {
        commit('FETCH_FINALIZEDEVENT_DATA', {
          finalized_event: response.data.data
        });
      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },

  async updateEvent({
    commit
  }, [context, UpEvent, idEvent]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    await axios.put(`/api/events/${idEvent}`, UpEvent)
      .then(response => {
        context.$success('Registro exitoso');
        context.$router.push({
          path: `/site/events`
        });
      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },

  async deleteEvent({
    commit,
    dispatch
  }, [idEvent]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    await axios.delete(`/api/events/${idEvent}`)
      .then(response => {
        dispatch(`fetchEvents`);
      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },

  async createEvent({
    commit,
    dispatch
  }, [context, dataEvent]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    await axios.post(`/api/events`, dataEvent)
      .then(response => {
        /*console.log('respuesta: ',response.data.data);*/
        context.$success('Registro exitoso');
        context.$router.push({
          path: `/site/events`
        });
        //dispatch(`fetchEvents`);
      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },

  async createInscriptions({
    commit,
    dispatch
  }, [context, Inscriptions, costo, id_event]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    let Data = {
      arrayIds: Inscriptions,
      costoEvent: costo,
      event: id_event
    }; //AJURO DEBE IR ASI
    console.log(Data);
    await axios.post(`/api/events_inscriptions`, Data)
      .then(response => {

        context.$success('Registro exitoso');
        context.$router.push({
          path: `/site/events/view/${context.$route.params.id}`
        });

      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },

  async createInscription({
    commit,
    dispatch
  }, [context, idInscription, costo, id_event]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    let Data = {
      arrayIds: idInscription,
      costoEvent: costo,
      event: id_event
    }; //AJURO DEBE IR ASI
    console.log(Data);
    await axios.post(`/api/events_inscription`, Data)
      .then(response => {

        context.$success('Registro exitoso');
        context.$router.push({
          path: `/site/events/view/${context.$route.params.id}`
        });

      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },

  async createAsistences({
    commit,
    dispatch
  }, [context, Asistences]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    /*let Data={arrayIds:Asistences}; ESTO ES CON EL CHECKBOX*/
    let Data = {
      arrayCodes: Asistences
    };
    await axios.post(`/api/events_asistences`, Data)
      .then(response => {

        context.$success('Registro exitoso');
        context.$router.push({
          path: `/site/events/view/${context.$route.params.id}`
        });
        context.createAsistences = [];
      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },

  async createAsistecia({
    commit,
    dispatch
  }, [context, Asistenc]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    let Data = {
      arrayIds: Asistences
    }; //AJURO DEBE IR ASI
    await axios.post(`/api/events_asistencia`, Asistenc)
      .then(response => {

        context.$success('Registro exitoso');
        context.$router.push({
          path: `/site/events/view/${context.$route.params.id}`
        });

      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },


  async fetchAsistences({
    commit,
    dispatch
  }, [context, id_event]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });


    await axios.get(`/api/events_asistences/${id_event}`)
      .then(response => {
        commit('FETCH_ASISTENCES_DATA', {
          asistences: response.data.data
        });
        /*console.log("asistences: ", state.asistences)*/
        /*for(let i=0; i<response.data.data.length;i++){
          context.idsInvitados.push(response.data.data[i].id_invitation);
        }*/

      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },

  async fetchInvitations({
    commit,
    dispatch
  }, [context, id_event]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });


    await axios.get(`/api/events_invitations/${id_event}`)
      .then(response => {
        commit('FETCH_INVITATIONS_DATA', {
          invitations: response.data.data
        });
        /*console.log("Invitations: ", state.invitations)*/
        for (let i = 0; i < response.data.data.length; i++) {
          context.idsInvitados.push(response.data.data[i].id_invitation);
        }

      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },

  async fetchInscriptions({
    commit,
    dispatch
  }, [context, id_event]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });


    await axios.get(`/api/events_inscriptions/${id_event}`)
      .then(response => {
        commit('FETCH_INSCRIPTIONS_DATA', {
          inscriptions: response.data.data
        });
        /*console.log("Inscriptions: ", state.inscriptions)*/
        for (let i = 0; i < response.data.data.length; i++) {
          context.idsInscritos.push(response.data.data[i].id_inscription);
        }

      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },

  async createPostulation({
    commit,
    dispatch
  }, [context, Postulation]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    /*console.log("Datos action",Postulation);*/
    await axios.post(`/api/events_postulation`, Postulation)
      .then(response => {
        /*console.log('respuesta: ',response.data.data);*/
        context.$success('Registro exitoso');
        context.ButtonPostulateEmprend = 'success';
        context.ButtonPostulateStatus = "pre-registered1";
        context.StatusPostulate = true;
        //context.$alert('Su codigo de inscripcion es: '+response.data.data);
        context.$router.push({
          path: `/site/events/view/${Postulation.id_event}`
        });

      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },

  async fetchPostulations({
    commit,
    dispatch
  }, [context, id_event]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    await axios.get(`/api/postulations/${id_event}`)
      .then(response => {
        /*console.log("Datos Postulacion: ", response.data.data);*/
        for (let i = 0; i < response.data.data.length; i++) {
          context.idsPostulados.push(response.data.data[i].id_postulation);
        }
        commit('FETCH_POSTULATIONS_DATA', {
          postulations: response.data.data
        });
      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });

  },

  async fetchPostulation({
    commit,
    dispatch
  }, [context, id_event]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    await axios.get(`/api/postulation/${id_event}`)
      .then(response => {
        /*console.log("Datos Postulacion: ",response.data.data);*/
        commit('FETCH_POSTULATION_DATA', {
          postulation: response.data.data
        });
        if (response.data.data.length == 1) {
          context.ButtonPostulateEmprend = 'success'
          context.ButtonPostulateStatus = "postulated";
          context.StatusPostulate = true;
        }

      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });

  },

  async fetchNextEvent({
    commit
  }) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    await axios.get(`/api/events/next_event`)
      .then(response => {
        commit('FETCH_NEXTEVENT_DATA', {
          NextEventLanding: response.data.data
        });
        console.log("next: ", response.data.data);
      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },


  async fetchNoInvitados({
    commit
  }, [id_event]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });

    await axios.get(`/api/events_noInvited/${id_event}`)
      .then(response => {
        commit('FETCH_NoInvitados_DATA', {
          noInvitados: response.data.data
        });
        console.log("next: ", response.data.data);
      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },

  async restInvitations({
    commit,
    dispatch
  }, [context, ids, id]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });
    let dataEvent = {
      idsInv: ids,
      id_event: id
    };
    await axios.post(`/api/rest_invitations`, dataEvent)
      .then(response => {
        /*console.log('respuesta: ',response.data.data);*/
        context.$success('Registro exitoso');
        context.$router.push({
          path: `/site/events/view/${id}`
        });
        context.modelForm.NoInvitados = [];
        //dispatch(`fetchEvents`);
      })
      .catch((e) => {
        console.error('error: ', e)
      })

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    })
  },

  async unRegister({
    commit,
    dispatch
  }, [context, idEvent]) {
    commit(`loading/${types.UPDATE_LOADING}`, {
      status: true
    }, {
      root: true
    });
    let dataEvent = {
      id_event: idEvent
    };
    await axios.post(`/api/un_register_event`, dataEvent)
      .then(response => {
        /*console.log('respuesta: ',response.data.data);*/
        context.$success('Operación exitosa');
        context.Binscrito=false;
        context.$router.push({
          path: `/site/events/view/${idEvent}`
        });
        //context.modelForm.NoInvitados=[];
        //dispatch(`fetchEvents`);
      })
      .catch((e) => {
        console.error('error: ', e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, {
      status: false
    }, {
      root: true
    });
  },
}