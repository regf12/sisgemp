// encuestas
import * as types from '../mutation-types';
import Cookies from 'js-cookie';
import axios from 'axios';

// state
export const state = {
  poll: [],
  polls: [],
  poll_result: {},
}

// getters
export const getters = {
	poll: state => state.poll,
  polls: state => state.polls,
  poll_result: state => state.poll_result,
}

// mutations
export const mutations = {
  [types.FETCH_ITEM] (state, { poll }) {
    state.poll = poll;
  },
	[types.FETCH_DATA] (state, { polls }) {
    state.polls = polls;
  },
  FETCH_POLL_RESULTS_DATA (state, { poll_result }) {
    state.poll_result = poll_result;
  },
  
}

// actions
export const actions = {
	async fetchPolls ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/polls`)
      .then(response =>{
        console.log('respuesta: ',response.data.data);
        commit(types.FETCH_DATA, { polls: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchPoll ({ commit }, [idPoll]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/polls/${idPoll}`)
      .then(response =>{
        console.log('respuesta: ',response.data.data);
        commit(types.FETCH_ITEM, { poll: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchPollResult ({ commit }, [context,idPoll]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/polls_results/${idPoll}`)
      .then(response =>{
        console.log('resultados poll: ',response.data.data);
        commit('FETCH_POLL_RESULTS_DATA', { poll_result: response.data.data });
        
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async deletePoll ({ commit, dispatch }, [idPoll]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.delete(`/api/polls/${idPoll}`)
      .then(response =>{
        dispatch(`fetchPolls`);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async createPoll ({ commit, dispatch }, [context,dataPoll]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.post(`/api/polls`, dataPoll)
      .then(response =>{
        context.$success('Registro exitoso');
        context.$router.push({ 
          path: `/site/polls` 
        });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async responsePoll ({ commit, dispatch }, [context,dataPoll]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.post(`/api/poll_response`, dataPoll)
      .then(response =>{
        context.$success('Registro exitoso');
        context.$router.push({ 
          path: `/site/polls` 
        });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },
}
