// emprendimientos
import * as types from '../mutation-types';
import Cookies from 'js-cookie';
import axios from 'axios';

// state
export const state = {
  image: {},
  images: [],
}

// getters
export const getters = {
	image: state => state.image,
  images: state => state.images,
}

// mutations
export const mutations = {
  [types.FETCH_ITEM] (state, { image }) {
    state.image = image;
  },
	[types.FETCH_DATA] (state, { images }) {
    state.images = images;
  },
  
}

// actions
export const actions = {
	async updateImageProfile ({ commit }, [context,urlImage]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.post(`/api/image/upload_emprendimiento`,{urlImage:urlImage})
      .then(response =>{
        console.log('updateImageProfile: ',response.data.data);
        context.$success(i18n.t('upload_success') );
        //commit(types.FETCH_DATA, { emprendiments: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

	async updateImageGallery ({ commit }, [context,data]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.post(`/api/image/upload_galery`, data)
      .then(response =>{
        console.log('updateImageProfile: ',response.data.data);
        context.$success(i18n.t('upload_success') );
        //commit(types.FETCH_DATA, { emprendiments: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async updateImageBanner ({ commit }, [context,urlImage]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.post(`/api/image/upload_banner`,{urlImage:urlImage})
      .then(response =>{
        console.log('updateImageBanner: ',response.data.data);
        context.$success(i18n.t('upload_success') );
        //commit(types.FETCH_DATA, { emprendiments: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchImages ({ commit }, [type]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });
    
    await axios.get(`/api/get_images/${'GALLERY'}`)
      .then(response =>{
        console.log('fetchImages: ',response.data.data);
        commit(types.FETCH_DATA, { images: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

   
}
