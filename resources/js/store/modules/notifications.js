// notificaciones
import * as types from '../mutation-types';
import Cookies from 'js-cookie';
import axios from 'axios';

// state
export const state = {
  notifications: [],
  countNotify: 0
}

// getters
export const getters = {
  notifications: state => state.notifications,
  countNotify: state => state.countNotify,
}

// mutations
export const mutations = {
  [types.FETCH_ITEM] (state, { countNotify }) {
    state.countNotify = countNotify;
  },
	[types.FETCH_DATA] (state, { notifications }) {
    state.notifications = notifications;
  },
  
}

// actions
export const actions = {
	async fetchNotifications ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/notifications`)
      .then(response =>{
        commit(types.FETCH_DATA, { notifications: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchCountNotifications ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/count_notifications`)
      .then(response =>{
        commit(types.FETCH_ITEM, { countNotify: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async viewNotification ({ commit }, [idNotification]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.put(`/api/notifications_visto/${idNotification}`)
      .then(response =>{
        console.log('respuesta: ',response.data.data);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  /*
  async fetchNotification ({ commit }, [idNotification]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/notifications/${idNotification}`)
      .then(response =>{
        commit(types.FETCH_ITEM, { notification: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },
  */

  /*async deleteNotification ({ commit, dispatch }, [idNotification]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.delete(`/api/notifications/${idNotification}`)
      .then(response =>{
        dispatch(`fetchNotifications`);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },*/
}
