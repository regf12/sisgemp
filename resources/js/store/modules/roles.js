// roles
import * as types from '../mutation-types';
import Cookies from 'js-cookie';
import axios from 'axios';

// state
export const state = {
  roles: [],
  permissions: {},
  permissionsRole: {},
}

// getters
export const getters = {
	roles: state => state.roles,
  permissions: state => state.permissions,
  permissionsRole: state => state.permissionsRole,
}

// mutations
export const mutations = {
	[types.FETCH_DATA] (state, { roles }) {
    state.roles = roles;
  },
  FETCH_PERMISSIONS (state, { permissions }) {
    state.permissions = permissions;
  },
  FETCH_PERMISSIONS_ROLE (state, { permissions }) {
    state.permissionsRole = permissions;
  },
}

// actions
export const actions = {
	async fetchRoles ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get('/api/roles')
      .then(response =>{
      	console.log('respuesta: ',response.data);
        commit(types.FETCH_DATA, { roles: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchPermissions ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get('/api/permissions')
      .then(response =>{
        console.log('respuesta: ',response.data);
        commit('FETCH_PERMISSIONS', { permissions: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async deleteRole ({ commit, dispatch }, [idRole]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.delete(`/api/roles/${idRole}`)
      .then(response =>{
        dispatch(`fetchRoles`);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchPermissionsRole ({ commit, dispatch }, [idRole]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/roles/${idRole}`)
      .then(response =>{
        console.log('respuesta: ',response.data)
        commit('FETCH_PERMISSIONS_ROLE', { permissions:response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },
}
