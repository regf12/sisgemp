// mensajes
import * as types from '../mutation-types';

// state
export const state = {
	messages: [],
}

// getters
export const getters = {
	messages: 		state => state.messages,
}

// mutations
export const mutations = {
	FETCH_MESSAGES (state, { messages }) {
    state.messages = messages;
  },
}

// actions
export const actions = {
	async fetchMessages ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get('/api/messages')
      .then(response =>{
        console.log(response.data);
        commit('FETCH_MESSAGES', { messages: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async sendMessage ({ commit, dispatch }, [message]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.post(`/api/messages`, message)
      .then(response =>{
        dispatch(`fetchMessages`);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async deleteMessage ({ commit, dispatch }, [idMessage]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.delete(`/api/messages/${idMessage}`)
      .then(response =>{
        dispatch(`fetchMessages`);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async sendMailContact ({ commit}, [context,message]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.post(`/api/mail`, message)
      .then(response =>{
        context.$success('Registro exitoso');
        /*dispatch(`fetchMessages`);*/
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },
}
