// solicitudes
import * as types from '../mutation-types';
import Cookies from 'js-cookie';
import axios from 'axios';

// state
export const state = {
  request: [],
  requests: [],
}

// getters
export const getters = {
	request: state => state.request,
  requests: state => state.requests,
}

// mutations
export const mutations = {
  [types.FETCH_ITEM] (state, { request }) {
    state.request = request;
  },
	[types.FETCH_DATA] (state, { requests }) {
    state.requests = requests;
  },

}

// actions
export const actions = {
	async fetchRequests ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/requests`)
      .then(response =>{
        commit(types.FETCH_DATA, { requests: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchRequest ({ commit }, [idRequest]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/requests/${idRequest}`)
      .then(response =>{
        commit(types.FETCH_ITEM, { request: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async deleteRequest ({ commit, dispatch }, [idRequest]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.delete(`/api/requests/${idRequest}`)
      .then(response =>{
        dispatch(`fetchRequests`);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async createRequest ({ commit, dispatch }, [context,dataRequest]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.post(`/api/requests`, dataRequest)
      .then(response =>{
        console.log('respuesta: ',response.data.data);
        context.$success('Registro exitoso');
        context.$router.push({
          path: `/site/requests` 
        });
        //dispatch(`fetchRequests`);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async evaluateRequest ({ commit, dispatch }, [context,dataRequest]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.post(`/api/requests_evaluate`, dataRequest)
      .then(response =>{
        console.log('respuesta: ',response.data.data);
        context.$success('Proceso exitoso');
        
        context.$router.push({
          path: `/site/quotes` 
        });

        //dispatch(`fetchRequests`);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },
}
