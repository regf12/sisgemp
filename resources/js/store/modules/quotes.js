// citas
import * as types from '../mutation-types';
import Cookies from 'js-cookie';
import axios from 'axios';

// state
export const state = {
  quote: [],
  quotes: [],
}

// getters
export const getters = {
	quote: state => state.quote,
  quotes: state => state.quotes,
}

// mutations
export const mutations = {
  [types.FETCH_ITEM] (state, { quote }) {
    state.quote = quote;
  },
	[types.FETCH_DATA] (state, { quotes }) {
    state.quotes = quotes;
  },
  
}

// actions
export const actions = {
	async fetchQuotes ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/quotes`)
      .then(response =>{
        console.log('respuesta: ',response.data.data);
        commit(types.FETCH_DATA, { quotes: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchQuote ({ commit }, [idQuote]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/quotes/${idQuote}`)
      .then(response =>{
        console.log('respuesta: ',response.data.data);
        commit(types.FETCH_ITEM, { quote: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchQuoteEdit ({ commit }, [idQuote]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/quotes_edit/${idQuote}`)
      .then(response =>{
        console.log('respuesta: ',response.data.data);
        commit(types.FETCH_ITEM, { quote: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async deleteQuote ({ commit, dispatch }, [idQuote]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.delete(`/api/quotes/${idQuote}`)
      .then(response =>{
        dispatch(`fetchQuotes`);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async createQuote ({ commit, dispatch }, [context,data]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.post(`/api/quotes`,data)
      .then(response =>{
        /*dispatch(`fetchQuotes`);*/

        context.$router.push({
          path: `/site/quotes` 
        });
        
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async editQuote ({ commit, dispatch }, [context,data,idQuote]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });
    
    await axios.put(`/api/quotes/${idQuote}`,data)
      .then(response =>{
        /*dispatch(`fetchQuotes`);*/
        context.$success('Registro exitoso');
        context.$router.push({
          path: `/site/quotes` 
        });
        
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },
}
