import * as types from '../mutation-types';

// state
export const state = {
	menu_index: [{
			icon: 'home',
			name: 'profile',
			route: 'home'
		},
		{
			icon: 'globe',
			name: 'gestion',
			route: 'site'
		},
		{
			icon: 'user',
			name: 'settings',
			route: 'profile'
		},
	],

	/// //////////////////////////////////////////////////////////////

	tabs_site: [

		{
			icon: 'solicitudes.png',
			name: 'requests',
			route: 'site.requests',
			role: ['super', 'admin']
		},
		{
			icon: 'citas.png',
			name: 'quotes',
			route: 'site.quotes',
			role: ['super', 'admin']
		},
		{
			icon: 'eventos.png',
			name: 'events',
			route: 'site.events',
			role: ['super', 'admin', 'emprend']
		},
		{
			icon: 'emprendimientos.png',
			name: 'emprendiments',
			route: 'site.emprendiments',
			role: ['super', 'admin']
		},
		{
			icon: 'productos.png',
			name: 'products',
			route: 'site.products',
			role: ['super', 'admin', 'emprend']
		},
		{
			icon: 'move.png',
			name: 'economics_movs',
			route: 'site.transactions',
			role: ['super', 'admin', 'emprend']
		},

		{
			icon: 'encuestas.png',
			name: 'polls',
			route: 'site.polls',
			role: ['super', 'admin', 'emprend']
		},

		{
			icon: 'reportes.png',
			name: 'reports',
			route: 'site.reports',
			role: ['super', 'admin']
		},

		{
			icon: 'cog',
			name: 'roles',
			route: 'site.roles',
			role: ['super']
		},
		{
			icon: 'users',
			name: 'users',
			route: 'site.users',
			role: ['super']
		}
	],

	tabs_site_report: [
		/* {
			icon: 'cog',
			name: 'roles',
			route: 'site.reports.roles'
		},
		{
			icon: 'cog',
			name: 'users',
			route: 'site.reports.users'
		}, */
		{
			icon: 'cog',
			name: 'historico_grupos',
			route: 'site.reports.historico_grupos',
			role: ['super', 'admin']
		},
		{
			icon: 'cog',
			name: 'resumen_emprendimientos',
			route: 'site.reports.resumen_emprendimientos',
			role: ['super', 'admin']
		}
		/* { icon: 'cog',
		name: 'resumen_grupos',
		route: 'site.reports.resumen_grupos',
		role: ['super','admin'],
		}, */
		/* { icon: 'cog',
			name: 'resumen_eventos',
			route: 'site.reports.resumen_eventos',
			role: ['super','admin'],
		},
		{ icon: 'cog',
			name: 'resumen_transacciones',
			route: 'site.reports.resumen_transacciones',
			role: ['super','admin','emprend'],
		},
		{ icon: 'cog',
			name: 'resumen_mensualidad',
			route: 'site.reports.resumen_mensualidad',
			role: ['super','admin'],
		}, */
	],

	// //////////////////////////////////////////////////////////////

	tabs_profile: [
		/* { icon: 'user',
		name: 'personal_data',
		route: 'profile.personal'
		}, */
		/* { icon: 'id-card-alt',
		name: 'emprendiment_data',
		route: 'profile.emprendiment',
		role: ['emprend'],
		},
		{ icon: 'id-card-alt',
		name: 'group_data',
		route: 'profile.group',
		role: ['super','admin'],
		},
		{ icon: 'users',
		name: 'parterns',
		route: 'profile.parterns',
		role: ['emprend'],
		}, */
		{
			icon: 'contenido.png',
			name: 'content_data',
			route: 'profile.content',
			role: ['super', 'admin']
		},

		{
			icon: 'email.png',
			name: 'email',
			route: 'profile.email',
			role: ['super', 'admin', 'emprend']
		},
		{
			icon: 'contraseña.png',
			name: 'password',
			route: 'profile.password',
			role: ['super', 'admin', 'emprend']
		},
		{
			icon: 'profile_edit.png',
			name: 'emprendiment',
			route: 'profile.emprendiment',
			role: ['emprend']
		},
		{
			icon: 'profile_edit.png',
			name: 'group',
			route: 'profile.group',
			role: ['admin']
		},
	],
}

// getters
export const getters = {
	menu_index: state => state.menu_index,
	tabs_site: state => state.tabs_site,
	tabs_site_report: state => state.tabs_site_report,
	tabs_profile: state => state.tabs_profile
}

// mutations
export const mutations = {

}

// actions
export const actions = {

}