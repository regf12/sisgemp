// mensualidades
import * as types from '../mutation-types';
import Cookies from 'js-cookie';
import axios from 'axios';

// state
export const state = {
  payment: [],
  payments: [],
  meses_pagos:[],
}

// getters
export const getters = {
	payment: state => state.payment,
  payments: state => state.payments,
  meses_pagos: state => state.meses_pagos,
}

// mutations
export const mutations = {
  [types.FETCH_ITEM] (state, { payment }) {
    state.payment = payment;
  },
	[types.FETCH_DATA] (state, { payments }) {
    state.payments = payments;
  },
   FETCH_MESESPAGOS(state, { meses_pagos }) {
    state.meses_pagos = meses_pagos;
  },
}

// actions
export const actions = {
	async fetchPayments ({ commit }) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/payments`)
      .then(response =>{
            
        commit(types.FETCH_DATA, { payments: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchMesesPagos ({ commit } ,[id]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });
    
    await axios.get(`/api/meses_pagos/${id}`)
      .then(response =>{
        
        commit('FETCH_MESESPAGOS', { meses_pagos: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async fetchPoll ({ commit }, [idPoll]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.get(`/api/payments/${idPoll}`)
      .then(response =>{
        commit(types.FETCH_ITEM, { payment: response.data.data });
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },

  async deletePoll ({ commit, dispatch }, [idPoll]) {
    commit(`loading/${types.UPDATE_LOADING}`, { status: true }, { root: true });

    await axios.delete(`/api/payments/${idPoll}`)
      .then(response =>{
        dispatch(`fetchPolls`);
      })
      .catch((e) => {
        console.error('error: ',e);
      });

    commit(`loading/${types.UPDATE_LOADING}`, { status: false }, { root: true });
  },
}
