import * as types from '../mutation-types';

// state
export const state = {
	menu_index: [
		{ icon: 'home',
			name: 'profile',
			route: 'home'
		},
		{ icon: 'globe',
			name: 'gestion',
			route: 'site'
		},
		{ icon: 'user',
			name: 'settings',
			route: 'profile'
		},
	],

	/////////////////////////////////////////////////////////////////

	tabs_site: [
		/*
		{ icon: 'bell',
			name: 'notifications',
			route: 'site.notifications',
			role: ['super','admin','emprend'],
			badge: true
		},
		*/
		{ icon: 'address-book',
			name: 'requests',
			route: 'site.requests',
			role: ['super','admin'],
		},
		{ icon: 'user-clock',
			name: 'quotes',
			route: 'site.quotes',
			role: ['super','admin'],
		},
		{ icon: 'glass-cheers',
			name: 'events',
			route: 'site.events',
			role: ['super','admin','emprend'],
		},
		{ icon: 'id-card-alt',
			name: 'emprendiments',
			route: 'site.emprendiments',
			role: ['super','admin'],
		},
		{ icon: 'boxes',
			name: 'products',
			route: 'site.products',
			role: ['super','admin','emprend'],
		},
		{ icon: 'hand-holding-usd',
			name: 'economics_movs',
			route: 'site.transactions',
			role: ['super','admin','emprend'],
		},
		/*
		{ icon: 'cash-register',
			name: 'payments',
			route: 'site.payments',
			role: ['super','admin'],
		},
		*/
		{ icon: 'poll',
			name: 'polls',
			route: 'site.polls',
			role: ['super','admin','emprend'],
		},
		/*{ icon: 'comments',
			name: 'forum',
			route: 'site.forum',
			role: ['super','admin','emprend'],
		},*/
		{ icon: 'chart-area',
			name: 'reports',
			route: 'site.reports',
			role: ['super','admin'],
			/*role: ['super','admin','emprend'],*/
		},

		{ icon: 'cog',
			name: 'roles',
			route: 'site.roles',
			role: ['super'],
		},
		{ icon: 'users',
			name: 'users',
			route: 'site.users',
			role: ['super'],
		},
	],

	tabs_site_report: [
		/*
		{ icon: 'cog',
			name: 'roles',
			route: 'site.reports.roles'
		},
		{ icon: 'cog',
			name: 'users',
			route: 'site.reports.users'
		},
		*/
		{ icon: 'cog',
			name: 'historico_grupos',
			route: 'site.reports.historico_grupos',
			role: ['super','admin'],
		},
		/*{ icon: 'cog',
			name: 'resumen_grupos',
			route: 'site.reports.resumen_grupos',
			role: ['super','admin'],
		},*/
		{ icon: 'cog',
			name: 'resumen_emprendimientos',
			route: 'site.reports.resumen_emprendimientos',
			role: ['super','admin'],
		},
		/*
		{ icon: 'cog',
			name: 'resumen_eventos',
			route: 'site.reports.resumen_eventos',
			role: ['super','admin'],
		},
		{ icon: 'cog',
			name: 'resumen_transacciones',
			route: 'site.reports.resumen_transacciones',
			role: ['super','admin','emprend'],
		},
		{ icon: 'cog',
			name: 'resumen_mensualidad',
			route: 'site.reports.resumen_mensualidad',
			role: ['super','admin'],
		},
		*/
	],

	/////////////////////////////////////////////////////////////////

	tabs_profile: [
		/*
		{ icon: 'user',
			name: 'personal_data',
			route: 'profile.personal'
		},
		*/
		/*
		{ icon: 'id-card-alt',
			name: 'emprendiment_data',
			route: 'profile.emprendiment',
			role: ['emprend'],
		},
		{ icon: 'id-card-alt',
			name: 'group_data',
			route: 'profile.group',
			role: ['super','admin'],
		},
		{ icon: 'users',
			name: 'parterns',
			route: 'profile.parterns',
			role: ['emprend'],
		},
		*/
		{ icon: 'cog',
			name: 'content_data',
			route: 'profile.content',
			role: ['super','admin'],
		},
		
		{ icon: 'envelope',
			name: 'email',
			route: 'profile.email',
			role: ['super','admin','emprend'],
		},
		{ icon: 'lock',
			name: 'password',
			route: 'profile.password',
			role: ['super','admin','emprend'],
		},
	],
}

// getters
export const getters = {
	menu_index: 		state => state.menu_index,
	tabs_site: 			state => state.tabs_site,
	tabs_site_report: 	state => state.tabs_site_report,
	tabs_profile: 		state => state.tabs_profile,
}

// mutations
export const mutations = {

}

// actions
export const actions = {

}