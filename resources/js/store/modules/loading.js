import axios from 'axios';
import Cookies from 'js-cookie';
import * as types from '../mutation-types';

// state
export const state = {
  loading: true,
}

// getters
export const getters = {
	loading: state => state.loading,
}

// mutations
export const mutations = {
	[types.UPDATE_LOADING] (state, { status }) {
    state.loading = status;
  },
}

// actions
export const actions = {
	async updateLoading ({ commit }, [status]) {
    commit(types.UPDATE_LOADING, { status: status });
  },
}
