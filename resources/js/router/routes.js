// Dinamycally import of pages components
function page (path) {
  return () => import(/* webpackChunkName: '' */ `~/pages/${path}`).then(m => m.default || m)
}
/* this.$attrs.color VARIABLE COLOR POR RUTA */

// Export of routes
export default [{
  path: '/',
  name: 'welcome',
  component: page('welcome.vue')
},

/* external ruotes */
{
  path: '/login',
  name: 'login',
  component: page('auth/login.vue'),
  props: {
    module: 'login',
    color: '#007bff'
  }
},
{
  path: '/register',
  name: 'register',
  component: page('site/requests/form.vue'),
  props: {
    module: 'register',
    color: '#007bff'
  }
},

{
  path: '/password/reset',
  name: 'password.request',
  component: page('auth/password/email.vue'),
  props: {
    module: 'reset',
    color: '#007bff'
  }
},
{
  path: '/password/reset/:token',
  name: 'password.reset',
  component: page('auth/password/reset.vue'),
  props: {
    module: 'reset',
    color: '#007bff'
  }
},

{
  path: '/email/verify/:id',
  name: 'verification.verify',
  component: page('auth/verification/verify.vue')
},
{
  path: '/email/resend',
  name: 'verification.resend',
  component: page('auth/verification/resend.vue')
},

/* public routes */
{
  path: '/stand',
  redirect: {
    name: 'stand.events'
  }
},

{
  path: '/stand/gallery',
  name: 'stand.gallery',
  component: page('stand/gallery.vue'),
  props: {
    module: 'stand',
    color: '#007bff'
  }
},
{
  path: '/stand/events',
  name: 'stand.events',
  component: page('stand/events.vue'),
  props: {
    module: 'stand',
    color: '#007bff'
  }
},
{
  path: '/stand/events/:id',
  name: 'stand.events',
  component: page('site/events/view.vue'),
  props: {
    module: 'stand',
    color: '#007bff'
  }
},

{
  path: '/stand/products',
  name: 'stand.products',
  component: page('stand/products.vue'),
  props: {
    module: 'stand',
    color: '#007bff'
  }
},
{
  path: '/stand/products/:id',
  name: 'stand.products',
  component: page('site/products/view.vue'),
  props: {
    module: 'stand',
    color: '#007bff'
  }
},

{
  path: '/stand/emprendiments',
  name: 'stand.emprendiments',
  component: page('stand/emprendiments.vue'),
  props: {
    module: 'stand',
    color: '#007bff'
  }
},
{
  path: '/stand/emprendiments/:id',
  name: 'stand.emprendiments',
  component: page('site/emprendiments/view.vue'),
  props: {
    module: 'stand',
    color: '#007bff'
  }
},

/* general routes */
{
  path: '/home',
  name: 'home',
  component: page('home.vue'),
  props: {
    module: 'home',
    color: '#007bff'
  }
},

{
  path: '',
  name: 'pages',
  component: page('pages.vue'),
  redirect: {
    name: 'site.notifications'
  },
  children: [

    {
      path: 'site',
      name: 'site',
      component: page('site/site.vue'),
      /* redirect: { name: 'site.notifications' }, */
      children: []
    },

    {
      path: 'site/notifications',
      name: 'site.notifications',
      component: page('site/notifications.vue'),
      props: {
        module: 'notifications',
        color: '#007bff',
        parent: 'site'
      }
    },

    {
      path: 'site/payments',
      name: 'site.payments',
      component: page('site/payments.vue'),
      props: {
        module: 'payments',
        color: '#007bff',
        parent: 'site'
      }
    },

    {
      path: 'site/forum',
      name: 'site.forum',
      component: page('site/forum.vue'),
      props: {
        module: 'forum',
        color: '#007bff',
        parent: 'site'
      }
    },

    {
      path: 'site/reports',
      name: 'site.reports',
      component: page('site/reports/reports.vue'),
      props: {
        module: 'reports',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/reports/roles',
      name: 'site.reports.roles',
      component: page('site/reports/report_roles.vue'),
      props: {
        module: 'reports',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/reports/users',
      name: 'site.reports.users',
      component: page('site/reports/report_users.vue'),
      props: {
        module: 'reports',
        color: '#007bff',
        parent: 'site'
      }
    },

    {
      path: 'site/reports/historico_grupos',
      name: 'site.reports.historico_grupos',
      component: page('site/reports/historico_grupos.vue'),
      props: {
        module: 'reports',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/reports/resumen_grupos',
      name: 'site.reports.resumen_grupos',
      component: page('site/reports/resumen_grupos.vue'),
      props: {
        module: 'reports',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/reports/resumen_emprendimientos',
      name: 'site.reports.resumen_emprendimientos',
      component: page('site/reports/resumen_emprendimientos.vue'),
      props: {
        module: 'reports',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/reports/resumen_eventos',
      name: 'site.reports.resumen_eventos',
      component: page('site/reports/resumen_eventos.vue'),
      props: {
        module: 'reports',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/reports/resumen_transacciones',
      name: 'site.reports.resumen_transacciones',
      component: page('site/reports/resumen_transacciones.vue'),
      props: {
        module: 'reports',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/reports/resumen_mensualidad',
      name: 'site.reports.resumen_mensualidad',
      component: page('site/reports/resumen_mensualidad.vue'),
      props: {
        module: 'reports',
        color: '#007bff',
        parent: 'site'
      }
    },

    {
      path: 'site/roles',
      name: 'site.roles',
      component: page('site/roles/roles.vue'),
      props: {
        module: 'roles',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/roles/form/:id?',
      name: 'site.roles.form',
      component: page('site/roles/form.vue'),
      props: {
        module: 'roles',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/roles/view/:id',
      name: 'site.roles.view',
      component: page('site/roles/view.vue'),
      props: {
        module: 'roles',
        color: '#007bff',
        parent: 'site'
      }
    },

    {
      path: 'site/users',
      name: 'site.users',
      component: page('site/users/users.vue'),
      props: {
        module: 'users',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/users/form/:id?',
      name: 'site.users.form',
      component: page('site/users/form.vue'),
      props: {
        module: 'users',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/users/view/:id',
      name: 'site.users.view',
      component: page('site/users/view.vue'),
      props: {
        module: 'users',
        color: '#007bff',
        parent: 'site'
      }
    },

    {
      path: 'site/events',
      name: 'site.events',
      component: page('site/events/events.vue'),
      props: {
        module: 'events',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/events/form/:id?',
      name: 'site.events.form',
      component: page('site/events/form.vue'),
      props: {
        module: 'events',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/events/view/:id',
      name: 'site.events.view',
      component: page('site/events/view.vue'),
      props: {
        module: 'events',
        color: '#007bff',
        parent: 'site'
      }
    },

    {
      path: 'site/products',
      name: 'site.products',
      component: page('site/products/products.vue'),
      props: {
        module: 'products',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/products/form/:id?',
      name: 'site.products.form',
      component: page('site/products/form.vue'),
      props: {
        module: 'products',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/products/view/:id',
      name: 'site.products.view',
      component: page('site/products/view.vue'),
      props: {
        module: 'products',
        color: '#007bff',
        parent: 'site'
      }
    },

    {
      path: 'site/transactions',
      name: 'site.transactions',
      component: page('site/transactions/transactions.vue'),
      props: {
        module: 'transactions',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/transactions/form/:id?',
      name: 'site.transactions.form',
      component: page('site/transactions/form.vue'),
      props: {
        module: 'transactions',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/transactions/event/:id_event/form',
      name: 'site.transactions.form_cost',
      component: page('site/transactions/form.vue'),
      props: {
        module: 'transactions',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/transactions/view/:id',
      name: 'site.transactions.view',
      component: page('site/transactions/view.vue'),
      props: {
        module: 'transactions',
        color: '#007bff',
        parent: 'site'
      }
    },

    {
      path: 'site/emprendiments',
      name: 'site.emprendiments',
      component: page('site/emprendiments/emprendiments.vue'),
      props: {
        module: 'emprendiments',
        color: '#007bff',
        parent: 'site'
      }
    },
    /* { path: 'site/emprendiments/form/:id?',
        name: 'site.emprendiments.form',
        component: page('site/emprendiments/form.vue'),
        props: { module: 'emprendiments', color: '#007bff', parent: 'site' }
      }, */
    {
      path: 'site/emprendiments/view/:id',
      name: 'site.emprendiments.view',
      component: page('site/emprendiments/view.vue'),
      props: {
        module: 'emprendiments',
        color: '#007bff',
        parent: 'site'
      }
    },

    {
      path: 'site/polls',
      name: 'site.polls',
      component: page('site/polls/polls.vue'),
      props: {
        module: 'polls',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/polls/form',
      name: 'site.polls.form',
      component: page('site/polls/form.vue'),
      props: {
        module: 'polls',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/polls/view/:id',
      name: 'site.polls.view',
      component: page('site/polls/view.vue'),
      props: {
        module: 'polls',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/polls/response/:id',
      name: 'site.polls.response',
      component: page('site/polls/response.vue'),
      props: {
        module: 'polls',
        color: '#007bff',
        parent: 'site'
      }
    },

    {
      path: 'site/requests',
      name: 'site.requests',
      component: page('site/requests/requests.vue'),
      props: {
        module: 'requests',
        color: '#007bff',
        parent: 'site'
      }
    },
    /* { path: 'site/requests/form/:id?',
        name: 'site.requests.form',
        component: page('site/requests/form.vue'),
        props: { module: 'requests', color: '#007bff', parent: 'site' }
      }, */
    {
      path: 'site/requests/view/:id',
      name: 'site.requests.view',
      component: page('site/requests/view.vue'),
      props: {
        module: 'requests',
        color: '#007bff',
        parent: 'site'
      }
    },
    // EDITAR CITA
    {
      path: 'site/requests/edit/:edit/:id',
      name: 'site.requests.edit',
      component: page('site/requests/view.vue'),
      props: {
        module: 'requests',
        color: '#007bff',
        parent: 'site'
      }
    },

    {
      path: 'site/quotes',
      name: 'site.quotes',
      component: page('site/quotes/quotes.vue'),
      props: {
        module: 'quotes',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/quotes/form/:id?',
      name: 'site.quotes.form',
      component: page('site/quotes/form.vue'),
      props: {
        module: 'quotes',
        color: '#007bff',
        parent: 'site'
      }
    },
    {
      path: 'site/quotes/view/:id',
      name: 'site.quotes.view',
      component: page('site/quotes/view.vue'),
      props: {
        module: 'quotes',
        color: '#007bff',
        parent: 'site'
      }
    },

    {
      path: 'profile',
      name: 'profile',
      component: page('profile/profile.vue'),
      // redirect: { name: 'profile.email' },
      children: []
    },

    /* { path: 'profile/personal',
        name: 'profile.personal',
        component: page('profile/personal.vue'),
        props: { module: 'personal', color: '#007bff' }
      }, */
    {
      path: 'profile/parterns',
      name: 'profile.parterns',
      component: page('profile/parterns.vue'),
      props: {
        module: 'parterns',
        color: '#007bff',
        parent: 'profile'
      }
    },
    {
      path: 'profile/emprendiment',
      name: 'profile.emprendiment',
      component: page('profile/emprendiment.vue'),
      props: {
        module: 'emprendiment',
        color: '#007bff',
        parent: 'profile'
      }
    },
    {
      path: 'profile/group',
      name: 'profile.group',
      component: page('profile/group.vue'),
      props: {
        module: 'group',
        color: '#007bff',
        parent: 'profile'
      }
    },
    {
      path: 'profile/content',
      name: 'profile.content',
      component: page('profile/content.vue'),
      props: {
        module: 'content',
        color: '#007bff',
        parent: 'profile'
      }
    },
    {
      path: 'profile/email',
      name: 'profile.email',
      component: page('profile/email.vue'),
      props: {
        module: 'email',
        color: '#007bff',
        parent: 'profile'
      }
    },
    {
      path: 'profile/password',
      name: 'profile.password',
      component: page('profile/password.vue'),
      props: {
        module: 'password',
        color: '#007bff',
        parent: 'profile'
      }
    }

  ]
},

/* errors routes */
{
  path: '*',
  component: page('errors/404.vue')
}]
