// Import vue and fontawesome
import Vue from 'vue';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import {
	far
} from '@fortawesome/free-regular-svg-icons';

import {
  fab 
} from '@fortawesome/free-brands-svg-icons';

import {  
  fas
} from '@fortawesome/free-solid-svg-icons';

library.add( far, fab, fas);

Vue.component('fa', FontAwesomeIcon);