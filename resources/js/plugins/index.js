// Import js pluggins
import './jQuery';

import './axios';
/*import './cloudinary';*/

import './element';
import './fontawesome';

import './html2canvas';
import './jspdf';

import './vueChatScroll';
//import './vueScrollTo';

import './moment';

import 'bootstrap';
