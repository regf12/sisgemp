// Import vue and element
import Vue from 'vue';
import i18n from '~/plugins/i18n';

import 'element-ui/lib/theme-chalk/index.css';
import Element from 'element-ui';

import esLocale from 'element-ui/lib/locale/lang/es';
import enLocale from 'element-ui/lib/locale/lang/en';

Vue.use(Element, {
  i18n: (key, value) => i18n.t(key, value)
});
