// Import vue and element
import Vue from 'vue';
import VueMoment from 'vue-moment';

Vue.use(VueMoment);