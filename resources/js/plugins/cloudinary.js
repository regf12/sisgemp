// Import vue and canvas
import Vue from 'vue';
import Cloudinary from "cloudinary-vue";

Vue.use(Cloudinary, {
  configuration: { cloudName: "regf" }
});