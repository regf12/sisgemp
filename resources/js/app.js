// Import vue with main plugins and components
import Vue from 'vue';
import store from '~/store';
import router from '~/router';
import i18n from '~/plugins/i18n';
import App from '~/components/mainComponents/App';

// Import util
import '~/bootstrap';

// Import plugins and componentes
import '~/plugins';
import '~/components';

// Import mixins
import Permissions from '~/mixins/Permissions';
import Forms from '~/mixins/Forms';
import Utils from '~/mixins/Utils';
Vue.mixin(Permissions);
Vue.mixin(Forms);
Vue.mixin(Utils);

// Config production state
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  i18n,
  store,
  router,
  ...App
});

//"postinstall": "npm run production"