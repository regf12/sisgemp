// Import vue
import Vue from 'vue'

// Import components
import Child from './mainComponents/Child'

import Card from './utilComponents/Card'
import Button from './utilComponents/Button'
import Checkbox from './utilComponents/Checkbox'
import DataTable from './utilComponents/DataTable'
import VueAnychart from './utilComponents/VueAnychart'
import RowFinalForm from './utilComponents/RowFinalForm'

import ChatForm from './chatComponents/ChatForm.vue'
import ChatMessages from './chatComponents/ChatMessages.vue'

import GgMap from './mapComponents/GgMap'
import GAutoCcomplete from './mapComponents/GAutoCcomplete'

import {
  HasError,
  AlertError,
  AlertSuccess
} from 'vform'

// Components that are registered globaly.
[
  Child,

  Card,
  Button,
  Checkbox,
  DataTable,
  VueAnychart,
  RowFinalForm,

  ChatForm,
  ChatMessages,

  GgMap,
  GAutoCcomplete,

  HasError,
  AlertError,
  AlertSuccess

].forEach(Component => {
  Vue.component(Component.name, Component)
})