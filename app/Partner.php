<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
   /**
	 * Fields that are mass assignable
	 *
	 * @var array
	 */
	protected $fillable = [
		'nombre',
		'apellido',
		'sexo',
		'fecha_nac',
		'cedula',
		'estado_civil',
		'nivel_instruccion'
	];
}
