<?php

namespace App\Http\Controllers\Web;

use App\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use DB;
use App\Emprendiment;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emprendiments = Emprendiment::select('id', 'nombre')->get();

        foreach ($emprendiments as $index => $emprend) {
            $meses = DB::table('payments')
                ->select('mes', 'monto')
                ->where('id_emprendiment', $emprend->id)
                ->groupBy('mes', 'monto')
                ->get();

            foreach ($meses as $mes) {
                $emprend[$mes->mes] = $mes->monto;
            }
        }


        return response()->json([
            'status' => 'success',
            'data' => $emprendiments
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //* no se usa *//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //* no se usa *//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function mesesPagos($id)
    {
        $mesesPagos = DB::table('payments')->select('mes')
            ->leftJoin('emprendiments', 'emprendiments.id', '=', 'id_emprendiment')
            ->where('emprendiments.id', $id)
            ->whereNotNull('monto')
            ->get();

        return response()->json([
            'status' => 'success',
            'data' => $mesesPagos
        ], 200);
    }
}
