<?php

namespace App\Http\Controllers\Web;

use App\Transaction;

use App\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use DB;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $transactions = Transaction::select(
            'transactions.*',
            'concepts.nombre as concept',
            'concepts.operacion',
            'concepts.role'
        )->join('concepts', 'concepts.id', 'id_concept')
            ->where('id_user', $user->id)
            ->orderBy('fecha', 'asc')
            ->get();

        $count = 0;
        $saldo = 0;

        if (count($transactions)) {
            foreach ($transactions as $key => $transaction) {
                if ($count == 0) {
                    $saldo = $transaction->monto;
                } else if ($transaction->operacion == 'ENTRADA') {
                    $saldo += $transaction->monto;
                } else if ($transaction->operacion == 'SALIDA') {
                    $saldo -= $transaction->monto;
                }
                $transaction['saldo'] = $saldo;
                $count++;
            }
        }

        return response()->json([
            'status' => 'success',
            'data' => $transactions
        ], 200);
    }

    public function concepts()
    {
        $concepts = DB::table('concepts')
            ->select('*')
            ->get();

        return response()->json([
            'status' => 'success',
            'data' => $concepts
        ], 200);
    }

    public function getMensualidad()
    {
        /*$result=DB::select();*/ }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //* no se usa *//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $idTransaction = DB::table('transactions')->insertGetId([
            'fecha' => $request->fecha,
            'monto' => $request->monto,
            'descripcion' => $request->descripcion,
            'automatica' => false,

            'id_referencia' => $user->id,
            'id_concept' => $request->id_concept,
            'id_user' => $user->id,

            'created_at' => now()
        ]);

        if ($request->id_concept == 13) {

            foreach ($request['Meses'] as $index => $mes) {
                if ($mes['monto'] > 0) {
                    DB::table('payments')->insert([
                        'mes' => $mes['mes'],
                        'monto' => $mes['monto'],
                        'id_transaction' => $idTransaction,
                        'id_emprendiment' => $request->id_emprendiment,
                        'id_group' => 1,
                        'created_at' => now(),
                    ]);
                }
            }
        }


        /* if ($request->id_concept == 11) {
            Event::where('id', $request->id_event_finalized)
                ->update([
                    'gastos' => $request->gastos,
                    'estado' => "REALIZADO",
                ]);
        } */

        if (count($request->checkGastos) > 0 && $request->id_concept == 11) {
            foreach ($request->checkGastos as $gasto) {
                DB::table('expenses')->insert([
                    'tipo' => $gasto['campo'],
                    'monto' => $gasto['monto'],
                    'id_transaction' => $idTransaction,
                    'id_event' => $request->id_event_finalized,
                    'created_at' => now(),
                ]);
            };
        }


        /*if($request->concepto =="mensualidad"){
            DB::table('paymets')->insert([
                'id_emprendiment' =>$request->id_emprendiment,
                'id_transaction'=> $idTransaction,
                'mes' => $request->mes
            ]);
        }*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();

        $data = Transaction::where('id', $id)
            ->where('id_user', $user->id)
            ->get();

        if ($data[0]->id_concept == 11) {
            $data[0]->event_data = DB::table('expenses')->where('id_transaction', $id)->get();
        }

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //* no se usa *//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        #$dato =Transaction::findOrFail($id);
        /*$dato->operacion = $request->operacion;*/
        /*$dato->id_type_transaction = $request->operacion;*/
        /*$dato->automatica = false;*/
        /*$dato->hora = now();*/

        $user = Auth::user();

        $dato = Transaction::where('id', $id)
            ->where('id_user', $user->id)
            ->get();

        if (count($dato)) {
            $dato = $dato[0];
        } else {
            return response()->json([
                'status' => 'fail',
                'data' => $dato
            ], 500);
        }


        $dato->fecha = $request->fecha;


        $dato->monto = $request->monto;
        $dato->id_concept = $request->id_concept;
        $dato->descripcion = $request->descripcion;

        $dato->id_referencia = $user->id;

        if (($request->id_event_finalized) && ($request->id_concept == 11)) {
            DB::table('expenses')
                ->where('id_transaction', $id)
                ->where('id_event', $request->id_event_finalized)
                ->delete();
            /*$random= DB::table('expenses')
                ->where('id_transaction',$id)
                ->where('id_event',$request->id_event_finalized)
                ->select('tipo')
                ->get();
            $random1=[];
            foreach ($random as $key => $value) {
                $random1[$key]=$value->tipo;
                echo $random1[$key]." ";
            }*/

            if (count($request->checkGastos) > 0) {
                foreach ($request->checkGastos as $gasto) {
                    DB::table('expenses')->insert([
                        'tipo' => $gasto['campo'],
                        'monto' => $gasto['monto'],
                        'id_transaction' => $id,
                        'id_event' => $request->id_event_finalized,
                        'created_at' => now(),
                    ]);
                };
            }

            /*$findCheck=in_array($value, $random1);
                    if(!$findCheck){
                        DB::table('expenses')->insert([
                            'tipo' => $value,
                            'monto' => $request->checkGastos['valores'][$key],
                            'id_transaction' => $id,
                            'id_event' => $request->id_event_finalized,
                            'created_at' => now(),
                        ]);
                    }else{
                        DB::table('expenses')->where('id_transaction',$id)
                            ->where("id_event",$request->id_event_finalized)
                            ->where('expenses.tipo',$value)
                            ->update([
                                'monto' => $request->checkGastos['valores'][$key],
                                'updated_at' =>now(),
                            ]);
                    }*/
        }

        $dato->updated_at = now();

        $dato->save();

        return response()->json([
            'status' => 'success',
            'data' => $dato
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Transaction::destroy($id);
    }
}
