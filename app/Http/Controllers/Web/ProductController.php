<?php

namespace App\Http\Controllers\Web;

use App\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use DB;

use App\Emprendiment;
use App\Notification;

class ProductController extends Controller
{

    // public function __construc()
    // {
    //   $this->middleware('auth');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->role['admin'] || $user->role['super']) {


            $products = DB::table('products')
                ->leftJoin('emprendiments', 'emprendiments.id', '=', 'id_emprendiment')
                ->select('emprendiments.nombre as emprend_name', 'products.*')
                ->orderBy('products.created_at')
                ->get();
        } else {
            if ($user->role('emprend')) {

                $id_emprend = DB::table('emprendiments')->where('id_user', '=', $user->id)->get();

                $products = DB::table('products')
                    ->join('emprendiments', 'emprendiments.id', '=', 'products.id_emprendiment')
                    ->where(function ($query) use ($id_emprend) {
                        $query->where('emprendiments.id', '=', $id_emprend[0]->id);
                    })
                    ->select('products.*', 'emprendiments.nombre as nombre_emprendimiento')
                    ->orderBy('products.created_at')
                    ->get();

                /*$products=[];
                $i=0;
                foreach ($total_data as $producto){
                    if($producto->estado!="PENDING"){
                        $products[$i]=$producto;
                        $i++;
                    }
                }*/
            }
        }

        return response()->json([
            'status' => 'success',
            'data' => $products
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //* no se usa *//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $dato = Emprendiment::where('id_user', $user->id)->get();

        $idProduct = DB::table('products')->insertGetId([
            'nombre' => $request->name,
            'descripcion' => $request->description,
            'precio' => $request->price,
            'id_emprendiment' => $dato[0]->id,
            'created_at' => now()
        ]);

        foreach ($request->images as $imagen) {
            if (isset($imagen['response'])) {
                $url = $imagen['response'];
            } else {
                if (isset($imagen['url'])) {
                    $url = $imagen['url'];
                } else {
                    $url = "images/imagen-no.png";
                }
            }

            DB::table('images')->insert([
                'url' => $url,
                'tipo' => 'PRODUCTS',
                'id_referencia' => $idProduct,
                'created_at' => now()
            ]);
        };

        foreach ([1, 2] as $adm) {
            DB::table('notifications')->insertGetId([
                'tipo' => 'PRODUCTS',
                'id_referencia' => $idProduct,
                'id_user' => $adm,
                'created_at' => now()
            ]);
        };
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::table('products')->where('products.id', $id)
            ->leftJoin('emprendiments', 'emprendiments.id', '=', 'id_emprendiment')
            ->select('emprendiments.nombre as emprend_name', 'products.*')
            ->get();

        #$data=DB::table('products')->where('id',$id)->get();

        $data[0]->images = DB::table('images')
            ->where('id_referencia', $id)
            ->where('tipo', "PRODUCTS")
            ->select('url')
            ->get();

        return response()->json([
            'status' => 'success',
            'data' => $data[0]
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //* no se usa *//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('products')
            ->where('id', '=', $id)
            ->update([
                'nombre' => $request->name,
                'descripcion' => $request->description,
                'precio' => $request->price,
                'estado' => "PENDING",
                'updated_at' => now()
            ]);

        DB::table('images')->where('id_referencia', $id)->where('tipo', "PRODUCTS")->delete();
        foreach ($request->images as $imagen) {
            if (isset($imagen['response'])) {
                $url = $imagen['response'];
            } else {
                if (isset($imagen['url'])) {
                    $url = $imagen['url'];
                } else {
                    $url = "images/imagen-no.png";
                }
            }

            DB::table('images')->insert([
                'url' => $url,
                'tipo' => 'PRODUCTS',
                'id_referencia' => $id,
                'created_at' => now()
            ]);
        };

        Notification::where('id_referencia', $id)->where('tipo', "PRODUCTS")->delete();
        foreach ([1, 2] as $adm) {
            DB::table('notifications')->insert([
                'tipo' => 'PRODUCTS',
                'id_referencia' => $id,
                'id_user' => $adm,
                'created_at' => now()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Product::destroy($id);
    }

    public function changeStatus(Request $request)
    {
        DB::table('products')
            ->where('id', '=', $request->id)
            ->update([
                'estado' => $request->button,
                'updated_at' => now()
            ]);

        $producto = DB::table('products')
            ->where('id', '=', $request->id)->first();

        $emprendimiento = DB::table('emprendiments')
            ->where('id', '=', $producto->id_emprendiment)->first();

        DB::table('notifications')->insert([
            'tipo' => 'PRODUCTS',
            'id_referencia' => $request->id,
            'id_user' => $emprendimiento->id_user,
            'created_at' => now()
        ]);
    }

    public function ProductsStand()
    {

        $total_data = Product::all();
        $products = [];
        $i = 0;
        foreach ($total_data as $producto) {
            if ($producto->estado == 'APPROVED') {
                $products[$i] = $producto;
                $i++;
            }
        }

        foreach ($total_data as $data) {
            $data->images = DB::table('images')
                ->where('id_referencia', $data->id)
                ->where('tipo', "PRODUCTS")
                ->select('url')
                ->get();
        }


        return response()->json([
            'status' => 'success',
            'data' => $products
        ], 200);
    }
}
