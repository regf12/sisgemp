<?php

namespace App\Http\Controllers\Web;

use App\Poll;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use DB;

class PollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $polls = Poll::all();

        return response()->json([
            'status' => 'success',
            'data' => $polls
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //* no se usa *//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $idPoll = DB::table('polls')->insertGetId([
            'nombre' => $request['nombre'],
            'id_user' => $user->id
        ]);

        foreach ($request['preguntas'] as $question) {
            $idQuestion = DB::table('questions')->insertGetId([
                'nombre' => $question['nombre'],
                'id_poll' => $idPoll
            ]);

            foreach ($question['respuestas'] as $response) {
                DB::table('responses')->insert([
                    'nombre' => $response['nombre'],
                    'id_question' => $idQuestion
                ]);
            };
        };

        return response()->json([
            'status' => 'success',
            'data' => 'success'
        ], 200);
    }

    public function responsePoll(Request $request)
    {
        $user = Auth::user();

        $idPoll_user = DB::table('poll_user')->insertGetId([
            'id_poll' => $request['id_encuesta'],
            'id_user' => $user->id
        ]);

        foreach ($request['preguntas'] as $question) {
            $idQuestion = DB::table('response_user')->insertGetId([
                'id_poll_user' => $idPoll_user,
                'id_response' => $question['id_respuesta']
            ]);
        };

        return response()->json([
            'status' => 'success',
            'data' => 'success'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dato = Poll::find($id);

        $dato->preguntas = DB::table('questions')
            ->where('id_poll', $dato->id)
            ->select('*')
            ->get();

        foreach ($dato->preguntas as $pregunta) {
            $pregunta->respuestas = DB::table('responses')
                ->where('id_question', $pregunta->id)
                ->select('*')
                ->get();
        }

        return response()->json([
            'status' => 'success',
            'data' => $dato
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //* no se usa *//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Poll::destroy($id);
    }

    public function PollResults($id)
    {
        $dato = Poll::find($id);

        $dato->totalRespuestas = DB::table('poll_user')->where('id_poll', $dato->id)->count();

        /*echo "TOTAL respuestas: "-$dato->totalRespuestas;*/

        $dato->preguntas = DB::table('questions')->leftJoin('polls', 'polls.id', '=', 'questions.id_poll')
            ->where("polls.id", '=', $id)
            ->select('questions.nombre as pregunta', 'questions.id as pregunta_id')
            ->get();

        /*echo $dato->preguntas;*/

        for ($i = 0; $i < count($dato->preguntas); $i++) {
            /*echo $dato->preguntas[$i]->pregunta_id;*/
            $dato->preguntas[$i]->respuestas = DB::table('responses')->leftJoin('response_user', 'response_user.id_response', '=', 'responses.id')
                ->leftJoin('questions', 'questions.id', '=', 'id_question')
                ->leftJoin('polls', 'polls.id', '=', 'id_poll')
                ->where('polls.id', '=', $id)
                ->where('questions.id', '=', $dato->preguntas[$i]->pregunta_id)
                ->select(DB::raw('responses.nombre as respuesta, count(id_response) as N_respuestas'))
                ->groupBy('responses.id')
                ->get();
            /*->where('responses.id','=',$dato->preguntas[$i]->pregunta_id)*/
        }

        /*echo $dato;*/

        return response()->json([
            'status' => 'success',
            'data' => $dato
        ], 200);
    }
}
