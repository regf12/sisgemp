<?php

namespace App\Http\Controllers\Web;

use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use DB;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /*$this->middleware([
            'role:super-admin',
            'permission:role-create|role-edit|role-delete'
        ])->except('index');*/

        $this->middleware([
            'role_or_permission:super|roles-create|roles-edit|roles-delete|roles-permissions-list|roles-permissions-delete'
        ])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();

        return response()->json([
            'status' => 'success',
            'data' => $roles
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function permissions()
    {
        $permissions = [];

        foreach (Permission::all() as $permission) {
            $permissions[] = $permission->name;
        }

        return response()->json([
            'status' => 'success',
            'data' => $permissions
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function permissionsRole($id)
    {
        $role = Role::findOrFail($id);
        $permissions = [];

        foreach (Permission::all() as $permission) {
            if ($role->hasPermissionTo($permission->name)) {
                $permissions[] = $permission->name;
            }
        }

        return response()->json([
            'status' => 'success',
            'data' => $permissions
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //* no se usa *//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::findOrFail($id);
        $permissions = [];

        foreach (Permission::all() as $permission) {
            if ($role->hasPermissionTo($permission->name)) {
                $permissions[] = $permission->name;
            }
        }

        return response()->json([
            'status' => 'success',
            'data' => $permissions
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //* no se usa *//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function roleDestroy($id)
    {
        return Role::destroy($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function permissionDestroy($id)
    {
        return Permission::destroy($id);
    }
}
