<?php

namespace App\Http\Controllers\Web;

use App\Image;
use App\Emprendiment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use DB;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::all();

        return response()->json([
            'status' => 'success',
            'data' => $images
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //* no se usa *//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function uploadImage(Request $request)
    {
        if ($request->file('file')) {
            $path = Storage::disk('public')->put('images',  $request->file('file'));
            return asset($path);
        }
        return false;
    }

    public function uploadImageEmprendimiento(Request $request)
    {
        $user = Auth::user();

        if ($user->role['admin'] || $user->role['super']) {

            DB::table('images')
                ->where('id_referencia', 1)
                ->where('tipo', "GROUPS")
                ->delete();

            $imagen = DB::table('images')->insertGetId([
                'url' => $request->urlImage,
                'tipo' => 'GROUPS',
                'id_referencia' => 1,
                'created_at' => now()
            ]);
        } else {

            $emprendiment = Emprendiment::where('id_user', $user->id)->get();

            DB::table('images')
                ->where('id_referencia', $emprendiment[0]->id)
                ->where('tipo', "EMPRENDIMENTS")
                ->delete();

            $imagen = DB::table('images')->insertGetId([
                'url' => $request->urlImage,
                'tipo' => 'EMPRENDIMENTS',
                'id_referencia' => $emprendiment[0]->id,
                'created_at' => now()
            ]);
        }

        return response()->json([
            'status' => 'success',
            'data' => $imagen
        ], 200);
    }

    public function uploadImageBanner(Request $request)
    {
        $user = Auth::user();

        if ($user->role['admin'] || $user->role['super']) {

            DB::table('images')
                ->where('id_referencia', 1)
                ->where('tipo', "BANNERGROUP")
                ->delete();

            $imagen = DB::table('images')->insertGetId([
                'url' => $request->urlImage,
                'tipo' => 'BANNERGROUP',
                'id_referencia' => 1,
                'created_at' => now()
            ]);
        } else {

            $emprendiment = Emprendiment::where('id_user', $user->id)->get();

            DB::table('images')
                ->where('id_referencia', $emprendiment[0]->id)
                ->where('tipo', "BANNER")
                ->delete();

            $imagen = DB::table('images')->insertGetId([
                'url' => $request->urlImage,
                'tipo' => 'BANNER',
                'id_referencia' => $emprendiment[0]->id,
                'created_at' => now()
            ]);
        }

        return response()->json([
            'status' => 'success',
            'data' => $imagen
        ], 200);
    }

    public function uploadGalery(Request $request)
    {

        DB::table('images')
            ->where('id_referencia', 1)
            ->where('tipo', "GALLERY")
            ->delete();

        foreach ($request->images as $imagen) {
            if (isset($imagen['response'])) {
                $url = $imagen['response'];
            } else {
                if (isset($imagen['url'])) {
                    $url = $imagen['url'];
                } else {
                    $url = "images/imagen-no.png";
                }
            }

            DB::table('images')->insert([
                'url' => $url,
                'tipo' => 'GALLERY',
                'id_referencia' => 1,
                'created_at' => now()
            ]);
        };

        return response()->json([
            'status' => 'success',
            'data' => true
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //* no se usa *//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Image::destroy($id);
    }


    public function getImages($type)
    {
        $tipo = 'GALLERY';

        if ($type != null && $type != '') {
            $tipo = $type;
        }

        $images = DB::table('images')
            ->where('tipo', $tipo)
            ->get();

        return response()->json([
            'status' => 'success',
            'data' => $images
        ], 200);
    }
}
