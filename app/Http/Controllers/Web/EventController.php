<?php

namespace App\Http\Controllers\Web;

use App\Notification;
use App\Event;
use App\Emprendiment;
use App\Transaction;
use Mail;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Carbon\Carbon;
use DB;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->role['admin'] || $user->role['super']) {
            /*$events = Event::select('events.*')
                ->where('estado', "ACTIVO")
                ->get();*/
            $events = Event::select('events.*')
                ->get();
        } else {
            /*
            $events = DB::table('events')->join('notifications',function($join){
                $join->on('id_referencia','=','events.id')
                ->where('tipo',"EVENTS");
            })->where('id_user',$user->id)->select('events.*')->get();
            */
            $events = Event::select('events.*')
                ->leftJoin('notifications', 'id_referencia', 'events.id')
                ->where('notifications.tipo', "EVENTS")
                ->where('estado', "ACTIVO")
                ->where('id_user', $user->id)
                ->get();
        }

        return response()->json([
            'status' => 'success',
            'data' => $events
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //* no se usa *//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lugar = $request['lugar2'];
        $idLugar = DB::table('places')->insertGetId([
            'name' => $lugar['name'],
            'lat' => $lugar['lat'],
            'lng' => $lugar['lng'],
            'created_at' => now()
        ]);

        $request['id_lugar'] = $idLugar;
        $dato = Event::create($request->all());

        $id_event = $dato->id;

        foreach ($request->images as $imagen) {
            if (isset($imagen['response'])) {
                $url = $imagen['response'];
            } else {
                if (isset($imagen['url'])) {
                    $url = $imagen['url'];
                } else {
                    $url = "images/imagen-no.png";
                }
            }

            DB::table('images')->insert([
                'url' => $url,
                'tipo' => 'EVENTS',
                'id_referencia' => $id_event,
                'created_at' => now()
            ]);
        };

        foreach ($request->invitados as $invitado) {
            DB::table('notifications')->insert([
                'tipo' => 'EVENTS',
                'id_referencia' => $id_event,
                'id_user' => $invitado,
                'created_at' => now()
            ]);
        };

        return response()->json([
            'status' => 'success',
            'data' => $dato
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Event::where('id', $id)->get();

        $data[0]->lugar = DB::table('places')
            ->where('places.id', $data[0]->id_lugar)
            ->select('name', 'lat', 'lng')
            ->get();

        $data[0]->invitados = Notification::where('id_referencia', $id)
            ->where('tipo', "EVENTS")
            ->select('id_user')
            ->get();

        $data[0]->images = DB::table('images')
            ->where('id_referencia', $id)
            ->where('tipo', "EVENTS")
            ->select('url')
            ->get();

        return response()->json([
            'status' => 'success',
            'data' => $data[0]
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //* no se usa *//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lugar = $request['lugar2'];

        $dato = DB::table('places')
            ->where('lat', $lugar['lat'])
            ->where('lng', $lugar['lng'])
            ->get();

        if (count($dato) > 0) {
            $request['id_lugar'] = $dato[0]->id;
        } else {
            $idLugar = DB::table('places')->insertGetId([
                'name' => $lugar['name'],
                'lat' => $lugar['lat'],
                'lng' => $lugar['lng'],
                'created_at' => now()
            ]);
            $request['id_lugar'] = $idLugar;
        }
        $dato = Event::where('id', $id)
            ->update([
                'nombre' => $request['nombre'],
                'motivo' => $request['motivo'],
                'costo' => $request['costo'],
                /*'gastos' => $request['gastos'],*/
                'id_lugar' => $request['id_lugar'],
                'cupos' => $request['cupos'],
                'fecha' => $request['fecha'],
                'hora_inicio' => $request['hora_inicio'],
                'hora_fin' => $request['hora_fin'],
                'estado' => "ACTIVO",
                'tipo' => $request['tipo'],
            ]);

        DB::table('images')->where('id_referencia', $id)->where('tipo', "EVENTS")->delete();
        foreach ($request->images as $imagen) {
            if (isset($imagen['response'])) {
                $url = $imagen['response'];
            } else {
                $url = $imagen['url'];
            }

            DB::table('images')->insert([
                'url' => $url,
                'tipo' => 'EVENTS',
                'id_referencia' => $id,
                'created_at' => now()
            ]);
        };

        Notification::where('id_referencia', $id)->where('tipo', "EVENTS")->delete();
        foreach ($request["invitados"] as $invitado) {
            DB::table('notifications')->insert([
                'tipo' => 'EVENTS',
                'id_referencia' => $id,
                'id_user' => $invitado,
                'created_at' => now()
            ]);
        }

        /* $dato = Event::findOrFail($id);
        $dato->nombre = $request->nombre;
        $dato->motivo = $request->motivo;
        $dato->lugar = $request->lugar;
        $dato->descripcion = $request->descripcion;
        $dato->fecha = $request->fecha;
        $dato->hora_inicio = $request->hora_inicio;
        $dato->hora_fin = $request->hora_fin;
        $dato->cupos = $request->cupos;
        $dato->gastos = $request->gastos;
        $dato->estado = $request->estado;
        $dato->costo = $request->costo;

        $dato->save();
        return response()->json($dato);*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Event::destroy($id);
    }

    public function postulationEmprend(Request $Postulation)
    {
        $user = Auth::user();

        $notificationID = Emprendiment::join('notifications', 'notifications.id_user', '=', 'emprendiments.id_user')
            ->where('notifications.id_referencia', '=', $Postulation->id_event)
            ->where('tipo', "EVENTS")
            ->where('emprendiments.id', $Postulation->id_emprend)
            ->get();

        $evento = Event::find($Postulation->id_event);

        $data = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20);

        $idINscrip = DB::table('postulations')->insertGetId([
            'id_notification' => $notificationID[0]->id,
            'id_emprend' => $Postulation->id_emprend,
            'id_event' => $Postulation->id_event,
            'codigo' => $data,
            'created_at' => now(),
        ]);

        $idTransaction = DB::table('transactions')->insertGetId([
            'fecha' => now(),
            'monto' => $evento->costo,
            'descripcion' => 'Pago por evento',
            'automatica' => true,

            'id_referencia' => $evento->id,
            'id_concept' => 9,
            'id_user' => $user->id,

            'created_at' => now()
        ]);

        foreach ($Postulation->images as $imagen) {
            if (isset($imagen['response'])) {
                $url = $imagen['response'];
            } else {
                $url = $imagen['url'];
            }

            DB::table('images')->insert([
                'url' => $url,
                'tipo' => 'TRANSACTIONS',
                'id_referencia' => $idTransaction,
                'created_at' => now()
            ]);
        };

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function getPostulations($id)
    {
        $data = DB::table('emprendiments')
            ->leftJoin('notifications', function ($join) use ($id) {
                $join->on('notifications.id_user', '=', 'emprendiments.id_user')
                    ->where('notifications.tipo', '=', "EVENTS")
                    ->where('id_referencia', '=', $id);
            })
            ->leftJoin('postulations', function ($join) use ($id) {
                $join->on('emprendiments.id', '=', 'postulations.id_emprend')
                    ->where('id_event', '=', $id);
            })
            ->leftJoin('inscriptions', 'id_postulation', '=', 'postulations.id')
            ->whereNotNull('tipo')
            ->whereNotNull('postulations.id')
            ->whereNull('inscriptions.id')
            ->select('emprendiments.id_user', 'postulations.id_emprend', 'emprendiments.nombre', 'postulations.id as id_postulation', 'postulations.codigo', 'postulations.created_at')
            ->get();

        $count=0;
        $data[0]->images=null;
        foreach ($data as $value) {

            $data[0]->images= DB::table('images')
            ->leftJoin('transactions','transactions.id','=','images.id_referencia')
            ->leftJoin('postulations','postulations.id_event','=','transactions.id_referencia')
            ->leftJoin('emprendiments','emprendiments.id','=','postulations.id_emprend')
            ->where('images.tipo','TRANSACTIONS')
            ->where('postulations.id_event',$id)
            ->where('transactions.id_user',$value->id_user)
            ->where('postulations.id_emprend',$value->id_emprend)
            ->select('images.url')
            ->get();
            
            $count++;


        }
        
        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function getPostulation($id)
    {
        $user = Auth::user();

        $id_emprend = DB::table('emprendiments')->where('id_user', $user->id)->select('emprendiments.id')->get();
        /*echo $id_emprend[0]->id; echo " ".$id;*/
        $data = DB::table('postulations')->where('id_emprend', $id_emprend[0]->id)->where('id_event', $id)->get();

        /* echo "Data: ".$data;*/
        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function createInscriptions(Request $ids)
    {
        foreach ($ids->arrayIds as $dato) {

            DB::table('inscriptions')->insert([
                'id_postulation' => $dato,
                'created_at' => now(),
            ]);

            Transaction::insert([
                'fecha' => now(),
                'monto' => $ids->costoEvent,
                'created_at' => now(),
                'descripcion' => "Pago de inscripcion evento",
                'id_user' => $dato,
                'id_referencia' => $ids->event,
                'automatica' => true,
                'id_concept' => 4,
                'tipo' => "EVENTS",
            ]);
        }
    }

    public function createInscription(Request $ids)
    {
        DB::table('inscriptions')->insert([
            'id_postulation' => $ids['arrayIds'],
            'created_at' => now(),
        ]);

        Transaction::insert([
            'fecha' => now(),
            'monto' => $ids['costoEvent'],
            'created_at' => now(),
            'descripcion' => "Pago de inscripcion evento",
            'id_user' => $ids['arrayIds'],
            'id_referencia' => $ids['event'],
            'automatica' => true,
            'id_concept' => 4,
            'tipo' => "EVENTS",
        ]);

        $data = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20);

        DB::table('postulations')
            ->where('id', $ids['arrayIds'] + 0)
            ->update(['codigo' => $data]);

        /*
        Mail::send('Emails.ContactMailView', [
            'solicitud' => $solicitud, 
            'path_logo' => asset('/images/logo_sisgemp.png'),
            'nombre' => $solicitud->nombre,
            'mail' => $solicitud->correo,
            'mensaje' => $solicitud['mensaje'],
            'tipo' => 'REQUEST'
        ], function ($mail) use ($solicitud) {
            $mail->to($solicitud->correo)->subject($solicitud['mensaje']);
        });
        */

        return response()->json([
            'status' => 'success',
            'data' => true
        ], 200);
    }

    public function createAsistences(Request $ids)
    {
        foreach ($ids->arrayCodes as $dato) {
            DB::table('assistences')->insert([
                'id_inscription' => $dato["id_inscription"],
                'created_at' => now(),
            ]);
        }

        /*foreach($ids->arrayIds as $dato ){
            
            DB::table('assistences')->insert([
                'id_inscription'=>$dato,
                'created_at'=>now(),
            ]);
        }*/
    }


    public function getInscriptions($id)
    {
        /*$data = DB::table('emprendiments')
            ->leftJoin('postulations',function($join) use($id){
                $join->on('emprendiments.id', '=', 'id_emprend')
                    ->where('id_event','=',$id);
            })
            ->leftJoin("inscriptions",'id_postulation','=','postulations.id')
            ->whereNotNull('inscriptions.id')
            ->select('emprendiments.id_user','id_emprend','emprendiments.nombre','inscriptions.id as id_inscription')
            ->get();*/

        /*$data = DB::table('inscriptions')
            ->leftJoin('postulations', 'postulations.id', '=', 'id_postulation')
            ->leftJoin('events', 'events.id', '=', 'id_event')
            ->leftJoin('emprendiments', 'emprendiments.id', '=', 'id_emprend')
            ->leftJoin('assistences', 'id_inscription', '=', 'inscriptions.id')
            ->where('events.id', $id)
            ->whereNull('id_inscription')
            ->select('postulations.id as id_inscription', 'emprendiments.nombre', 'id_emprend', 'emprendiments.id_user', 'codigo')
            ->get(); ESTE ES EL QUE ESTABA FUNCIONAL*/

        $data = DB::table('inscriptions')
            ->leftJoin('postulations', 'postulations.id', '=', 'id_postulation')
            ->leftJoin('events', 'events.id', '=', 'id_event')
            ->leftJoin('emprendiments', 'emprendiments.id', '=', 'id_emprend')
            ->leftJoin('assistences', 'id_inscription', '=', 'inscriptions.id')
            ->where('events.id', $id)
            ->whereNull('id_inscription')
            ->select('inscriptions.id as id_inscription', 'emprendiments.nombre', 'id_emprend', 'emprendiments.id_user', 'codigo')
            ->get();

        foreach ($data as $emp) {
            $emp->images = DB::table('images')
                ->where('id_referencia', $emp->id_emprend)
                ->where('tipo', 'EMPRENDIMENTS')
                ->select('url')
                ->get();
        }

        /* foreach ($data as $emp) {
            $emp->images = DB::table('images')
                ->where('id_referencia', $emp->id_emprend)
                ->where('tipo', 'EMPRENDIMENTS')
                ->select('url')
                ->get();
        } */

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function getInvitations($id)
    {
        $data = DB::table('emprendiments')
            ->leftJoin('notifications', function ($join) use ($id) {
                $join->on('notifications.id_user', '=', 'emprendiments.id_user')
                    ->where('notifications.tipo', '=', "EVENTS")
                    ->where('id_referencia', '=', $id);
            })
            ->leftJoin('postulations', function ($join) use ($id) {
                $join->on('emprendiments.id', '=', 'postulations.id_emprend')
                    ->where('id_event', '=', $id);
            })
            ->whereNotNull('tipo')
            ->whereNull('postulations.id')
            ->select('emprendiments.id_user', 'emprendiments.nombre', 'notifications.id as id_notification', 'emprendiments.id as id_emprendiment')
            ->get();

        foreach ($data as $emp) {
            $emp->images = DB::table('images')
                ->where('id_referencia', $emp->id_emprendiment)
                ->where('tipo', 'EMPRENDIMENTS')
                ->select('url')
                ->get();
        }

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function FinalizedEvents()
    {
        /*date_default_timezone_set('Etc/GMT+4');*/
        date_default_timezone_set('America/Caracas');
        $hora_actual = date("H") . ":" . date("i") . ":" . date("s");

        /*$EventosFinalizados = Event::where('fecha', '<=', now())
            ->where('hora_fin', '<=', $hora_actual)
            ->get();*/
        $Eventos = Event::all();
        /*$EventosFinalizados=array();
        
        $tamEvents=0;*/

        foreach ($Eventos as $evento) {

            if ((strtotime($evento->fecha)) < (strtotime(date('Y-m-d')))) {


                /*$EventosFinalizados[$tamEvents]=$evento;
                $tamEvents++;*/
                if ($evento->estado == 'ACTIVO') {
                    Event::where('id', $evento->id)->update([
                        'estado' => 'REALIZADO',
                        'updated_at' => now(),
                    ]);
                }
            } else {

                if (((strtotime($evento->fecha)) == (strtotime(date('Y-m-d')))) && ($evento->hora_fin < $hora_actual)) {

                    /*$EventosFinalizados[$tamEvents]=$evento;
                    $tamEvents++;*/


                    if ($evento->estado == 'ACTIVO') {
                        Event::where('id', $evento->id)->update([
                            'estado' => 'REALIZADO',
                            'updated_at' => now(),
                        ]);
                    }
                }
            }
        }

        $Eventos = Event::where('estado', 'REALIZADO')->get();

        return response()->json([
            'status' => 'success',
            'data' => $Eventos
        ], 200);
    }

    public function FinalizedEvent($id)
    {
        /*date_default_timezone_set('Etc/GMT+4');*/
        date_default_timezone_set('America/Caracas');
        /*$hora_actual = date("H") . ":" . date("i") . ":" . date("s");

        $EventoFinalizado = Event::where('fecha', '<=', now())
            ->where('hora_fin', '<=', $hora_actual)
            ->where('id', $id)
            ->get();*/

        $EventoFinalizado = Event::where('id', $id)->where('estado', 'REALIZADO')->get();

        return response()->json([
            'status' => 'success',
            'data' => $EventoFinalizado
        ], 200);
    }


    public function getAsistences($id)
    {
        $data = DB::table('assistences')
            ->leftJoin('inscriptions', 'id_inscription', '=', 'inscriptions.id')
            ->leftJoin("postulations", 'id_postulation', '=', 'postulations.id')
            ->leftJoin("events", 'id_event', '=', 'events.id')
            ->leftJoin("emprendiments", 'id_emprend', '=', 'emprendiments.id')
            ->where('events.id', $id)
            ->select('emprendiments.id_user', 'id_emprend', 'emprendiments.nombre', 'id_inscription')
            ->get();
        /*->whereNotNull('inscriptions.id')*/
        foreach ($data as $emp) {
            $emp->images = DB::table('images')
                ->where('id_referencia', $emp->id_emprend)
                ->where('tipo', 'EMPRENDIMENTS')
                ->select('url')
                ->get();
        }
        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function EventsStand()
    {
        $events = Event::all();
        foreach ($events as $data) {
            $data->images = DB::table('images')
                ->where('id_referencia', $data->id)
                ->where('tipo', "EVENTS")
                ->select('url')
                ->get();
        }

        return response()->json([
            'status' => 'success',
            'data' => $events
        ], 200);
    }

    public function NextEvent()
    {
        date_default_timezone_set('America/Caracas');
        $hora_actual = date("H") . ":" . date("i") . ":" . date("s");

        $NextEvent = Event::select('fecha', 'nombre', 'motivo', 'places.name', 'hora_inicio')
            ->leftJoin('places', 'places.id', '=', 'id_lugar')
            ->where('fecha', '>=', now())
            ->orWhere('hora_inicio', '>', $hora_actual)
            ->orderBy('hora_inicio', 'desc')
            ->get();

        return response()->json([
            'status' => 'success',
            'data' => count($NextEvent) > 0 ? $NextEvent[0] : null
        ], 200);
    }

    public function emprendsNoInviteds($id)
    {
        $data = DB::table('notifications')
            ->select('id_user')
            ->where('tipo', 'EVENTS')
            ->where('id_referencia', $id)
            ->get();
        $data2 = DB::table('emprendiments')->get();

        $dato = array();
        $flag = false;

        foreach ($data2 as  $value2) {

            $flag = false;

            foreach ($data as $key => $value) {


                if ($value2->id_user == $value->id_user) {
                    $flag = true;
                    break;
                }
            }
            if (!$flag) {
                array_push($dato, $value2);
            }
        }


        return response()->json([
            'status' => 'success',
            'data' => $dato
        ], 200);
    }

    public function sendRestInvitation(Request $data)
    {

        foreach ($data->idsInv as $invitado) {
            DB::table('notifications')->insert([
                'tipo' => 'EVENTS',
                'id_referencia' => $data->id_event,
                'id_user' => $invitado,
                'created_at' => now()
            ]);
        };
    }

    public function unRegisterEvent(Request $data)
    {
        $user = Auth::user();

        $id_emprend = DB::table('emprendiments')->where('id_user', $user->id)->select('emprendiments.id')->get();
        /*echo $id_emprend[0]->id; echo " ".$id;*/
        $dato = DB::table('postulations')->where('id_emprend', $id_emprend[0]->id)->where('id_event', $data->id_event)->get();

        DB::table('postulations')
            ->where('id', $dato[0]->id)
            ->update(['codigo' => '']);

        /* echo "Data: ".$data;*/
        return response()->json([
            'status' => 'success',
            'data' => DB::table('inscriptions')->where('id_postulation', $dato[0]->id)->delete()
        ], 200);
    }
}
