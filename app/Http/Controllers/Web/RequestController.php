<?php

namespace App\Http\Controllers\Web;

use App\Request as Solicitud;
use App\Partner;
use App\User;
use App\Transaction;
use Mail;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use DB;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solicitudes = Solicitud::where('estado', 'PENDIENTE')->get();

        return response()->json([
            'status' => 'success',
            'data' => $solicitudes
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //* no se usa *//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lugar = $request['lugar2'];
        if ($lugar['name'] == '') {
            $lugar['name'] = $request['lugar'];
        }
        $idLugar = DB::table('places')->insertGetId([
            'name' => $lugar['name'],
            'lat' => $lugar['lat'],
            'lng' => $lugar['lng'],
            'created_at' => now()
        ]);

        $request['id_lugar'] = $idLugar;

        $dato = Solicitud::create($request->all());
        $id_request = $dato->id;

        foreach ($request->socios as $socio) {
            $id_partner = DB::table('partners')->insertGetId([
                'nombre' => $socio['nombre'],
                'apellido' => $socio['apellido'],
                'sexo' => $socio['sexo'],
                'fecha_nac' => $socio['fecha_nac'],
                'cedula' => $socio['cedula'],
                'estado_civil' => $socio['estado_civil'],
                'nivel_instruccion' => $socio['nivel_instruccion']
            ]);

            DB::table('partner_request')->insert([
                'id_request' => $id_request,
                'id_partner' => $id_partner,
            ]);
        }

        foreach ($request->images as $imagen) {
            if (isset($imagen['response'])) {
                $url = $imagen['response'];
            } else {
                if (isset($imagen['url'])) {
                    $url = $imagen['url'];
                } else {
                    $url = "images/imagen-no.png";
                }
            }

            DB::table('images')->insert([
                'url' => $url,
                'tipo' => 'REQUESTS',
                'id_referencia' => $id_request,
                'created_at' => now()
            ]);
        };

        foreach ([1, 2] as $adm) {
            DB::table('notifications')->insert([
                'tipo' => 'REQUESTS',
                'id_referencia' => $id_request,
                'id_user' => $adm,
                'created_at' => now()
            ]);
        };

        return response()->json([
            'status' => 'success',
            'data' => true
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Solicitud::find($id);
        $temp = DB::table('categories')->select('nombre')->where('id',$data->id_category)->get();
        $data->category_name=$temp[0]->nombre;
        $data->lugar = DB::table('places')
            ->where('places.id', $data->id_lugar)
            ->select('name', 'lat', 'lng')
            ->get();

        $data->images = DB::table('images')
            ->where('id_referencia', $id)
            ->where('tipo', "REQUESTS")
            ->select('url')
            ->get();

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //* no se usa *//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Solicitud::destroy($id);
    }


    public function requestsEvaluate(Request $request)
    {
        $solicitud = Solicitud::find($request['idRequest']);


        if ($request['accion'] == 'RECHAZAR') {

            DB::table('requests')
                ->where('id', $request['idRequest'] + 0)
                ->update(['estado' => 'RECHAZADA']);

            #$solicitud->estado='RECHAZADA';
            #$solicitud->save();
            $solicitud['mensaje'] = $request['mensaje'];
        } else {

            DB::table('requests')
                ->where('id', $request['idRequest'] + 0)
                ->update(['estado' => 'APROBADA']);
            #$solicitud->estado='APROBADA';
            #$solicitud->save();
            $solicitud['mensaje'] = 'Solicitud aprobada!';

            $user = User::create([
                'name'      => $solicitud->nombre,
                'email'     => $solicitud->correo,
                'status'    => 'ACTIVO',
                'password'  => bcrypt('123456'),
            ])->assignRole('emprend');

            $idUser = $user->id;

            $idEmprendiment = DB::table('emprendiments')->insertGetId([
                'nombre'                => $solicitud->nombre,
                'caracteristicas'       => $solicitud->caracteristicas,
                'descripcion'           => $solicitud->descripcion,
                'fecha_constitucion'    => $solicitud->fecha_constitucion,
                'id_places'             => $solicitud->id_lugar,
                'id_category'           => $solicitud->id_category,
                'id_user'               => $idUser,
                'created_at'            => now()
            ]);

            $socios = DB::table('partner_request')
                ->where('id_request', $solicitud->id)
                ->get();

            if (count($socios)) {
                foreach ($socios as $socio) {
                    DB::table('emprendiment_partner')->insert([
                        'id_emprendiment'  => $idEmprendiment,
                        'id_partner'       => $socio->id_partner,
                        'created_at'       => now()
                    ]);
                }
            }

            Transaction::insert([
                'fecha' => now(),
                'monto' => 10000,
                'descripcion' => "Pago de membresia",
                'id_user' => 2,
                'id_referencia' => $idEmprendiment,
                'automatica' => true,
                'id_concept' => 3,
                'tipo' => "REQUESTS",
                'created_at' => now(),
            ]);
        }

        $request['path_logo'] = asset('/images/logo_sisgemp.png');

        Mail::send('Emails.ContactMailView', [
            'solicitud' => $solicitud,
            'path_logo' => asset('/images/logo_sisgemp.png'),
            'nombre' => $solicitud->nombre,
            'mail' => $solicitud->correo,
            'mensaje' => $solicitud['mensaje'],
            'tipo' => 'REQUEST'
        ], function ($mail) use ($solicitud) {
            $mail->to($solicitud->correo)->subject($solicitud['mensaje']);
        });

        return response()->json([
            'status' => 'success',
            'data' => true
        ], 200);
    }
}
