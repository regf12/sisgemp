<?php

namespace App\Http\Controllers\Web;

use App\Emprendiment;
use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use DB;

class EmprendimentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emprendiments = Emprendiment::select(
            'emprendiments.*',
            'users.status'
        )->join('users', 'users.id', 'id_user')
            ->get();

        return response()->json([
            'status' => 'success',
            'data' => $emprendiments
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //* no se usa *//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = 'siiiiiiii exito';

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function myEmprend()
    {
        $user = Auth::user();

        $data = Emprendiment::where('id_user', $user->id)->get();

        $data[0]->imageProfile = DB::table('images')
            ->where('id_referencia', $data[0]->id)
            ->where('tipo', "EMPRENDIMENTS")
            ->select('url')
            ->get();

        $data[0]->imageBanner = DB::table('images')
            ->where('id_referencia', $data[0]->id)
            ->where('tipo', "BANNER")
            ->select('url')
            ->get();

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function myGroup()
    {
        $user = Auth::user();

        $data = Emprendiment::where('id_user', $user->id)->get();

        $data[0]->imageProfile = DB::table('images')
            ->where('id_referencia', $data[0]->id)
            ->where('tipo', "EMPRENDIMENTS")
            ->select('url')
            ->get();

        $data[0]->imageBanner = DB::table('images')
            ->where('id_referencia', $data[0]->id)
            ->where('tipo', "BANNER")
            ->select('url')
            ->get();

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //* no se usa *//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $id_place=0;
        DB::table('emprendiments')->where('id',$id)
                ->update([
                    'nombre' => $request->name,
                    'caracteristicas' => $request->caracteristic,
                    'descripcion' => $request->description,
                ]);
        if(!($request->addres=='')){
            $id_place=DB::table('places')->insertGetId([
                'lat' => $request->addres2['lat'],
                'lng' => $request->addres2['lng'],
                'name' => $request->addres2['name'],
                'created_at' => now(),
            ]);

            DB::table('emprendiments')->where('id',$id)->update([
                'id_places' => $id_place,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Emprendiment::destroy($id);
    }

    public function emprendimentPartners($id)
    {

        if ($id != 0) {

            $dato = Emprendiment::where('id', $id)
                ->select('id as emp_id', 'nombre as emp_nombre', 'caracteristicas', 'descripcion', 'fecha_constitucion')
                ->get();
            $data = $dato[0];
        } else {
            $user = Auth::user();
            $dato = Emprendiment::where('id_user', $user->id)
                ->select('id as emp_id', 'nombre as emp_nombre', 'caracteristicas', 'descripcion', 'fecha_constitucion')
                ->get();
            $data = $dato[0];
            $id = $dato[0]->emp_id;
        }




        $data['socios'] = DB::table('partners')
            ->leftJoin('emprendiment_partner', 'emprendiment_partner.id_partner', '=', 'partners.id')
            ->leftJoin('emprendiments', 'emprendiment_partner.id_emprendiment', '=', 'emprendiments.id')
            ->where("emprendiments.id", '=', $id)
            ->select('partners.nombre', 'partners.apellido', 'partners.cedula', 'partners.sexo', 'partners.fecha_nac', 'partners.estado_civil', 'partners.nivel_instruccion')
            ->get();

        $data['productos'] = DB::table('products')
            ->leftJoin('emprendiments', 'id_emprendiment', '=', 'emprendiments.id')
            ->where("emprendiments.id", '=', $id)
            ->select('products.nombre', 'products.descripcion', 'products.precio', 'products.estado', 'products.id')
            ->get();
        $data['direccion'] = DB::table('places')
            ->leftJoin('emprendiments', 'id_places', '=', 'places.id')
            ->where("emprendiments.id", '=', $id)
            ->select('places.name', 'places.lat', 'places.lng', 'places.id as id_place')
            ->get();

        foreach ($data['productos'] as $productos) {

            $productos->images = DB::table('images')
                ->where('id_referencia', $productos->id)
                ->where('tipo', "PRODUCTS")
                ->select('url')
                ->get();
        }



        $data->imageProfile = DB::table('images')
            ->where('id_referencia', $data['emp_id'])
            ->where('tipo', "EMPRENDIMENTS")
            ->select('url')
            ->get();

        $data->imageBanner = DB::table('images')
            ->where('id_referencia', $data['emp_id'])
            ->where('tipo', "BANNER")
            ->select('url')
            ->get();


        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function EmprendimentsStand()
    {

        $emprendiments = Emprendiment::all();


        foreach ($emprendiments as $data) {
            $data->images = DB::table('images')
                ->where('id_referencia', $data->id)
                ->where('tipo', "EMPRENDIMENTS")
                ->select('url')
                ->get();
        }

        return response()->json([
            'status' => 'success',
            'data' => $emprendiments
        ], 200);
    }
    public function fetchCategories()
    {

        $categories = DB::table('categories')->select('categories.*')->get();

        return response()->json([
            'status' => 'success',
            'data' => $categories
        ], 200);
    }

    public function GroupData()
    {
        $group = DB::table('groups')->select('nombre', 'mision', 'vision', 'descripcion', 'fecha_constitucion', 'id_places')->get();

        $banner = DB::table('images')->select('url')->where('id_referencia', 1)->where('tipo', "BANNERGROUP")->get();

        if (count($banner)) {
            $group[0]->image_banner = $banner[0];
        } else {
            $group[0]->image_banner = '';
        }

        $profile = DB::table('images')->select('url')->where('id_referencia', 1)->where('tipo', "GROUPS")->get();

        if (count($profile)) {
            $group[0]->image_profile = $profile[0];
        } else {
            $group[0]->image_profile = '';
        }

        $group[0]->lugar = DB::table('places')->select('name', 'lat', 'lng')->where('id', 1)->get();

        return response()->json([
            'status' => 'success',
            'data' => $group[0]
        ], 200);
    }

    public function suspend($id)
    {
        $data = User::find($id);
        $estado = 'INACTIVO';

        if ($data['status'] == 'INACTIVO') {
            $estado = 'ACTIVO';
        }

        $dato = User::where('id', $id)
            ->update([
                'status' => $estado,
            ]);

        return response()->json([
            'status' => 'success',
            'data' => $dato
        ], 200);
    }

    public function emprendsEditEvent($id)
    {
        $dato = DB::table('notifications')
        ->leftJoin('postulations','postulations.id_notification','=','notifications.id')
        ->leftJoin('emprendiments','emprendiments.id_user','=','notifications.id_user')
        ->where('notifications.tipo',"EVENTS")
        ->where('notifications.id_referencia','=',$id)
        ->whereNull('postulations.id')
        ->select(DB::raw("emprendiments.*"))
        ->get();

        return response()->json([
            'status' => 'success',
            'data' => $dato
        ], 200);
    }
}
