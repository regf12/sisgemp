<?php

namespace App\Http\Controllers\Web;

use App\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use DB;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::all();

        return response()->json([
            'status' => 'success',
            'data' => $reports
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Report::destroy($id);
    }

    ////////////////////////////////////////////

    public function reportsHistoricoGrupos()
    {
        #$data['antiguedad'] = 0;
        $data['emprendimientos'] = 0;
        $data['emprendimientosEliminados'] = 0;
        $data['productos'] = 0;
        $data['solicitudesRecibidas'] = 0;
        #$data['solicitudesAtendidas'] = 0;
        $data['solicitudesAceptadas'] = 0;
        $data['solicitudesRechazadas'] = 0;
        #$data['solicitudesPendientesConCita'] = 0;
        #$data['solicitudesPendientesSinCita'] = 0;
        #$data['citasReprogramadas'] = 0;
        #$data['citasPerdidas'] = 0;
        #$data['citasCanceladas'] = 0;
        $data['eventosRealizados'] = 0;
        $data['encuestasRealizadas'] = 0;
        $data['totalEntraDinero'] = 0;
        $data['totalSalidaDinero'] = 0;
        $data['totalMovEntraDinero'] = 0;
        $data['totalMovSalidaDinero'] = 0;
        $data['saldoActual'] = 0;



        #$data['antiguedad'] = 0;

        $data['emprendimientos'] = DB::table('emprendiments')->get()->count();

        #$data['emprendimientosEliminados'] = 0;

        $data['productos'] = DB::table('products')->get()->count();

        $data['solicitudesRecibidas'] = DB::table('requests')->get()->count();

        #$data['solicitudesAtendidas'] = DB::table('requests')->where('estado','!=', 'PENDIENTE')->get()->count();
        $data['solicitudesAceptadas'] = $data['emprendimientos'];
        #$data['solicitudesRechazadas'] = 0;
        #$data['solicitudesPendientesConCita'] = 0;
        #$data['solicitudesPendientesSinCita'] = 0;
        #$data['citasReprogramadas'] = DB::table('requests')->get()->count();
        #$data['citasCanceladas'] = 0;

        $data['eventosRealizados'] = DB::table('events')
            ->where('fecha', '<=', now())
            ->get()
            ->count();


        $data['encuestasRealizadas'] = DB::table('polls')->get()->count();

        $user = Auth::user();

        $data['totalMovEntraDinero'] = DB::table('transactions')->select(
            'transactions.*',
            'concepts.nombre as concept',
            'concepts.operacion',
            'concepts.role'
        )->join('concepts', 'concepts.id', 'id_concept')
            ->where('id_user', $user->id)
            ->where('concepts.operacion', '=', 'ENTRADA')
            ->orderBy('fecha', 'asc')
            ->get()
            ->count();

        $transactions = DB::table('transactions')
            ->select(
                'transactions.*',
                'concepts.nombre as concept',
                'concepts.operacion',
                'concepts.role'
            )->join('concepts', 'concepts.id', 'id_concept')
            ->where('id_user', $user->id)
            ->orderBy('fecha', 'asc')
            ->get();

        $count = 0;
        $saldo = 0;

        if (count($transactions)) {
            foreach ($transactions as $transaction) {
                if ($count == 0) {
                    $saldo = $transaction->monto;
                } else if ($transaction->operacion == 'ENTRADA') {
                    $saldo += $transaction->monto;
                    $data['totalEntraDinero'] += $transaction->monto;
                } else if ($transaction->operacion == 'SALIDA') {
                    $saldo -= $transaction->monto;
                    $data['totalSalidaDinero'] -= $transaction->monto;
                }
                #$transaction['saldo'] = $saldo;
                $count++;
                $data['saldoActual'] = $saldo;
            }
        }

        $data['totalMovSalidaDinero'] = DB::table('transactions')->select(
            'transactions.*',
            'concepts.nombre as concept',
            'concepts.operacion',
            'concepts.role'
        )->join('concepts', 'concepts.id', 'id_concept')
            ->where('id_user', $user->id)
            ->where('concepts.operacion', '=', 'SALIDA')
            ->orderBy('fecha', 'asc')
            ->get()
            ->count();


        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function reportsResumenGrupos()
    {

        $data['emprendimientos'] = 0;
        $data['emprendimientosEliminados'] = 0;
        $data['solicitudesRecibidas'] = 0;
        $data['solicitudesAtendidas'] = 0;
        $data['solicitudesAceptadas'] = 0;
        $data['solicitudesRechazadas'] = 0;

        $data['eventosRealizados'] = 0;

        $data['encuestasRealizadas'] = 0;
        $data['totalEntraDinero'] = 0;
        $data['totalSalidaDinero'] = 0;

        $data['citasPerdidas'] = 0;
        $data['citasCanceladas'] = 0;

        $data['emprendimientos'] = DB::table('emprendiments')->get()->count();

        $data['solicitudesRecibidas'] = DB::table('requests')->get()->count();

        $data['eventosRealizados'] = DB::table('events')
            ->where('fecha', '<=', now())
            ->get()
            ->count();


        $data['encuestasRealizadas'] = DB::table('polls')->get()->count();

        $user = Auth::user();

        $transactions = DB::table('transactions')
            ->select(
                'transactions.*',
                'concepts.nombre as concept',
                'concepts.operacion',
                'concepts.role'
            )->join('concepts', 'concepts.id', 'id_concept')
            ->where('id_user', $user->id)
            ->orderBy('fecha', 'asc')
            ->get();

        $count = 0;
        $saldo = 0;

        if (count($transactions)) {
            foreach ($transactions as $transaction) {
                if ($count == 0) {
                    $saldo = $transaction->monto;
                } else if ($transaction->operacion == 'ENTRADA') {
                    $saldo += $transaction->monto;
                    $data['totalEntraDinero'] += $transaction->monto;
                } else if ($transaction->operacion == 'SALIDA') {
                    $saldo -= $transaction->monto;
                    $data['totalSalidaDinero'] -= $transaction->monto;
                }
                #$transaction['saldo'] = $saldo;
                $count++;
                $data['saldoActual'] = $saldo;
            }
        }

        /*
        $data = DB::table('places')
        ->where('places.id',$data[0]->id_lugar)
        ->select('name','lat','lng')
        ->get();
        */

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function reportsResumenEmprendimientos()
    {
        $data['emprendimientosNombres'] = '';
        $data['eventosInvitados'] = 0;
        $data['eventosPostulados'] = 0;
        $data['eventosInscritos'] = 0;
        $data['eventosAsistidos'] = 0;
        $data['facturacion'] = 0;
        $data['encuestasRecibidas'] = 0;
        $data['encuestasRespondidas'] = 0;
        $data['cantidadPublicaciones'] = 0;
        $data['porcentajeFidelidad'] = 0;


        $emprendimientos = DB::table('emprendiments')
            ->get();

        foreach ($emprendimientos as $emprend) {

            $emprend->usuario = DB::table('users')->find($emprend->id_user);

            $emprend->invitaciones = DB::table('notifications')
                ->where('tipo', 'EVENTS')
                ->where('id_user', $emprend->usuario->id)
                ->get();

            $emprend->postulaciones = DB::table('postulations')
                ->where('id_emprend', $emprend->id)
                ->get();

            foreach ($emprend->postulaciones as $postulacion) {
                $emprend->inscripciones = DB::table('inscriptions')
                    ->where('id_postulation', $postulacion->id)
                    ->get();

                foreach ($emprend->inscripciones as $inscripcion) {
                    $emprend->asistencias = DB::table('inscriptions')
                        ->where('id_postulation', $postulacion->id)
                        ->get()->count();
                }
            }

            $emprend->respuestas = DB::table('poll_user')
                ->where('id_user', $emprend->usuario->id)
                ->get()->count();

            $emprend->productos = DB::table('products')
                ->where('id_emprendiment', $emprend->id)
                ->get()->count();
        }

        $data = $emprendimientos;

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function reportsResumenEventos()
    {
        $data['eventoNombre'] = '';
        $data['eventoLugar'] = '';
        $data['empInvitados'] = 0;
        $data['empPostulados'] = 0;
        $data['empInscritos'] = 0;
        $data['empAsistentes'] = 0;
        $data['ingresoInscripción'] = 0;
        $data['gastosPrevistos'] = 0;
        $data['gastosImprevistos'] = 0;
        $data['gastoTotal'] = 0;
        $data['montoGenerado'] = 0;

        /*
        $data = DB::table('places')
        ->where('places.id',$data[0]->id_lugar)
        ->select('name','lat','lng')
        ->get();
        */

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function reportsResumenTransacciones()
    {
        $data['antiguedad'] = 0;
        $data['emprendimientos'] = 0;
        $data['emprendimientosEliminados'] = 0;
        $data['productos'] = 0;
        $data['solicitudesRecibidas'] = 0;
        $data['solicitudesAtendidas'] = 0;
        $data['solicitudesAceptadas'] = 0;
        $data['solicitudesRechazadas'] = 0;
        $data['solicitudesPendientesConCita'] = 0;
        $data['solicitudesPendientesSinCita'] = 0;
        $data['citasReprogramadas'] = 0;
        $data['citasPerdidas'] = 0;
        $data['citasCanceladas'] = 0;
        $data['eventosRealizados'] = 0;
        $data['eventosRealizados'] = 0;
        $data['encuestasRealizadas'] = 0;
        $data['totalEntraDinero'] = 0;
        $data['totalSalidaDinero'] = 0;
        $data['saldoActual'] = 0;

        /*
        $data = DB::table('places')
        ->where('places.id',$data[0]->id_lugar)
        ->select('name','lat','lng')
        ->get();
        */

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function reportsResumenMensualidad()
    {
        $data['antiguedad'] = 0;
        $data['emprendimientos'] = 0;
        $data['emprendimientosEliminados'] = 0;
        $data['productos'] = 0;
        $data['solicitudesRecibidas'] = 0;
        $data['solicitudesAtendidas'] = 0;
        $data['solicitudesAceptadas'] = 0;
        $data['solicitudesRechazadas'] = 0;
        $data['solicitudesPendientesConCita'] = 0;
        $data['solicitudesPendientesSinCita'] = 0;
        $data['citasReprogramadas'] = 0;
        $data['citasPerdidas'] = 0;
        $data['citasCanceladas'] = 0;
        $data['eventosRealizados'] = 0;
        $data['eventosRealizados'] = 0;
        $data['encuestasRealizadas'] = 0;
        $data['totalEntraDinero'] = 0;
        $data['totalSalidaDinero'] = 0;
        $data['saldoActual'] = 0;

        /*
        $data = DB::table('places')
        ->where('places.id',$data[0]->id_lugar)
        ->select('name','lat','lng')
        ->get();
        */

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function reportCantEmprendsCategories()
    {
        $emprendiments = DB::table('emprendiments');

        $data = $emprendiments->leftJoin('categories', 'categories.id', '=', 'id_category')
            ->select(DB::raw('categories.nombre as Category, count(emprendiments.*) as cant'))
            ->groupBy('categories.nombre')
            ->orderBy('cant', 'asc')
            ->get();

        /*
        $data = DB::table('places')
        ->where('places.id',$data[0]->id_lugar)
        ->select('name','lat','lng')
        ->get();
        */

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function reportAprovedReject()
    {
        $data = DB::table('requests')->select(DB::raw("(select COUNT(*) from requests where estado = 'APROBADA') Taprobadas, (select COUNT(*) from requests where estado = 'RECHAZADA') as Trechazadas, (count(requests.*)) as total"))->get();

        /*echo $data;        */

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function reportCantProductsCategories()
    {
        $data = DB::table('categories')
            ->rightJoin('emprendiments', 'emprendiments.id_category', '=', 'categories.id')
            ->leftJoin('products', 'emprendiments.id', '=', 'products.id_emprendiment')
            ->where('products.estado', 'APPROVED')
            ->groupBy('categories.nombre')
            ->orderBy('categories.nombre', 'asc')
            ->select(DB::raw('categories.nombre, count(products.*) as cantidad'))
            ->get();

        /*echo $data;*/

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function reportItemsGastos()
    {
        $data = DB::table('expenses')
            ->select('tipo', DB::raw('sum(monto) as sumatoria'))
            ->groupBy('tipo')
            ->orderBy('sumatoria', 'asc')
            ->get();

        /*echo $data;*/

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function reportEmprendsAssistences()
    {
        $data = DB::table('postulations')
            ->selectRaw('emprendiments.nombre, id_emprend, count(id_emprend), (select count(events.*) from events) as total_Events')
            ->leftJoin('inscriptions','inscriptions.id_postulation','=','postulations.id')
            ->leftJoin('assistences','assistences.id_inscription','=','inscriptions.id')
            ->leftJoin('emprendiments','emprendiments.id','=','id_emprend')
            ->groupBy('emprendiments.nombre')
            ->groupBy('id_emprend')
            ->get();

        /*echo $data;*/

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }
}
