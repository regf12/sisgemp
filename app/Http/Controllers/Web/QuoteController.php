<?php

namespace App\Http\Controllers\Web;

use App\Quote;
use App\Request as Solicitud;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use DB;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quotes = Quote::all()->groupBy('fecha');

        foreach ($quotes as $fecha) {
            foreach ($fecha as $quote) {
                $solicitud = Solicitud::where('id', $quote['id_request'])->where('estado', '!=', 'RECHAZADA')
                    ->where('estado', '!=', 'APROBADA')->get();
                if (count($solicitud) > 0) {
                    $quote->solicitud = $solicitud[0]->nombre;
                }
            }
        }

        return response()->json([
            'status' => 'success',
            'data' => $quotes
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //* no se usa *//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $lugar = $request['lugar2'];
        $idLugar = DB::table('places')->insertGetId([
            'name' => $lugar['name'],
            'lat' => $lugar['lat'],
            'lng' => $lugar['lng'],
            'created_at' => now()
        ]);

        $request['id_lugar'] = $idLugar;
        Quote::insert([
            'fecha' => $request->fecha,
            'hora' => $request->hora,
            'id_lugar' => $request['id_lugar'],
            'id_request' => $request->id_request,
            'comentario' => $request->comentarios,
            'created_at' => now()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Quote::find($id);

        $data->lugar = DB::table('places')
            ->where('places.id', $data['id_lugar'])
            ->select('name', 'lat', 'lng')
            ->get();

        $data->solicitud = DB::table('requests')->find($data['id_request']);

        $data->solicitud->images = DB::table('images')
            ->where('id_referencia', $data->solicitud->id)
            ->where('tipo', "REQUESTS")
            ->select('url')
            ->get();

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //* no se usa *//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $lugar = $request['lugar2'];
        $idLugar = DB::table('places')->where('id', $request['id_placeEdit'])->update([
            'name' => $lugar['name'],
            'lat' => $lugar['lat'],
            'lng' => $lugar['lng'],
            'updated_at' => now()
        ]);

        $data = DB::table('quotes')->where('id', $id)->update([
            'fecha' => $request['fecha'],
            'hora' => $request['hora'],
            'comentario' => $request['comentarios'],
            'updated_at' => now(),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //* no se usa *//
    }

    public function oldquote($id)
    {
        $data = DB::table('quotes')->leftJoin('places', 'quotes.id_lugar', '=', 'places.id')
            ->leftJoin('requests', 'requests.id', '=', 'quotes.id_request')
            ->where('quotes.id', $id)
            ->select(DB::raw('places.name as lugarname,places.lat,places.lng,quotes.fecha,quotes.hora,quotes.comentario,quotes.id_request, places.id as id_lugar'))
            ->get();

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }
}
