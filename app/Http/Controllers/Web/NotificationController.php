<?php

namespace App\Http\Controllers\Web;

use App\Notification;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use DB;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $idUser = $user->id;

        $notifications = DB::select(DB::raw("
            SELECT * 
            FROM (
                SELECT *, ROW_NUMBER () OVER (ORDER BY created_at, visto)
                FROM notifications
            ) x 
            WHERE id_user = '$idUser';
        "));

        if ($user->role['super']) {
            $notifications = Notification::all();
        }

        return response()->json([
            'status' => 'success',
            'data' => $notifications
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //* no se usa *//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //* no se usa *//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //* no se usa *//
    }

    public function visto($id)
    {
        $dato = Notification::findOrFail($id);
        $dato->visto = true;
        $dato->save();

        return response()->json([
            'status' => 'success',
            'data' => 'success'
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //* no se usa *//
    }

    public function countNotifications()
    {
        $user = Auth::user();

        $countNotifications = Notification::where('id_user', $user->id)
            ->where('visto', false)
            ->count();

        #$countNotifications = DB::table('notifications')
        #->where('id_user',$user->id)
        #->where('visto',false)
        #->count();

        return response()->json([
            'status' => 'success',
            'data' => $countNotifications
        ], 200);
    }
}
