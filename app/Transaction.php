<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fecha',
        'hora',
        'monto',
        'descripcion',
        'automatica',
        'id_referencia',
        'id_concept',
        'id_user'
    ];
}
