<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
  /**
	 * Fields that are mass assignable
	 *
	 * @var array
	 */
	protected $fillable = [
		'visto',
		'tipo',
		'id_referencia',
		'id_user',
	];
}
