<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
  /**
	 * Fields that are mass assignable
	 *
	 * @var array
	 */
	protected $fillable = [
		'nombre',
		'correo',
		'caracteristicas',
		'descripcion',
		'direccion',
		'fecha_constitucion',
		'telefono', 
		'id_category'
	];
}
