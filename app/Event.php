<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
  /**
	 * Fields that are mass assignable
	 *
	 * @var array
	 */
	protected $fillable = [
		'nombre',
		'motivo',
		'costo',
		'gastos',
    
		'id_lugar',
		'cupos',
		'fecha',
		'hora_inicio',
		'hora_fin',
		
    'estado',
	];
}
