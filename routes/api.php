<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('get_images/{type}',            'Web\ImageController@getImages');
Route::get('events/stand',         'Web\EventController@EventsStand');
Route::get('events/next_event',         'Web\EventController@NextEvent');

Route::get('products/stand',         'Web\ProductController@ProductsStand');
Route::get('emprendiments/stand',         'Web\EmprendimentController@EmprendimentsStand');
Route::get('emprendiments_categories',         'Web\EmprendimentController@fetchCategories');

Route::resource('categories',               'Web\CategoryController');
Route::get('items',                         'Web\CategoryController@items');
Route::get('items_categories',              'Web\CategoryController@itemsCategories');

Route::post('image/upload',                'Web\ImageController@uploadImage');

///////////////////////////////////////////////
Route::resource('mail',         'MailController');

Route::resource('emprendiments',         'Web\EmprendimentController');
Route::get('emprendiments_edit_event/{id}',         'Web\EmprendimentController@emprendsEditEvent');
Route::resource('events',                   'Web\EventController');
Route::resource('products',              'Web\ProductController');

Route::group(['middleware' => ['auth:api']], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::patch('settings/profile', 'Settings\ProfileController@update');
    Route::patch('settings/password', 'Settings\PasswordController@update');


    /////////////////////////////////////////////// custom routes



    #Route::get('emprendiments',         'Web\EmprendimentController@index');
    #Route::get('emprendiments/{id}',    'Web\EmprendimentController@show');
    #Route::delete('emprendiments/{id}', 'Web\EmprendimentController@destroy');
    #Route::put('emprendiments/{id}',    'Web\EmprendimentController@update');
    #Route::post('emprendiments',        'Web\EmprendimentController@store');
    Route::put('suspend/{id}',               'Web\EmprendimentController@suspend');
    Route::get('my_emprend',                 'Web\EmprendimentController@myEmprend');
    Route::get('my_group',                   'Web\EmprendimentController@myGroup');
    Route::get('emprendiment_partners/{id}', 'Web\EmprendimentController@emprendimentPartners');
    Route::get('data_group', 'Web\EmprendimentController@GroupData');



    #Route::get('events',                   'Web\EventController@index');
    #Route::get('events/{id}',              'Web\EventController@show');
    #Route::delete('events/{id}',           'Web\EventController@destroy');
    #Route::put('events/{id}',              'Web\EventController@update');
    #Route::post('events',                  'Web\EventController@store');
    Route::get('postulation/{id}',              'Web\EventController@getPostulation');
    Route::get('postulations/{id}',             'Web\EventController@getPostulations');
    Route::get('events_invitations/{id}',       'Web\EventController@getInvitations');
    Route::get('events_inscriptions/{id}',      'Web\EventController@getInscriptions');
    Route::post('events_inscriptions',          'Web\EventController@createInscriptions');
    Route::post('events_inscription',          'Web\EventController@createInscription');
    Route::post('events_postulation',           'Web\EventController@postulationEmprend');
    Route::get('events_finalized',              'Web\EventController@FinalizedEvents');
    Route::get('events_finalized/{id}',         'Web\EventController@FinalizedEvent');
    Route::post('events_asistences',            'Web\EventController@createAsistences');
    Route::post('events_asistencia',            'Web\EventController@createAsistencia');
    Route::get('events_asistences/{id}',        'Web\EventController@getAsistences');
    Route::get('events_noInvited/{id}',        'Web\EventController@emprendsNoInviteds');
    Route::post('rest_invitations',            'Web\EventController@sendRestInvitation');
    Route::post('un_register_event',            'Web\EventController@unRegisterEvent');



    Route::resource('notifications',             'Web\NotificationController');
    #Route::get('notifications',             'Web\NotificationController@index');
    #Route::get('notifications/{id}',        'Web\NotificationController@show');
    #Route::delete('notifications/{id}',     'Web\NotificationController@destroy');
    #Route::put('notifications/{id}',        'Web\NotificationController@update');
    #Route::post('notifications',            'Web\NotificationController@store');
    Route::get('count_notifications',            'Web\NotificationController@countNotifications');
    Route::put('notifications_visto/{id}',       'Web\NotificationController@visto');


    Route::resource('payments',         'Web\PaymentController');
    #Route::get('payments',         'Web\PaymentController@index');
    #Route::get('payments/{id}',    'Web\PaymentController@show');
    #Route::delete('payments/{id}', 'Web\PaymentController@destroy');
    #Route::put('payments/{id}',    'Web\PaymentController@update');
    #Route::post('payments',        'Web\PaymentController@store');
    Route::get('meses_pagos/{id}',      'Web\PaymentController@mesesPagos');


    Route::resource('polls',         'Web\PollController');
    #Route::get('polls',         'Web\PollController@index');
    #Route::get('polls/{id}',    'Web\PollController@show');
    #Route::delete('polls/{id}', 'Web\PollController@destroy');
    #Route::put('polls/{id}',    'Web\PollController@update');
    #Route::post('polls',        'Web\PollController@store');
    Route::post('poll_response',     'Web\PollController@responsePoll');
    Route::get('polls_results/{id}', 'Web\PollController@PollResults');




    #Route::get('products',              'Web\ProductController@index');
    #Route::get('products/{id}',         'Web\ProductController@show');
    #Route::delete('products/{id}',      'Web\ProductController@destroy');
    #Route::put('products/{id}',         'Web\ProductController@update');
    #Route::post('products',             'Web\ProductController@store');
    Route::post('products/status_change',    'Web\ProductController@changeStatus');




    Route::resource('quotes',         'Web\QuoteController');
    Route::get('quotes_edit/{id}',    'Web\QuoteController@oldquote');
    #Route::get('quotes',         'Web\QuoteController@index');
    #Route::get('quotes/{id}',    'Web\QuoteController@show');
    #Route::delete('quotes/{id}', 'Web\QuoteController@destroy');
    #Route::put('quotes/{id}',    'Web\QuoteController@update');
    #Route::post('quotes',        'Web\QuoteController@store');


    Route::resource('requests',         'Web\RequestController');
    #Route::get('requests',         'Web\RequestController@index');
    #Route::get('requests/{id}',    'Web\RequestController@show');
    #Route::delete('requests/{id}', 'Web\RequestController@destroy');
    #Route::put('requests/{id}',    'Web\RequestController@update');
    #Route::post('requests',        'Web\RequestController@store');
    Route::post('requests_evaluate',     'Web\RequestController@requestsEvaluate');


    Route::resource('roles',               'Web\RoleController');
    #Route::get('roles',               'Web\RoleController@index');
    #Route::get('roles/{id}',          'Web\RoleController@show');
    #Route::delete('roles/{id}',       'Web\RoleController@roleDestroy');
    #Route::put('roles/{id}',          'Web\RoleController@update');
    #Route::post('roles',              'Web\RoleController@store');
    Route::get('permissions',              'Web\RoleController@permissions');
    Route::delete('permissions/{id}',      'Web\RoleController@permissionDestroy');


    Route::resource('transactions',         'Web\TransactionController');
    #Route::get('transactions',         'Web\TransactionController@index');
    #Route::get('transactions/{id}',    'Web\TransactionController@show');
    #Route::delete('transactions/{id}', 'Web\TransactionController@destroy');
    #Route::put('transactions/{id}',    'Web\TransactionController@update');
    #Route::post('transactions',        'Web\TransactionController@store');
    Route::get('concepts',                  'Web\TransactionController@concepts');


    #Route::resource('categories',               'Web\CategoryController');
    #Route::get('categories',               'Web\CategoryController@index');
    #Route::get('categories/{id}',          'Web\CategoryController@show');
    #Route::delete('categories/{id}',       'Web\CategoryController@destroy');
    #Route::put('categories/{id}',          'Web\CategoryController@update');
    #Route::post('categories',              'Web\CategoryController@store');
    #Route::get('items',                         'Web\CategoryController@items');
    #Route::get('items_categories',              'Web\CategoryController@itemsCategories');



    Route::resource('users',         'Web\UserController');
    #Route::get('users',         'Web\UserController@index');
    #Route::get('users/{id}',    'Web\UserController@show');
    #Route::delete('users/{id}', 'Web\UserController@destroy');
    #Route::put('users/{id}',    'Web\UserController@update');
    #Route::post('users',        'Web\UserController@store');


    Route::get('reports_historico_grupos',                'Web\ReportController@reportsHistoricoGrupos');
    Route::get('reports_resumen_grupos',                  'Web\ReportController@reportsResumenGrupos');
    Route::get('reports_resumen_emprendimientos',         'Web\ReportController@reportsResumenEmprendimientos');
    Route::get('reports_resumen_eventos',                 'Web\ReportController@reportsResumenEventos');
    Route::get('reports_resumen_transacciones',           'Web\ReportController@reportsResumenTransacciones');
    Route::get('reports_resumen_mensualidad',             'Web\ReportController@reportsResumenMensualidad');
    Route::get('reports_emprends_categories',             'Web\ReportController@reportCantEmprendsCategories');
    Route::get('reports_aproved_rejected',             'Web\ReportController@reportAprovedReject');
    Route::get('reports_products_categories',             'Web\ReportController@reportCantProductsCategories');
    Route::get('reports_items_gastos',             'Web\ReportController@reportItemsGastos');
    Route::get('reports_emprendiment_assistences',             'Web\ReportController@reportEmprendsAssistences');


    ///////////////////////////////////////////////



    
    Route::post('image/upload_galery',         'Web\ImageController@uploadGalery');
    Route::post('image/upload_emprendimiento', 'Web\ImageController@uploadImageEmprendimiento');
    Route::post('image/upload_banner',         'Web\ImageController@uploadImageBanner');


    ///////////////////////////////////////////////


    Route::resource('messages',          'ChatsController');
    #Route::get('messages',          'ChatsController@fetchMessages');
    #Route::post('messages',         'ChatsController@sendMessage');
    #Route::delete('messages/{id}',  'ChatsController@deleteMessage');
    #Route::put('messages/{id}',     'ChatsController@update');
    #Route::post('messages',         'ChatsController@store');


    /////////////////////////////////////////////// protected routes


    Route::group(['middleware' => ['role:super-admin|writer']], function () {
        //
    });

    Route::group(['middleware' => ['permission:publish articles|edit articles']], function () {
        //
    });

    Route::group(['middleware' => ['role_or_permission:super-admin|edit articles']], function () {
        //
    });
});


Route::group(['middleware' => ['guest:api',]], function () {

    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');

    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('email/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Auth\VerificationController@resend');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');

    /////////////////////////////////////////////// custom routes

    Route::post('requests',        'Web\RequestController@store');
    /*
    Route::get('events/{id}',              'Web\EventController@show');
    Route::get('products/{id}',         'Web\ProductController@show');
    Route::get('emprendiment_partners/{id}', 'Web\EmprendimentController@emprendimentPartners');
    Route::get('data_group', 'Web\EmprendimentController@GroupData');
*/
});
