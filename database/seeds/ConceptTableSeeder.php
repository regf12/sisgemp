<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConceptTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('concepts')->insert([ # concepto 1
            'nombre' => 'Capital inicial',
            'descripcion' => 'Monto de inicio, que se usara para el calculo de su saldo actual.',
            'operacion' => 'ENTRADA',
            'role' =>   'PUBLIC', # estados posibles: EMPREND ADMIN PUBLIC
        ]);

        DB::table('concepts')->insert([ # concepto 2
            'nombre' => 'Ventas',
            'descripcion' => 'Las ventas son actividades relacionadas con la venta o la cantidad de bienes o servicios vendidos en un período de tiempo determinado.',
            'operacion' => 'ENTRADA',
            'role' =>   'EMPREND', # estados posibles: EMPREND ADMIN PUBLIC
        ]);


        DB::table('concepts')->insert([ # concepto 3
            'nombre' => 'Cuota de afiliación',
            'descripcion' => 'pago que se recibe con la finalidad de que una persona pertenezca o sea afiliado de una organización social o en cualquier organización civil o empresarial, que requiera la afiliación a la institución, para ejercer derechos y obtener beneficios sobre ella.',
            'operacion' => 'ENTRADA',
            'role' =>   'ADMIN', # estados posibles: EMPREND ADMIN PUBLIC
        ]);

        DB::table('concepts')->insert([ # concepto 4
            'nombre' => 'Cuota de eventos',
            'descripcion' => 'pago recibido de las personas que participan en los eventos programados por una organización social o en cualquier organización civil o empresarial.',
            'operacion' => 'ENTRADA',
            'role' =>   'ADMIN', # estados posibles: EMPREND ADMIN PUBLIC
        ]);

        DB::table('concepts')->insert([ # concepto 5
            'nombre' => 'Otros',
            'descripcion' => 'operaciones o transacciones que generen ingresos a una organización social o en cualquier organización civil o empresarial.',
            'operacion' => 'ENTRADA',
            'role' =>   'PUBLIC', # estados posibles: EMPREND ADMIN PUBLIC
        ]);


        DB::table('concepts')->insert([ # concepto 6
            'nombre' => 'Pago de personal',
            'descripcion' => 'Gastos relacionados con los recursos humanos pertenecientes a una organizacion.',
            'operacion' => 'SALIDA',
            'role' =>   'ADMIN', # estados posibles: EMPREND ADMIN PUBLIC
        ]);

        DB::table('concepts')->insert([ # concepto 7
            'nombre' => 'Materia prima',
            'descripcion' => 'Gastos relacionados a la materia extraída de la naturaleza y que se transforma para elaborar materiales que más tarde se convertirán en bienes de consumo.',
            'operacion' => 'SALIDA',
            'role' =>   'EMPREND', # estados posibles: EMPREND ADMIN PUBLIC
        ]);

        DB::table('concepts')->insert([ # concepto 8
            'nombre' => 'Servicios',
            'descripcion' => 'Gastos relacionados a un conjunto de actividades que buscan satisfacer las necesidades de un cliente.',
            'operacion' => 'SALIDA',
            'role' =>   'EMPREND', # estados posibles: EMPREND ADMIN PUBLIC
        ]);

        DB::table('concepts')->insert([ # concepto 9
            'nombre' => 'Pago cuota de afiliación',
            'descripcion' => 'Gastos realizados con finalidad de que una persona pertenezca o sea afiliado de una organización social o en cualquier organización civil o empresarial, que requiera la afiliación a la institución, para ejercer derechos y obtener beneficios sobre ella.',
            'operacion' => 'SALIDA',
            'role' =>   'EMPREND', # estados posibles: EMPREND ADMIN PUBLIC
        ]);

        DB::table('concepts')->insert([ # concepto 10
            'nombre' => 'Pago de evento',
            'descripcion' => 'Gastos realizados por las personas que desean participan en los eventos programados por una organización social o en cualquier organización civil o empresarial.',
            'operacion' => 'SALIDA',
            'role' =>   'EMPREND', # estados posibles: EMPREND ADMIN PUBLIC
        ]);

        DB::table('concepts')->insert([ # concepto 11
            'nombre' => 'Gastos por evento',
            'descripcion' => 'Gastos relaionados a un suceso evento finalizado.',
            'operacion' => 'SALIDA',
            'role' =>   'ADMIN', # estados posibles: EMPREND ADMIN PUBLIC
        ]);

        DB::table('concepts')->insert([ # concepto 12
            'nombre' => 'Otros',
            'descripcion' => 'operaciones o transacciones que generen gastos para una organización social o en cualquier organización civil o empresarial.',
            'operacion' => 'SALIDA',
            'role' =>   'PUBLIC', # estados posibles: EMPREND ADMIN PUBLIC
        ]);

        DB::table('concepts')->insert([ # concepto 13
            'nombre' => 'Pago por mensualidad',
            'descripcion' => 'Pago recibido por permanencia mensual en el grupo',
            'operacion' => 'ENTRADA',
            'role' =>   'ADMIN', # estados posibles: EMPREND ADMIN PUBLIC
        ]);

        DB::table('concepts')->insert([ # concepto 14
            'nombre' => 'Pago de mensualidad',
            'descripcion' => 'Pago realizado por permanencia mensual en el grupo',
            'operacion' => 'SALIDA',
            'role' =>   'EMPREND', # estados posibles: EMPREND ADMIN PUBLIC
        ]);
    }
}
