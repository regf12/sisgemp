<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Request;

class RequestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('requests')->insert([ # solicitud 1
            'nombre' => 'Dulces delicias',
            'correo' => 'dulcesdelicias@gmail.co',
            'descripcion' => 'Los ingredientes con los que son realizados nuestros productos no contienen gluten, de tal forma que las personas  que  son intolerantes a esta proteína, puedan degustar de un buen dulce. Además, las decoraciones realizadas son personalizadas, de acuerdo a las necesidades y solicitudes de los clientes.',
            'fecha_constitucion' => date('Y-m-d', strtotime("-5 year", strtotime(now()))),
            'id_lugar' => 1,
            'telefono' => '04127775637',
            'id_category' => 1, # id de la categoria
            'estado' => 'APROBADA',
            'created_at' => now()
        ]);

        DB::table('requests')->insert([ # solicitud 2
            'nombre' => 'Mi pajarita perfecta',
            'correo' => 'pajaritaperfecta@gmail.co',
            'descripcion' => 'Las pajaritas que se ofrecen son realizadas con materiales reciclados como corcho, madera, vidrio, plásticos, telas y demás. Con entrega a domicilio, y diseños totalmente personalizados. Los clientes pueden proporcionar el material y hacemos la mano de obra, con total exclusividad.',
            'fecha_constitucion' => date('Y-m-d', strtotime("-6 year", strtotime(now()))),
            'id_lugar' => 1,
            'telefono' => '04124568738',
            'id_category' => 3, # id de la categoria
            'estado' => 'APROBADA',
            'created_at' => now()
        ]);

        DB::table('requests')->insert([ # solicitud 3
            'nombre' => 'M&R dance',
            'correo' => 'danceMR@gmail.co',
            'descripcion' => 'Somos Miriam y Rafael, profesores de baile que fusionamos pasos de diferentes géneros a un género en específico, por ejemplo, pasos o figuras de ballet a salsa casino y salsa en línea. Además, damos clases grupales, individuales, y a domicilio.',
            'fecha_constitucion' => date('Y-m-d', strtotime("-7 year", strtotime(now()))),
            'id_lugar' => 1,
            'telefono' => '04148984512',
            'id_category' => 10, # id de la categoria
            'estado' => 'APROBADA',
            'created_at' => now()
        ]);

        DB::table('requests')->insert([ # solicitud 4
            'nombre' => 'Tarima andante circo teatro',
            'correo' => 'tariamandante@gmail.co',
            'descripcion' => 'Ofrecemos la mejor diversión para tus eventos: recreación, animaciones, juegos, magias, pinta caritas, malabares, monociclos, globoflexia, obras teatrales,  mimos, zancos, escupe fuegos, tela acrobáticas, danza aérea, circo teatro, maquillaje personalizados para ocasiones especiales.',
            'fecha_constitucion' => date('Y-m-d', strtotime("-8 year", strtotime(now()))),
            'id_lugar' => 1,
            'telefono' => '04127459876',
            'id_category' => 10, # id de la categoria
            'estado' => 'APROBADA',
            'created_at' => now()
        ]);

        DB::table('requests')->insert([ # solicitud 5
            'nombre' => 'Creaciones valerias desings',
            'correo' => 'valerydesings@gmail.co',
            'descripcion' => 'Los accesorios que se ofrecen están elaborados a mano, cada uno realizado con un grado de detalle en el diseño, elección de los materiales, y adaptados a las exigencias de los clientes.',
            'fecha_constitucion' => date('Y-m-d', strtotime("-8 year", strtotime(now()))),
            'id_lugar' => 1,
            'telefono' => '04127459876',
            'id_category' => 10, # id de la categoria
            'estado' => 'APROBADA',
            'created_at' => now()
        ]);
        /*DB::table('requests')->insert([# solicitud 1
            'nombre' => 'Dulces delicias',
            'correo' => 'dulcesdelicias@gmail.com',
            'descripcion' => 'Los ingredientes con los que son realizados nuestros productos no contienen gluten, de tal forma que las personas  que  son intolerantes a esta proteína, puedan degustar de un buen dulce. Además, las decoraciones realizadas son personalizadas, de acuerdo a las necesidades y solicitudes de los clientes.',
            'fecha_constitucion' => date('Y-m-d', strtotime("-5 year", strtotime(now()))),
            'id_lugar' => 1,
            'telefono' => '04127775637',
            'id_category' => 1,# id de la categoria
            'created_at' => now()
        ]);

        DB::table('requests')->insert([# solicitud 2
            'nombre' => 'Mi pajarita perfecta',
            'correo' => 'pajaritaperfecta@gmail.com',
            'descripcion' => 'Las pajaritas que se ofrecen son realizadas con materiales reciclados como corcho, madera, vidrio, plásticos, telas y demás. Con entrega a domicilio, y diseños totalmente personalizados. Los clientes pueden proporcionar el material y hacemos la mano de obra, con total exclusividad.',
            'fecha_constitucion' => date('Y-m-d', strtotime("-6 year", strtotime(now()))),
            'id_lugar' => 1,
            'telefono' => '04124568738',
            'id_category' => 3,# id de la categoria
            'created_at' => now()
        ]);

        DB::table('requests')->insert([# solicitud 3
            'nombre' => 'M&R dance',
            'correo' => 'danceMR@gmail.com',
            'descripcion' => 'Somos Miriam y Rafael, profesores de baile que fusionamos pasos de diferentes géneros a un género en específico, por ejemplo, pasos o figuras de ballet a salsa casino y salsa en línea. Además, damos clases grupales, individuales, y a domicilio.',
            'fecha_constitucion' => date('Y-m-d', strtotime("-7 year", strtotime(now()))),
            'id_lugar' => 1,
            'telefono' => '04148984512',
            'id_category' => 10,# id de la categoria
            'created_at' => now()
        ]);

        DB::table('requests')->insert([# solicitud 4
            'nombre' => 'Tarima andante circo teatro',
            'correo' => 'tariamandante@gmail.com',
            'descripcion' => 'Ofrecemos la mejor diversión para tus eventos: recreación, animaciones, juegos, magias, pinta caritas, malabares, monociclos, globoflexia, obras teatrales,  mimos, zancos, escupe fuegos, tela acrobáticas, danza aérea, circo teatro, maquillaje personalizados para ocasiones especiales.',
            'fecha_constitucion' => date('Y-m-d', strtotime("-8 year", strtotime(now()))),
            'id_lugar' => 1,
            'telefono' => '04127459876',
            'id_category' => 10,# id de la categoria
            'created_at' => now()
        ]);

        DB::table('requests')->insert([# solicitud 5
            'nombre' => 'Creaciones valerias desings',
            'correo' => 'valerydesings@gmail.com',
            'descripcion' => 'Los accesorios que se ofrecen están elaborados a mano, cada uno realizado con un grado de detalle en el diseño, elección de los materiales, y adaptados a las exigencias de los clientes.',
            'fecha_constitucion' => date('Y-m-d', strtotime("-8 year", strtotime(now()))),
            'id_lugar' => 1,
            'telefono' => '04127459876',
            'id_category' => 10,# id de la categoria
            'created_at' => now()
        ]);*/
    }
}
