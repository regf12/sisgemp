<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Notification;

class NotificationTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'REQUESTS',
      'id_referencia'         => '1',
      'id_user'               => '2',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => true,
      'tipo'                  => 'REQUESTS',
      'id_referencia'         => '1',
      'id_user'               => '2',
    ]);


    DB::table('notifications')->insert([
      'visto'                 => true,
      'tipo'                  => 'POLLS',
      'id_referencia'         => '1',
      'id_user'               => '2',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'POLLS',
      'id_referencia'         => '1',
      'id_user'               => '2',
    ]);

    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'PRODUCTS',
      'id_referencia'         => '1',
      'id_user'               => '2',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => true,
      'tipo'                  => 'PRODUCTS',
      'id_referencia'         => '1',
      'id_user'               => '2',
    ]);

    //////////////////////////////invitaciones a eventos


    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '9',
      'id_user'               => '3',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '2',
      'id_user'               => '3',
    ]);

    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '4',
      'id_user'               => '3',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '5',
      'id_user'               => '3',
    ]);

    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '7',
      'id_user'               => '3',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '8',
      'id_user'               => '3',
    ]);



    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '9',
      'id_user'               => '4',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '2',
      'id_user'               => '4',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '3',
      'id_user'               => '4',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '4',
      'id_user'               => '4',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '5',
      'id_user'               => '4',
    ]);

    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '7',
      'id_user'               => '4',
    ]);




    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '9',
      'id_user'               => '5',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '2',
      'id_user'               => '5',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '3',
      'id_user'               => '5',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '4',
      'id_user'               => '5',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '5',
      'id_user'               => '5',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '6',
      'id_user'               => '5',
    ]);




    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '9',
      'id_user'               => '6',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '2',
      'id_user'               => '6',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '3',
      'id_user'               => '6',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '4',
      'id_user'               => '6',
    ]);

    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '6',
      'id_user'               => '6',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '7',
      'id_user'               => '6',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '8',
      'id_user'               => '6',
    ]);



    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '9',
      'id_user'               => '7',
    ]);


    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '7',
      'id_user'               => '7',
    ]);
    DB::table('notifications')->insert([
      'visto'                 => false,
      'tipo'                  => 'EVENTS',
      'id_referencia'         => '8',
      'id_user'               => '7',
    ]);
  }
}
