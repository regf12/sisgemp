<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CategoryTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    /****************************************************************************************
                                  seeder rubros
     */

    DB::table('categories')->insert([ # rubro 1
      'nombre'      =>    'Gastronomía',
      'descripcion' =>    "Estudio de la relación del ser humano con su alimentación y                    su medio ambiente o entorno.",
    ]);

    DB::table('categories')->insert([ # rubro 2
      'nombre'      =>    'Electrodomésticos/Artículos para el hogar',
      'descripcion' =>    "Se refiere tanto al trabajo del artesano, como al objeto o                     producto obtenido en el que cada pieza es distinta a las                        demás.",
    ]);

    DB::table('categories')->insert([ # rubro 3
      'nombre'      =>    'Bisutería/Joyería',
      'descripcion' =>    "Es el sector de la industria dedicado a la producción de                       fibras, hilados, telas y productos relacionados con la                          confección de ropa.",
    ]);

    DB::table('categories')->insert([ # rubro 4
      'nombre'      =>    'Textiles/Confecciones',
      'descripcion' =>    "Es cualquier sustancia o preparado que, sin tener la                           consideración legal de cosmético, biocida, producto sanitario                   o medicamento, está destinado a ser aplicado sobre la piel,                     dientes o mucosas del cuerpo humano con la finalidad de                         higiene o de estética, o para neutralizar o eliminar                            ectoparásitos.",
    ]);

    DB::table('categories')->insert([ # rubro 5
      'nombre'      =>    'Belleza/Estética/Higiene personal',
      'descripcion' =>    "Este amplio término abarca servicios tales como la vida                        asistida, cuidado de adultos, cuidados de larga duración,                       residencias de ancianos, cuidado en hospicios y atención en                     el hogar.",
    ]);

    DB::table('categories')->insert([ # rubro 6
      'nombre'      =>    'Libros',
      'descripcion' =>    "Es una forma de atención educativa donde el profesor apoya a                   un estudiante o a un grupo de estudiantes de una manera                         sistemática, por medio de la estructuración de objetivos,                       programas, organización por áreas, técnicas de enseñanza                        apropiadas e integración de grupos conforme a ciertos                           criterios y mecanismos de monitoreo y control, entre otros.",
    ]);

    DB::table('categories')->insert([ # rubro 7
      'nombre'      =>    'Tecnología ',
      'descripcion' =>    "Es aquel que se utiliza para designar a todas aquellas                         actividades relacionadas con el ocio y el divertimento de una                   persona o de un conjunto de personas.",
    ]);

    DB::table('categories')->insert([ # rubro 8
      'nombre'      =>    'Juguetería',
      'descripcion' =>    "Es el proceso de diseño, planificación y producción de                         congresos , festivales, ceremonias, fiestas, convenciones u                     otro tipo de reuniones, cada una de las cuales puede tener                      diferentes finalidades.",
    ]);

    DB::table('categories')->insert([ # rubro 9
      'nombre'      =>    'Servicios/Alquiler y Decoraciones de eventos o fiestas',
      'descripcion' =>    "Es el proceso de diseño, planificación y producción de                         congresos , festivales, ceremonias, fiestas, convenciones u                     otro tipo de reuniones, cada una de las cuales puede tener                      diferentes finalidades.",
    ]);

    DB::table('categories')->insert([ # rubro 10
      'nombre'      =>    'Entretenimiento',
      'descripcion' =>    "Es el proceso de diseño, planificación y producción de                         congresos , festivales, ceremonias, fiestas, convenciones u                     otro tipo de reuniones, cada una de las cuales puede tener                      diferentes finalidades.",
    ]);

    DB::table('categories')->insert([ # rubro 11
      'nombre'      =>    'Educativos y tutorías',
      'descripcion' =>    "Es el proceso de diseño, planificación y producción de                         congresos , festivales, ceremonias, fiestas, convenciones u                     otro tipo de reuniones, cada una de las cuales puede tener                      diferentes finalidades.",
    ]);

    DB::table('categories')->insert([ # rubro 12
      'nombre'      =>    'Transporte',
      'descripcion' =>    "Es el proceso de diseño, planificación y producción de                         congresos , festivales, ceremonias, fiestas, convenciones u                     otro tipo de reuniones, cada una de las cuales puede tener                      diferentes finalidades.",
    ]);

    DB::table('categories')->insert([ # rubro 13
      'nombre'      =>    'Viajes y Turismo',
      'descripcion' =>    "Es el proceso de diseño, planificación y producción de                         congresos , festivales, ceremonias, fiestas, convenciones u                     otro tipo de reuniones, cada una de las cuales puede tener                      diferentes finalidades.",
    ]);

    DB::table('categories')->insert([ # rubro 14
      'nombre'      =>    'Servicios/Accesorios para animales y mascotas',
      'descripcion' =>    "Es el proceso de diseño, planificación y producción de                         congresos , festivales, ceremonias, fiestas, convenciones u                     otro tipo de reuniones, cada una de las cuales puede tener                      diferentes finalidades.",
    ]);

    DB::table('categories')->insert([ # rubro 15
      'nombre'      =>    'Oficios (herrería, albañilería, tapicería, etc.)',
      'descripcion' =>    "Es el proceso de diseño, planificación y producción de                         congresos , festivales, ceremonias, fiestas, convenciones u                     otro tipo de reuniones, cada una de las cuales puede tener                      diferentes finalidades.",
    ]);

    DB::table('categories')->insert([ # rubro 16
      'nombre'      =>    'Mantenimiento/ artículos para vehículos',
      'descripcion' =>    "Es el proceso de diseño, planificación y producción de                         congresos , festivales, ceremonias, fiestas, convenciones u                     otro tipo de reuniones, cada una de las cuales puede tener                      diferentes finalidades.",
    ]);

    DB::table('categories')->insert([ # rubro 17
      'nombre'      =>    'Artículos/ accesorios deportivos',
      'descripcion' =>    "Es el proceso de diseño, planificación y producción de                         congresos , festivales, ceremonias, fiestas, convenciones u                     otro tipo de reuniones, cada una de las cuales puede tener                      diferentes finalidades.",
    ]);

    DB::table('categories')->insert([ # rubro 18
      'nombre'      =>    'Artes y antigüedades (cuadros, pintura, artesanía, etc.)',
      'descripcion' =>    "Es el proceso de diseño, planificación y producción de                         congresos , festivales, ceremonias, fiestas, convenciones u                     otro tipo de reuniones, cada una de las cuales puede tener                      diferentes finalidades.",
    ]);

    DB::table('categories')->insert([ # rubro 19
      'nombre'      =>    'Salud y equipamiento médicos',
      'descripcion' =>    "Es el proceso de diseño, planificación y producción de                         congresos , festivales, ceremonias, fiestas, convenciones u                     otro tipo de reuniones, cada una de las cuales puede tener                      diferentes finalidades.",
    ]);

    DB::table('categories')->insert([ # rubro 20
      'nombre'      =>    'Colecciones y hobbies',
      'descripcion' =>    "Es el proceso de diseño, planificación y producción de                         congresos , festivales, ceremonias, fiestas, convenciones u                     otro tipo de reuniones, cada una de las cuales puede tener                      diferentes finalidades.",
    ]);

    DB::table('categories')->insert([ # rubro 21
      'nombre'      =>    'Jardín (materos, plantas y semillas, etc.)',
      'descripcion' =>    "Es el proceso de diseño, planificación y producción de                         congresos , festivales, ceremonias, fiestas, convenciones u                     otro tipo de reuniones, cada una de las cuales puede tener                      diferentes finalidades.",
    ]);

    DB::table('categories')->insert([ # rubro 22
      'nombre'      =>    'Profesionales (abogados, contadores, etc.)',
      'descripcion' =>    "Es el proceso de diseño, planificación y producción de                         congresos , festivales, ceremonias, fiestas, convenciones u                     otro tipo de reuniones, cada una de las cuales puede tener                      diferentes finalidades.",
    ]);

    DB::table('categories')->insert([ # rubro 23
      'nombre'      =>    'Otros',
      'descripcion' =>    "Es el proceso de diseño, planificación y producción de                         congresos , festivales, ceremonias, fiestas, convenciones u                     otro tipo de reuniones, cada una de las cuales puede tener                      diferentes finalidades.",
    ]);


    /********************************************************************************
                                seeder categorias
     */

    /*
      DB::table('categories')->insert([# categoria 1
        'nombre' 			=> 		'Alimentos',
        'descripcion' => 		"Es cualquier sustancia normalmente ingerida por seres vivos 											con fines nutricionales, sociales y psicológicos.",
        'id_item' 		=> 		"1",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 2
        'nombre' 			=> 		'Bebidas',
        'descripcion' => 		"Es cualquier líquido que se ingiere y aunque la bebida por 										excelencia es el agua, el término se refiere por antonomasia a 									las bebidas alcohólicas y las bebidas gaseosas.",
        'id_item' 		=> 		"1",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 3
        'nombre' 			=> 		'Elaboración y Suministro',
        'descripcion' => 		"Elaboración de un producto que se hace transformando una o 										varias materias en sucesivas operaciones. Cuando se habla de 										suministro se hace referencia al acto y consecuencia de 												suministrar (es decir, proveer a alguien de algo que requiere).										",
        'id_item' 		=> 		"1",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 4
        'nombre' 			=> 		'Alquiler y Atención',
        'descripcion' => 		"Alquiler o arrendamiento es un contrato por el medio una 											parte se compromete a transferir temporalmente el uso de una 										cosa mueble o inmueble a una segunda parte que se compromete a 									su vez a pagar por ese uso un determinado precio.",
        'id_item' 		=> 		"1",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 5
        'nombre' 			=> 		'Arte',
        'descripcion' => 		"Es entendido generalmente como cualquier actividad o producto 										realizado con una finalidad estética y también comunicativa, 										 mediante la cual se expresan ideas, emociones y, en general, 									 una visión del mundo, a través de diversos recursos, como los 										plásticos, lingüísticos, sonoros, corporales y mixtos.",
        'id_item' 		=> 		"2",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 6
        'nombre' 			=> 		'Bisutería',
        'descripcion' => 		"Industria que produce objetos o materiales de adorno que no 										 están hechos de materiales preciosos.",
        'id_item' 		=> 		"2",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 7
        'nombre' 			=> 		'Juguetería',
        'descripcion' => 		"Es una tienda o comercio minorista dedicado a la venta de 											 juguetes a los consumidores.",
        'id_item' 		=> 		"2",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 8
        'nombre' 			=> 		'Calzado',
        'descripcion' => 		"Es la parte de la indumentaria utilizada para proteger los 										 pies.",
        'id_item' 		=> 		"3",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 9
        'nombre' 			=> 		'Ropa',
        'descripcion' => 		"se refiere a las prendas de diferentes texturas fabricadas en 										fabricas con telas o pieles de animales, usadas por el ser 											 humano para cubrir su cuerpo, incluyendo la ropa impermeable 									 y otra para protegerse del clima adverso.",
        'id_item' 		=> 		"3",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 10
        'nombre' 			=> 		'Lencería',
        'descripcion' => 		"Es un término genérico que designa a cierto tipo de ropa de 										cama, baño y de ropa interior. También se denomina así a la 										tienda donde se puede comprar este tipo de ropa. La lencería 										se caracteriza por los tejidos finos y elegantes, normalmente 									bordados o guarnecidos de encajes sutiles.",
        'id_item' 		=> 		"3",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 11
        'nombre' 			=> 		'Tejidos',
        'descripcion' => 		"Material que resulta de tejer o entrelazar hilos, 															especialmente el hecho con fibras textiles que se emplea para 									confeccionar ropa de cualquier clase.",
        'id_item' 		=> 		"3",# id del rubro
      ]);
      DB::table('categories')->insert([# categoria 12
        'nombre' 			=> 		'Accesorios de moda',
        'descripcion' => 		"El accesorio es siempre un auxiliar de aquello que es central 										y esto puede aplicarse a un sinfín de elementos de diferente 										tipo, Uno de esos ámbitos, quizás en el que más se utiliza el 									término, es el de la moda.",
        'id_item' 		=> 		"3",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 13
        'nombre' 			=> 		'Aseo personal',
        'descripcion' => 		"Es el conjunto de conocimientos y técnicas que aplican los 										individuos para el control de los factores que ejercen o 												pueden ejercer efectos nocivos sobre su salud.",
        'id_item' 		=> 		"4",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 14
        'nombre' 			=> 		'Salud',
        'descripcion' => 		"Es un estado de bienestar o de equilibrio que puede ser 												 visto a nivel subjetivo o a nivel objetivo.",
        'id_item' 		=> 		"4",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 15
        'nombre' 			=> 		'Limpieza',
        'descripcion' => 		"Acción de limpiar la suciedad, lo superfluo o lo perjudicial 									de algo. La limpieza también es muy importante en la vida 											cotidiana ya que sin ella muchos podríamos contraer 														enfermedades causadas por bacterias de la suciedad.",
        'id_item' 		=> 		"4",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 16
        'nombre' 			=> 		'Cuidado de Adultos Mayores',
        'descripcion' => 		"Cuidar de un adulto mayor es una gran responsabilidad, por 										eso es importante que la persona que se haga cargo debe ser 										una persona capacitada que lo trate con paciencia, con cariño 									para mejorar su bienestar físico, mental y social.",
        'id_item' 		=> 		"5",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 17
        'nombre' 			=> 		'Cuidado de Niños',
        'descripcion' => 		"Los cuidadores de niños cuidan de niños y bebés cuyos padres 									o tutores van a trabajar. Además de proporcionar los cuidados 									básicos con responsabilidades prácticas como lavarlos, 													vestirlos y darles de comer, fomentan el desarrollo social y 										educativo de los niños.",
        'id_item' 		=> 		"5",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 18
        'nombre' 			=> 		'Masajes',
        'descripcion' => 		"Es una forma de manipulación de las capas superficiales y 											profundas de los músculos del cuerpo utilizando varias 													técnicas, para mejorar sus funciones, ayudar en procesos de 										curación, disminuir la actividad refleja de los músculos, 											inhibir la excitabilidad motoneuronal, promover la relajación 									y el bienestar y como actividad recreativa.",
        'id_item' 		=> 		"5",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 19
        'nombre' 			=> 		'Peluquería/ Barbería',
        'descripcion' => 		"Es un local donde se ofrecen varios servicios estéticos, 											 principalmente el corte de pelo, pero también suelen 													 realizarse otros como afeitado, depilado, manicura, 														 pedicura, etc.",
        'id_item' 		=> 		"5",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 20
        'nombre' 			=> 		'Tareas dirigidas',
        'descripcion' => 		"Las tareas dirigidas son una especie de actividad 															extracurricular, consisten en clases particulares o de 													pequeños grupos de niños en las que se les refuerza lo visto 										en el colegio.",
        'id_item' 		=> 		"6",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 21
        'nombre' 			=> 		'Clases particulares',
        'descripcion' => 		"son clases que se dirigen a una única persona. Una clase 											particular, consiste en un profesor que transmite su saber y 										sus competencias a un único alumno.",
        'id_item' 		=> 		"6",# id del rubro
      ]);
      DB::table('categories')->insert([# categoria 22
        'nombre' 			=> 		'Clases de Música',
        'descripcion' => 		"La educación musical comprende todo lo que rodea los 													procesos de enseñanza y aprendizaje con respecto al ámbito de 									la música: el sistema educativo, los programas educativos, 											los métodos de enseñanza, las instituciones, los 																responsables, maestros y pedagogos, etc.",
        'id_item' 		=> 		"6",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 23
        'nombre' 			=> 		'Asesoría Académica',
        'descripcion' => 		"Es un servicio que te ofrecen docentes del departamento en 										el que estudias, su propósito es apoyarte en el proceso de 											estudio y orientarte sobre el uso de estrategias de 														aprendizaje individual y grupal.",
        'id_item' 		=> 		"6",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 24
        'nombre' 			=> 		'Música en Vivo',
        'descripcion' => 		"Es una actuación musical en «que se ejecutan composiciones 											sueltas»",
        'id_item' 		=> 		"7",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 25
        'nombre' 			=> 		'Sonido para fiesta y eventos',
        'descripcion' => 		"Cuando te encuentras organizando un evento, uno de los 												factores a tener en cuenta es el equipamiento que necesitarás 									para el sonido, ya sea para ambientar con música de fondo, 											bailar o simplemente amplificar las voces de los oradores.",
        'id_item' 		=> 		"7",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 26
        'nombre' 			=> 		'Bailes/Bailarines',
        'descripcion' => 		"Es un arte donde se utiliza el movimiento del cuerpo 													usualmente con música, como una forma de expresión y de 												interacción social, con fines de entretenimiento, artísticos 										o religiosos. Es el movimiento en el espacio que se realiza 										con una parte o todo el cuerpo del ejecutante, con cierto 											compás o ritmo como expresión de sentimientos individuales, o 									de símbolos de la cultura y la sociedad.",
        'id_item' 		=> 		"7",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 27
        'nombre' 			=> 		'Animadores/Recreadores',
        'descripcion' => 		"Es la persona que por interés personal y decidida vocación, 										se dedica a ser facilitador del proceso comunicativo de la 											recreación a través de la vivencia personal, con el apoyo de 										los medios y técnicas recreativas, anteponiendo las 														necesidades e intereses del grupo a las propias.",
        'id_item' 		=> 		"7",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 28
        'nombre' 			=> 		'Inflables',
        'descripcion' => 		"son estructuras que con aire adoptan diversas formas como 											castillos o laberintos y son muy utilizados en eventos 													infantiles así como también en campañas publicitarias.",
        'id_item' 		=> 		"7",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 29
        'nombre' 			=> 		'Carros de baterías',
        'descripcion' => 		"Carros de juguete que funcionan principalmente con una 												bateria, donde La batería es un acumulador y proporciona la 										energía eléctrica para el motor de arranque.",
        'id_item' 		=> 		"7",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 30
        'nombre' 			=> 		'Bartender',
        'descripcion' => 		"Barman (del inglés hombre de la barra) es la persona que 											atiende a los clientes en la barra de un bar, cervecería, 											taberna, cantina o local de ocio.",
        'id_item' 		=> 		"8",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 31
        'nombre' 			=> 		'Decoradores',
        'descripcion' => 		"Se denomina decorador a la persona dedicada a diseñar el 											interior de oficinas, viviendas o establecimientos 															comerciales con criterios estéticos y funcionales.",
        'id_item' 		=> 		"8",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 32
        'nombre' 			=> 		'Maestro de Ceremonias',
        'descripcion' => 		"Un maestro de ceremonias, MC abreviado o maestro de 														ceremonias o controlador de micrófono es el anfitrión oficial 									de una ceremonia, evento en escena o actuación similar.",
        'id_item' 		=> 		"8",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 33
        'nombre' 			=> 		'Meseros',
        'descripcion' => 		"Es la persona que tiene como oficio atender a los clientes 										de un establecimiento de hostelería, proporcionándoles 													alimentos, bebidas y asistencia durante la estancia.",
        'id_item' 		=> 		"8",# id del rubro
      ]);

      DB::table('categories')->insert([# categoria 34
        'nombre' 			=> 		'Seguridad',
        'descripcion' => 		"Estudio de la relación del ser humano con su alimentación y 										su medio ambiente o entorno.",
        'id_item' 		=> 		"8",# id del rubro
      ]);

*/
  }
}
