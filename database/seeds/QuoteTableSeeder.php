<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Quote;

class QuoteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('quotes')->insert([ # cita 1
            'fecha' => date('Y-m-d', strtotime("-1 day", strtotime(now()))),
            'hora' => '09:09:09',
            'id_lugar' => 1,
            'id_request' => 1, # id de la solicitud
            'created_at' => now()
        ]);
        DB::table('quotes')->insert([ # cita 2
            'fecha' => now(),
            'hora' => '10:09:09',
            'id_lugar' => 1,
            'id_request' => 2, # id de la solicitud
            'created_at' => now()
        ]);
        DB::table('quotes')->insert([ # cita 3
            'fecha' => date('Y-m-d', strtotime("+1 day", strtotime(now()))),
            'hora' => '11:09:09',
            'id_lugar' => 1,
            'id_request' => 3, # id de la solicitud
            'created_at' => now()
        ]);
        DB::table('quotes')->insert([ # cita 4
            'fecha' => date('Y-m-d', strtotime("+1 day", strtotime(now()))),
            'hora' => '14:09:09',
            'id_lugar' => 1,
            'id_request' => 4, # id de la solicitud
            'created_at' => now()
        ]);
        DB::table('quotes')->insert([ # cita 5
            'fecha' => date('Y-m-d', strtotime("+1 day", strtotime(now()))),
            'hora' => '15:09:09',
            'id_lugar' => 1,
            'id_request' => 5, # id de la solicitud
            'created_at' => now()
        ]);
    }
}
