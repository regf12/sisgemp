<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('places')->insertGetId([
            'name' => 'Cumana',
            'lat' => 10.459453269048117,
            'lng' => -64.1762924194336,
            'created_at' => now()
        ]);

        DB::table('groups')->insertGetId([
            'nombre' => 'Udo Productva',
            'descripcion' => 'Bienvenidos!',
            'mision' => 'Mision',
            'vision' => 'Vision',
            'fecha_constitucion' => date('Y-m-d', strtotime("-8 year", strtotime(now()))),
            'id_places' => 1,
            'created_at' => now()
        ]);

        $this->call(PermissionTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        $this->call(CategoryTableSeeder::class);

        DB::table('messages')->insertGetId([
            'user_id' => 2,
            'message' => 'Bienvenidos!',
            'created_at' => now()
        ]);

        $this->call(ImageTableSeeder::class);

        $this->call(RequestTableSeeder::class);
        $this->call(QuoteTableSeeder::class);

        $this->call(EventTableSeeder::class);
        $this->call(EmprendimentTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(PartnerTableSeeder::class);

        $this->call(ConceptTableSeeder::class);
        $this->call(TransactionTableSeeder::class);
        $this->call(PaymentTableSeeder::class);

        $this->call(PollTableSeeder::class);

        $this->call(NotificationTableSeeder::class);
    }
}
