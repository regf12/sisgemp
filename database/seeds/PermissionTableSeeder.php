<?php


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class PermissionTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

    $this->permissions();
    $this->roles();
  }

  public function permissions()
  {
    $permissions = [

      #'emprendiments',
      'emprendiments-list',
      #'emprendiments-create',
      'emprendiments-edit',
      'emprendiments-show',
      #'emprendiments-delete',


      #'events',
      'events-list',
      'events-create',
      'events-edit',
      'events-show',
      #'events-delete',


      'forum',
      #'messages-list',
      #'messages-create',
      #'messages-delete',


      'notifications',
      #'notifications-list',
      #'notifications-show',
      #'notifications-visto',


      'payments',
      #'payments-list',
      #'payments-create',
      #'payments-edit',
      #'payments-show',
      #'payments-delete',


      #'polls',
      'polls-list',
      'polls-create',
      'polls-show',
      #'polls-delete',


      #'products',
      'products-list',
      'products-create',
      'products-edit',
      'products-show',
      'products-delete',


      #'quotes',
      'quotes-list',
      'quotes-create',
      'quotes-edit',
      'quotes-show',


      #'requests',
      'requests-list',
      'requests-create',
      'requests-edit',
      'requests-show',


      #'roles',
      'roles-list',
      #'roles-create',
      'roles-edit',
      'roles-show',
      'roles-delete',
      'roles-permissions-list',
      'roles-permissions-delete',


      'transactions',
      #'transactions-list',
      #'transactions-create',
      #'transactions-edit',
      #'transactions-show',
      #'transactions-delete',


      #'users',
      'users-list',
      'users-create',
      'users-edit',
      'users-show',
      'users-delete',


      #'images',
      'images-upload'

    ];

    foreach ($permissions as $permission) {
      Permission::create(['name' => $permission]);
    }
  }

  public function roles()
  {
    Role::create(['name' => 'super'])
      ->givePermissionTo(Permission::all());

    Role::create(['name' => 'admin'])
      ->givePermissionTo([
        'emprendiments-list',
        'emprendiments-show',

        'events-list',
        'events-create',
        'events-edit',
        'events-show',

        'forum',

        'notifications',

        'payments',

        'polls-list',
        'polls-create',
        'polls-show',

        'products-list',
        'products-show',

        'quotes-list',
        'quotes-create',
        'quotes-edit',
        'quotes-show',

        'requests-list',
        'requests-show',

        'roles-list',
        'roles-edit',
        'roles-show',
        'roles-delete',
        'roles-permissions-list',
        'roles-permissions-delete',

        'transactions',

        'users-list',
        'users-create',
        'users-edit',
        'users-show',
        'users-delete',

        'images-upload'
      ]);

    Role::create(['name' => 'emprend'])
      ->givePermissionTo([
        'emprendiments-list',
        'emprendiments-edit',
        'emprendiments-show',

        'events-list',
        'events-show',

        'forum',

        'notifications',

        'polls-list',
        'polls-show',

        'products-list',
        'products-create',
        'products-edit',
        'products-show',
        'products-delete',

        'quotes-show',

        'requests-create',

        'users-list',
        'users-edit',
        'users-show',

        'images-upload'
      ]);

    /*Role::create(['name' => 'user'])
        ->givePermissionTo([
        	'product-list',
        ]);*/

    Role::create(['name' => 'banned']);
  }
}
