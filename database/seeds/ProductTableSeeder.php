<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Product;

class ProductTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    Product::create([ # producto 1
      'nombre'             => 'Chocofrutas',
      'descripcion'       => 'Arreglo de chocofresas, duraznos y rosas',
      'precio'             => '30000',
      'id_emprendiment'    => '1', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now()
    ]);

    Product::create([ # producto 1
      'nombre'             => 'Dulce desayuno sorpresa',
      'descripcion'       => 'Delicioso desayuno para soprender a esa persona especial',
      'precio'             => '50000',
      'id_emprendiment'    => '1', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now()
    ]);

    Product::create([ # producto 1
      'nombre'             => 'Dulce love',
      'descripcion'       => 'Tú solo imagina y nosotros lo creamos',
      'precio'             => '45000',
      'id_emprendiment'    => '1', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);

    Product::create([ # producto 1
      'nombre'             => 'Chocofresas',
      'descripcion'       => 'Arreglo de fresas cubiertas de chocolate',
      'precio'             => '25000',
      'id_emprendiment'    => '1', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);

    Product::create([ # producto 1
      'nombre'            => 'Torta de chocolate',
      'descripcion'       => 'Deliciosa torta de chocolate con chocofresas',
      'precio'            => '25000',
      'id_emprendiment'   => '1', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);

    Product::create([ # producto 1
      'nombre'            => 'Dulce desayuno papá',
      'descripcion'       => 'Delicioso desayuno para soprenderlo',
      'precio'            => '50000',
      'id_emprendiment'   => '1', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);

    Product::create([ # producto 1
      'nombre'            => 'Caja dulce tentación',
      'descripcion'       => 'Caja de chocolate rellena de chocofresas',
      'precio'            => '50000',
      'id_emprendiment'   => '1', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);

    Product::create([ # producto 1
      'nombre'            => 'Caja san valentin',
      'descripcion'       => 'Linda caja con almohada con mensaje y rosas',
      'precio'            => '50000',
      'id_emprendiment'   => '1', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);

    Product::create([ # producto 1
      'nombre'            => 'Caja dia especial',
      'descripcion'       => 'Linda caja de chocofresas y rosas',
      'precio'            => '50000',
      'id_emprendiment'   => '1', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);

    Product::create([ # producto 1
      'nombre'            => 'Caja de bombones',
      'descripcion'       => 'Caja de bombones de chocofresas cubiertas de maní',
      'precio'            => '50000',
      'id_emprendiment'   => '1', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);




    Product::create([ # producto 2
      'nombre'             => 'Amor sin género',
      'descripcion'       => 'Pajarita y broche',
      'precio'             => '1000',
      'id_emprendiment'    => '2', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);

    Product::create([ # producto 2
      'nombre'             => 'Pajaritas de tela',
      'descripcion'       => 'Pajaritas hechas con telas estampadas',
      'precio'             => '1500',
      'id_emprendiment'    => '2', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);

    Product::create([ # producto 2
      'nombre'             => 'Pajarita batman',
      'descripcion'       => 'Pajarita de forma de batman elaborada artesanalmente',
      'precio'             => '60000',
      'id_emprendiment'    => '2', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);

    Product::create([ # producto 2
      'nombre'             => 'Pajarita hard rock',
      'descripcion'       => 'Porque el arte, estilo y buen gusto no tiene límites',
      'precio'             => '12000',
      'id_emprendiment'    => '2', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);

    Product::create([ # producto 2
      'nombre'             => 'Pajarita tetris wood',
      'descripcion'       => 'Pajarita de madera radiada',
      'precio'             => '10000',
      'id_emprendiment'    => '2', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);

    Product::create([ # producto 2
      'nombre'             => 'Pajarita 3D',
      'descripcion'       => 'Pajarita impresión 3D, granate negro',
      'precio'             => '5000',
      'id_emprendiment'    => '2', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);

    Product::create([ # producto 2
      'nombre'            => 'Pajarita plast instar',
      'descripcion'       => 'Pajarita elaborada de plastico decorada con estrellas',
      'precio'            => '5000',
      'id_emprendiment'   => '2', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);

    Product::create([ # producto 2
      'nombre'            => 'Pajarita escocés',
      'descripcion'       => 'Pajarita elaborada de tela',
      'precio'            => '5000',
      'id_emprendiment'   => '2', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);

    Product::create([ # producto 2
      'nombre'            => 'Pajarita 3D',
      'descripcion'       => 'Nuestra primera pajarita 3D',
      'precio'            => '5000',
      'id_emprendiment'   => '2', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);









    Product::create([ # producto 3
      'nombre'             => 'Baile latino',
      'descripcion'       => 'Presentación de bailes con diversas melodías y ritmo',
      'precio'             => '45000',
      'id_emprendiment'    => '3', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);





    Product::create([ # producto 4
      'nombre'             => 'Más alla de los cuentos',
      'descripcion'       => 'Encuetro de teatro para niños',
      'precio'             => '25000',
      'id_emprendiment'    => '4', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);





    Product::create([ # producto 5
      'nombre'             => 'Collar original',
      'descripcion'       => 'Si lo tuyo es la exclusividad, este collar es lo que buscas, unica pieza',
      'precio'             => '100000',
      'id_emprendiment'    => '5', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);




/*
    Product::create([ # producto 6
      'nombre'             => 'Seguridad garantizada',
      'descripcion'       => 'Prestamos servicios de suguridad, para tus eventos, fiestas o celebraciones',
      'precio'             => '150000',
      'id_emprendiment'    => '6', # id del emprendimiento
      'estado'            => 'APPROVED', # estados posibles: PENDIENTE APROBADO RECHAZADO
      'created_at'        => now(),
    ]);*/
  }
}
