<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Event;

class EventTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Event::create([ # evento 1
			'nombre'				=> 'Celebracion especial dia del niño',
			'motivo'				=> 'Dia del niño',
			'costo'					=> 5000,
			'gastos'				=> 3000,
			'id_lugar'			=> 1,
			'cupos'					=> 30,
			'fecha'					=> now(),
			'hora_inicio'		=> '10:00',
			'hora_fin'			=> '15:30',
			'estado' 				=> 'ACTIVO', # estados posibles: ACTIVO CANCELADO REALIZADO
			'created_at' 		=> now(),
		]);

		Event::create([ # evento 2
			'nombre'				=> 'Celebremos juntos el dia del padre',
			'motivo'				=> 'Dia del Padre',
			'costo'					=> 8000,
			'gastos'				=> 4000,
			'id_lugar'			=> 1,
			'cupos'					=> 20,
			'fecha'					=> date('Y-m-d', strtotime("+1 day", strtotime(now()))),
			'hora_inicio'		=> '10:00',
			'hora_fin'			=> '15:30',
			'estado' 				=> 'ACTIVO', # estados posibles: ACTIVO CANCELADO REALIZADO
			'created_at' 		=> now(),
		]);

		Event::create([ # evento 3
			'nombre'				=> 'Fin de semana de terror',
			'motivo'				=> 'Halloween',
			'costo'					=> 7000,
			'gastos'				=> 5000,
			'id_lugar'			=> 1,
			'cupos'					=> 40,
			'fecha'					=> date('Y-m-d', strtotime("+2 day", strtotime(now()))),
			'hora_inicio'		=> '10:00',
			'hora_fin'			=> '15:30',
			'estado' 				=> 'ACTIVO', # estados posibles: ACTIVO CANCELADO REALIZADO
			'created_at' 		=> now(),
		]);

		Event::create([ # evento 4
			'nombre'				=> 'Somos gente que trabaja',
			'motivo'				=> 'Dia internacional del trabajador',
			'costo'					=> 9000,
			'gastos'				=> 2000,
			'id_lugar'			=> 1,
			'cupos'					=> 45,
			'fecha'					=> date('Y-m-d', strtotime("+3 day", strtotime(now()))),
			'hora_inicio'		=> '10:00',
			'hora_fin'			=> '15:30',
			'estado' 				=> 'ACTIVO', # estados posibles: ACTIVO CANCELADO REALIZADO
			'created_at' 		=> now(),
		]);

		Event::create([ # evento 5
			'nombre'				=> 'Celebracion especial de aniversario',
			'motivo'				=> 'Aniversario de nuestro grupo',
			'costo'					=> 5000,
			'gastos'				=> 2000,
			'id_lugar'			=> 1,
			'cupos'					=> 45,
			'fecha'					=> date('Y-m-d', strtotime("+4 day", strtotime(now()))),
			'hora_inicio'		=> '10:00',
			'hora_fin'			=> '15:30',
			'estado' 				=> 'ACTIVO', # estados posibles: ACTIVO CANCELADO REALIZADO
			'created_at' 		=> now(),
		]);

		Event::create([ # evento 6
			'nombre'				=> 'Con amor para nuestras madres',
			'motivo'				=> 'Dia de la madre',
			'costo'					=> 4000,
			'gastos'				=> 2000,
			'id_lugar'			=> 1,
			'cupos'					=> 25,
			'fecha'					=> date('Y-m-d', strtotime("+5 day", strtotime(now()))),
			'hora_inicio'		=> '10:00',
			'hora_fin'			=> '15:30',
			'estado' 				=> 'ACTIVO', # estados posibles: ACTIVO CANCELADO REALIZADO
			'created_at' 		=> now(),
		]);

		Event::create([ # evento 7
			'nombre'				=> 'Cuidemos nuestros árboles',
			'motivo'				=> 'Dia internacional del árbol',
			'costo'					=> 8000,
			'gastos'				=> 4000,
			'id_lugar'			=> 1,
			'cupos'					=> 25,
			'fecha'					=> date('Y-m-d', strtotime("+6 day", strtotime(now()))),
			'hora_inicio'		=> '10:00',
			'hora_fin'			=> '15:30',
			'estado' 				=> 'ACTIVO', # estados posibles: ACTIVO CANCELADO REALIZADO
			'created_at' 		=> now(),
		]);

		Event::create([ # evento 8
			'nombre'				=> 'Con amor a nuestras bellas mujeres',
			'motivo'				=> 'Dia internacional de la mujer',
			'costo'					=> 8000,
			'gastos'				=> 4000,
			'id_lugar'			=> 1,
			'cupos'					=> 25,
			'fecha'					=> date('Y-m-d', strtotime("+7 day", strtotime(now()))),
			'hora_inicio'		=> '10:00',
			'hora_fin'			=> '15:30',
			'estado' 				=> 'ACTIVO', # estados posibles: ACTIVO CANCELADO REALIZADO
			'created_at' 		=> now(),
		]);


		Event::create([ # evento 9
			'nombre'				=> 'Disfrutemos del regreso a clases',
			'motivo'				=> 'Incio del año escolar',
			'costo'					=> 7000,
			'gastos'				=> 5000,
			'id_lugar'			=> 1,
			'cupos'					=> 40,
			'fecha'					=> date('Y-m-d', strtotime("+8 day", strtotime(now()))),
			'hora_inicio'		=> '10:00',
			'hora_fin'			=> '15:30',
			'estado' 				=> 'ACTIVO', # estados posibles: ACTIVO CANCELADO REALIZADO
			'created_at' 		=> now(),
		]);

		Event::create([ # evento 10
			'nombre'				=> 'Nos vamos de vacaciones',
			'motivo'				=> 'Fin del año escolar',
			'costo'					=> 7000,
			/*'gastos'				=> 5000,*/
			'id_lugar'			=> 1,
			'cupos'					=> 40,
			'fecha'					=> date('Y-m-d', strtotime("-1 day", strtotime(now()))),
			'hora_inicio'		=> '10:00',
			'hora_fin'			=> '15:30',
			'estado' 				=> 'ACTIVO', # estados posibles: ACTIVO CANCELADO REALIZADO
			'created_at' 		=> now(),
		]);
	}
}
