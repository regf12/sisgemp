<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PartnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*DB::table('partners')->insert([# socio 1 emprendimiento 1
            'nombre' => ' Jessica',
            'apellido' => 'Ruíz',
            'cedula' => '23947584',
            'fecha_nac' => date('Y-m-d', strtotime("-15 year", strtotime(now()))),
            'sexo' => 'FEMENINO',# estados posibles: MASCULINO FEMENINO INDEFINIDO
            'estado_civil' => 'SOLTERO',# estados posibles: SOLTERO CASADO
            'nivel_instruccion' => 'PROFESIONAL',# estados posibles: SIN ESTUDIO PRIMARIA SECUNDARIA TSU PROFESIONAL SUPERIOR
            'created_at' => now()
        ]);

        DB::table('partners')->insert([# socio 2 emprendimiento 1
            'nombre' => 'José',
            'apellido' => 'Hernández',
            'cedula' => '23265783',
            'fecha_nac' => date('Y-m-d', strtotime("-15 year", strtotime(now()))),
            'sexo' => 'FEMENINO',# estados posibles: MASCULINO FEMENINO INDEFINIDO
            'estado_civil' => 'SOLTERO',# estados posibles: SOLTERO CASADO
            'nivel_instruccion' => 'PROFESIONAL',# estados posibles: SIN ESTUDIO PRIMARIA SECUNDARIA TSU PROFESIONAL SUPERIOR
            'created_at' => now()
        ]);

        DB::table('partners')->insert([# socio 1 emprendimiento 2
            'nombre' => 'Laura',
            'apellido' => 'Castillejo',
            'cedula' => '25676543',
            'fecha_nac' => date('Y-m-d', strtotime("-15 year", strtotime(now()))),
            'sexo' => 'FEMENINO',# estados posibles: MASCULINO FEMENINO INDEFINIDO
            'estado_civil' => 'SOLTERO',# estados posibles: SOLTERO CASADO
            'nivel_instruccion' => 'PROFESIONAL',# estados posibles: SIN ESTUDIO PRIMARIA SECUNDARIA TSU PROFESIONAL SUPERIOR
            'created_at' => now()
        ]);

        DB::table('partners')->insert([# socio 2 emprendimiento 2
            'nombre' => 'Marina',
            'apellido' => 'Betancourt',
            'cedula' => '22345675',
            'fecha_nac' => date('Y-m-d', strtotime("-15 year", strtotime(now()))),
            'sexo' => 'FEMENINO',# estados posibles: MASCULINO FEMENINO INDEFINIDO
            'estado_civil' => 'SOLTERO',# estados posibles: SOLTERO CASADO
            'nivel_instruccion' => 'PROFESIONAL',# estados posibles: SIN ESTUDIO PRIMARIA SECUNDARIA TSU PROFESIONAL SUPERIOR
            'created_at' => now()
        ]);

        DB::table('partners')->insert([# socio 1 emprendimiento 3
            'nombre' => 'Miriam',
            'apellido' => 'Briceño',
            'cedula' => '17320568',
            'fecha_nac' => date('Y-m-d', strtotime("-15 year", strtotime(now()))),
            'sexo' => 'MASCULINO',# estados posibles: MASCULINO FEMENINO INDEFINIDO
            'estado_civil' => 'SOLTERO',# estados posibles: SOLTERO CASADO
            'nivel_instruccion' => 'PROFESIONAL',# estados posibles: SIN ESTUDIO PRIMARIA SECUNDARIA TSU PROFESIONAL SUPERIOR
            'created_at' => now()
        ]);

         DB::table('partners')->insert([# socio 2 emprendimiento 3
            'nombre' => 'Rafael',
            'apellido' => 'Hernández',
            'cedula' => '16189530',
            'fecha_nac' => date('Y-m-d', strtotime("-15 year", strtotime(now()))),
            'sexo' => 'MASCULINO',# estados posibles: MASCULINO FEMENINO INDEFINIDO
            'estado_civil' => 'SOLTERO',# estados posibles: SOLTERO CASADO
            'nivel_instruccion' => 'PROFESIONAL',# estados posibles: SIN ESTUDIO PRIMARIA SECUNDARIA TSU PROFESIONAL SUPERIOR
            'created_at' => now()
        ]);

         DB::table('partners')->insert([# socio 1 emprendimiento 4
            'nombre' => 'Luis',
            'apellido' => 'Manrique',
            'cedula' => '26767289',
            'fecha_nac' => date('Y-m-d', strtotime("-15 year", strtotime(now()))),
            'sexo' => 'MASCULINO',# estados posibles: MASCULINO FEMENINO INDEFINIDO
            'estado_civil' => 'SOLTERO',# estados posibles: SOLTERO CASADO
            'nivel_instruccion' => 'PROFESIONAL',# estados posibles: SIN ESTUDIO PRIMARIA SECUNDARIA TSU PROFESIONAL SUPERIOR
            'created_at' => now()
        ]);

         DB::table('partners')->insert([# socio 1 emprendimiento 5
            'nombre' => 'Valeria',
            'apellido' => 'Fuentes',
            'cedula' => '18672980',
            'fecha_nac' => date('Y-m-d', strtotime("-15 year", strtotime(now()))),
            'sexo' => 'MASCULINO',# estados posibles: MASCULINO FEMENINO INDEFINIDO
            'estado_civil' => 'SOLTERO',# estados posibles: SOLTERO CASADO
            'nivel_instruccion' => 'PROFESIONAL',# estados posibles: SIN ESTUDIO PRIMARIA SECUNDARIA TSU PROFESIONAL SUPERIOR
            'created_at' => now()
        ]);



        DB::table('partner_request')->insert([# solicitud de socio 1
            'id_request' => '1',# id de la solicitud
            'id_partner' => '1',# id del socio
            'created_at' => now()
        ]);

        DB::table('emprendiment_partner')->insert([# emprendimiento de socio 1
            'id_emprendiment' => '1',# id del emprendimiento
            'id_partner' => '1',# id del socio
            'created_at' => now()
        ]);*/ }
}
