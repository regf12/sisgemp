<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Transaction;

class TransactionTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    /*Transaction::create([# movimiento economico 1
      'fecha'                 => '2019-06-09',
      'monto' 								=> '12121',
      'descripcion'           => 'descripcion',
      'automatica'            => false,
      'id_referencia'         => null,
      'id_user'               => '1',# id del usuario que hizo esa transaccion
      'id_concept'         		=> '1',# id del concepto
      'tipo'                  => null,
    ]);

    Transaction::create([# movimiento economico 2
      'fecha'                 => '2019-06-09',
      'monto' 								=> '12121',
      'descripcion'						=> 'descripcion',
      'automatica'            => false,
      'id_referencia'         => null,
      'id_user'               => '1',# id del usuario que hizo esa transaccion
      'id_concept'         		=> '3',# id del concepto
      'tipo'                  => null,
    ]);

    Transaction::create([# movimiento economico 3
      'fecha'                 => '2019-06-09',
      'monto' 								=> '12121',
      'descripcion'						=> 'descripcion',
      'automatica'            => false,
      'id_referencia'         => null,
      'id_user'               => '1',# id del usuario que hizo esa transaccion
      'id_concept'         		=> '5',# id del concepto
      'tipo'                  => null,
    ]);

    Transaction::create([# movimiento economico 4
      'fecha'                 => '2019-06-09',
      'monto' 								=> '12121',
      'descripcion'						=> 'descripcion',
      'automatica'            => false,
      'id_referencia'         => null,
      'id_user'               => '1',# id del usuario que hizo esa transaccion
      'id_concept'         		=> '2',# id del concepto
      'tipo'                  => null,
    ]);

    Transaction::create([# movimiento economico 5
      'fecha'                 => '2019-06-09',
      'monto' 								=> '12121',
      'descripcion'						=> 'descripcion',
      'automatica'            => false,
      'id_referencia'         => null,
      'id_user'               => '1',# id del usuario que hizo esa transaccion
      'id_concept'         		=> '3',# id del concepto
      'tipo'                  => null,
    ]);

    Transaction::create([# movimiento economico 6
      'fecha'                 => '2019-06-09',
      'monto' 								=> '12121',
      'descripcion'						=> 'descripcion',
      'automatica'            => false,
      'id_referencia'         => null,
      'id_user'               => '1',# id del usuario que hizo esa transaccion
      'id_concept'         		=> '2',# id del concepto
      'tipo'                  => null,
    ]);

    Transaction::create([# movimiento economico 7
      'fecha'                 => '2019-06-09',
      'monto' 								=> '12121',
      'descripcion'						=> 'descripcion',
      'automatica'            => false,
      'id_referencia'         => null,
      'id_user'               => '1',# id del usuario que hizo esa transaccion
      'id_concept'         		=> '4',# id del concepto
      'tipo'                  => null,
    ]);

    Transaction::create([# movimiento economico 8
      'fecha'                 => '2019-06-09',
      'monto' 								=> '12121',
      'descripcion'						=> 'descripcion',
      'automatica'            => false,
      'id_referencia'         => null,
      'id_user'               => '2',# id del usuario que hizo esa transaccion
      'id_concept'         		=> '5',# id del concepto
      'tipo'                  => null,
    ]);

    Transaction::create([# movimiento economico 9
      'fecha'                 => '2019-06-09',
      'monto' 								=> '12121',
      'descripcion'						=> 'descripcion',
      'automatica'            => false,
      'id_referencia'         => null,
      'id_user'               => '2',# id del usuario que hizo esa transaccion
      'id_concept'         		=> '6',# id del concepto
      'tipo'                  => null,
    ]);

    Transaction::create([# movimiento economico 10
      'fecha'                 => '2019-06-09',
      'monto' 								=> '12121',
      'descripcion'						=> 'descripcion',
      'automatica'            => false,
      'id_referencia'         => null,
      'id_user'               => '2',# id del usuario que hizo esa transaccion
      'id_concept'         		=> '7',# id del concepto
      'tipo'                  => null,
    ]);

    Transaction::create([# movimiento economico 11
      'fecha'                 => '2019-06-09',
      'monto' 								=> '12121',
      'descripcion'						=> 'descripcion',
      'automatica'            => false,
      'id_referencia'         => null,
      'id_user'               => '2',# id del usuario que hizo esa transaccion
      'id_concept'         		=> '8',# id del concepto
      'tipo'                  => null,
    ]);

    Transaction::create([# movimiento economico 12
      'fecha'                 => '2019-06-09',
      'monto' 								=> '12121',
      'descripcion'						=> 'descripcion',
      'automatica'            => false,
      'id_referencia'         => null,
      'id_user'               => '2',# id del usuario que hizo esa transaccion
      'id_concept'         		=> '9',# id del concepto
      'tipo'                  => null,
    ]);

    Transaction::create([# movimiento economico 13
      'fecha'                 => '2019-06-09',
      'monto' 								=> '12121',
      'descripcion'						=> 'descripcion',
      'automatica'            => false,
      'id_referencia'         => null,
      'id_user'               => '2',# id del usuario que hizo esa transaccion
      'id_concept'         		=> '10',# id del concepto
      'tipo'                  => null,
    ]);

    Transaction::create([# movimiento economico 14
      'fecha'                 => '2019-06-09',
      'monto' 								=> '12121',
      'descripcion'						=> 'descripcion',
      'automatica'            => false,
      'id_referencia'         => null,
      'id_user'               => '1',# id del usuario que hizo esa transaccion
      'id_concept'         		=> '11',# id del concepto
      'tipo'                  => null,
    ]);

    Transaction::create([# movimiento economico 15
      'fecha'                 => '2019-06-09',
      'monto' 								=> '12121',
      'descripcion'						=> 'descripcion',
      'automatica'            => false,
      'id_referencia'         => null,
      'id_user'               => '1',# id del usuario que hizo esa transaccion
      'id_concept'         		=> '12',# id del concepto
      'tipo'                  => null,
    ]);

    Transaction::create([# movimiento economico 16
      'fecha'                 => '2019-06-09',
      'monto' 								=> '12121',
      'descripcion'						=> 'descripcion',
      'automatica'            => false,
      'id_referencia'         => null,
      'id_user'               => '1',# id del usuario que hizo esa transaccion
      'id_concept'         		=> '13',# id del concepto
      'tipo'                  => null,
    ]);*/ }
}
