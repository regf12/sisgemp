<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Emprendiment;

class EmprendimentTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    Emprendiment::create([ # emprendimiento 1
      'nombre'                     => 'Dulces delicias',
      'caracteristicas'           => 'Los ingredientes con los que son realizados nuestros productos no contienen gluten, de tal forma que las personas  que  son intolerantes a esta proteína, puedan degustar de un buen dulce. Además, las decoraciones realizadas son personalizadas, de acuerdo a las necesidades y solicitudes de los clientes.',
      'descripcion'               => 'Realizamos tortas, ponqués, cupcake, suspiros, besos de coco, galletas, alfajores, desayunos, entre otros. Además, lo disponemos en hermosos arreglos para cualquier ocasión especial.',
      'fecha_constitucion'         => date('Y-m-d', strtotime("-3 year", strtotime(now()))),
      'id_places'                 => '1', # id del lugar
      'id_category'               => '1', # id de la categoria
      'id_user'                   => '3', # id del usuario
      'created_at'                => now(),
    ]);

    Emprendiment::create([ # emprendimiento 2
      'nombre'                     => 'Mi pajarita perfecta',
      'caracteristicas'           => 'Las pajaritas que se ofrecen son realizadas con materiales reciclados como corcho, madera, vidrio, plásticos, telas y demás. Con entrega a domicilio, y diseños totalmente personalizados. Los clientes pueden proporcionar el material y hacemos la mano de obra, con total exclusividad.',
      'descripcion'               => 'Creamos pajaritas con cualquier tipo de material.',
      'fecha_constitucion'         => date('Y-m-d', strtotime("-4 year", strtotime(now()))),
      'id_places'                 => '1', # id del lugar
      'id_category'               => '3', # id de la categoria
      'id_user'                   => '4', # id del usuario
      'created_at'                => now(),
    ]);

    Emprendiment::create([ # emprendimiento 3
      'nombre'                     => 'M&R dance',
      'caracteristicas'           => 'Somos Miriam y Rafael, profesores de baile que fusionamos pasos de diferentes géneros a un género en específico, por ejemplo, pasos o figuras de ballet a salsa casino y salsa en línea. Además, damos clases grupales, individuales, y a domicilio.',
      'descripcion'               => 'Dictamos clases de diferentes géneros musicales',
      'fecha_constitucion'         => date('Y-m-d', strtotime("-5 year", strtotime(now()))),
      'id_places'                 => '1', # id del lugar
      'id_category'               => '10', # id de la categoria
      'id_user'                   => '5', # id del usuario
      'created_at'                => now(),
    ]);

    Emprendiment::create([ # emprendimiento 4
      'nombre'                     => 'Tarima andante circo teatro',
      'caracteristicas'           => 'Ofrecemos la mejor diversión para tus eventos: recreación, animaciones, juegos, magias, pinta caritas, malabares, monociclos, globoflexia, obras teatrales,  mimos, zancos, escupe fuegos, tela acrobáticas, danza aérea, circo teatro, maquillaje personalizados para ocasiones especiales.',
      'descripcion'               => 'Somos recreadores de eventos y fiestas.',
      'fecha_constitucion'         => date('Y-m-d', strtotime("-6 year", strtotime(now()))),
      'id_places'                 => '1', # id del lugar
      'id_category'               => '10', # id de la categoria
      'id_user'                   => '6', # id del usuario
      'created_at'                => now(),
    ]);

    Emprendiment::create([ # emprendimiento 5
      'nombre'                    => 'Creaciones valerias desings',
      'caracteristicas'           => ': Los accesorios que se ofrecen están elaborados a mano, cada uno realizado con un grado de detalle en el diseño, elección de los materiales, y adaptados a las exigencias de los clientes. ',
      'descripcion'               => 'Bisuterías hechas a mano.',
      'fecha_constitucion'        => date('Y-m-d', strtotime("-2 year", strtotime(now()))),
      'id_places'                 => '1', # id del lugar
      'id_category'               => '3', # id de la categoria
      'id_user'                   => '7', # id del usuario
      'created_at'                => now(),
    ]);

    /*Emprendiment::create([ # emprendimiento 6
      'nombre'                    => 'Emprendimiento  6',
      'caracteristicas'           => 'Caracteristicas',
      'descripcion'               => 'Descripcion',
      'fecha_constitucion'        => date('Y-m-d', strtotime("-1 year", strtotime(now()))),
      'id_places'                 => '1', # id del lugar
      'id_category'               => '1', # id de la categoria
      'id_user'                   => '3', # id del usuario
      'created_at'                => now(),
    ]);*/
  }
}
