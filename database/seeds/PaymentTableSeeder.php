<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    /*DB::table('payments')->insert([# mensualidad 1
          'id_emprendiment'                 => 3,
          'id_group'                 => 1,
          'id_transaction'                 => 1,
      		'monto'                  => 3000,
      		'mes'               => 'ENERO',
        ]);
        DB::table('payments')->insert([# mensualidad 2
          'id_emprendiment'                 => 3,
          'id_group'                 => 1,
          'id_transaction'                 => 1,
      		'monto'                  => 4000,
      		'mes'               => 'FEBRERO',
        ]);
        DB::table('payments')->insert([# mensualidad 3
          'id_emprendiment'                 => 3,
          'id_group'                 => 1,
          'id_transaction'                 => 1,
      		'monto'                  => 5000,
      		'mes'               => 'MARZO',
        ]);
        //FIN EMPREND 1

        DB::table('payments')->insert([# mensualidad 4
          'id_emprendiment'                 => 4,
          'id_group'                 => 1,
          'id_transaction'                 => 2,
      		'monto'                  => 3000,
      		'mes'               => 'ENERO',
        ]);
        DB::table('payments')->insert([# mensualidad 5
          'id_emprendiment'                 => 4,
          'id_group'                 => 1,
          'id_transaction'                 => 2,
      		'monto'                  => 4000,
      		'mes'               => 'FEBRERO',
        ]);
        DB::table('payments')->insert([# mensualidad 6
          'id_emprendiment'                 => 4,
          'id_group'                 => 1,
          'id_transaction'                 => 2,
      		'monto'                  => 5000,
      		'mes'               => 'MARZO',
        ]);
        DB::table('payments')->insert([# mensualidad 7
          'id_emprendiment'                 => 4,
          'id_group'                 => 1,
          'id_transaction'                 => 2,
      		'monto'                  => 6000,
      		'mes'               => 'ABRIL',
        ]);
        //FIN EMPREND 2

        DB::table('payments')->insert([# mensualidad 8
          'id_emprendiment'                 => 5,
          'id_group'                 => 1,
          'id_transaction'                 => 3,
      		'monto'                  => 3000,
      		'mes'               => 'ENERO',
        ]);
        DB::table('payments')->insert([# mensualidad 9
          'id_emprendiment'                 => 5,
          'id_group'                 => 1,
          'id_transaction'                 => 3,
      		'monto'                  => 4000,
      		'mes'               => 'FEBRERO',
        ]);
        DB::table('payments')->insert([# mensualidad 10
          'id_emprendiment'                 => 5,
          'id_group'                 => 1,
          'id_transaction'                 => 3,
      		'monto'                  => 5000,
      		'mes'               => 'MARZO',
        ]);
        DB::table('payments')->insert([# mensualidad 11
          'id_emprendiment'                 => 5,
          'id_group'                 => 1,
          'id_transaction'                 => 3,
      		'monto'                  => 6000,
      		'mes'               => 'ABRIL',
        ]);
        DB::table('payments')->insert([# mensualidad 12
          'id_emprendiment'                 => 5,# id del emprendimiento
          'id_group'                 => 1,
          'id_transaction'                 => 3,# id del movimiento economico
      		'monto'                  => 7000,
      		'mes'               => 'MAYO',
        ]);
        //FIN EMPREND 3*/ }
}
