<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PollTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {


    DB::table('polls')->insert([ # encuesta 1
      'nombre'       =>     'Vota por el evento del fin de semana pasado',
      'id_user'      =>     "1",
      'created_at'   => now(),
    ]);
    DB::table('questions')->insert([ # pregunta 1 
      'nombre'       =>     '¿Qué tan bueno fue el evento?',
      'id_poll'      =>     "1", # id de la encuesta
      'created_at'   => now(),
    ]);
    DB::table('responses')->insert([ # respuesta 1
      'nombre'       =>     'Muy bueno',
      'id_question' =>     "1", # id de la pregunta
      'created_at'   => now(),
    ]);
    DB::table('responses')->insert([ # respuesta 2
      'nombre'       =>     'Bueno',
      'id_question' =>     "1", # id de la pregunta
      'created_at'   => now(),
    ]);
    DB::table('responses')->insert([ # respuesta 3
      'nombre'       =>     'Regular',
      'id_question' =>     "1", # id de la pregunta
      'created_at'   => now(),
    ]);
    DB::table('responses')->insert([ # respuesta 4
      'nombre'       =>     'Malo',
      'id_question' =>     "1", # id de la pregunta
      'created_at'   => now(),
    ]);
    DB::table('responses')->insert([ # respuesta 5
      'nombre'       =>     'Muy malo',
      'id_question' =>     "1", # id de la pregunta
      'created_at'   => now(),
    ]);



    DB::table('polls')->insert([ # encuesta 2
      'nombre'       =>     'Vota por el motivo del proximo evento',
      'id_user'      =>     "1",
      'created_at'   => now(),
    ]);
    DB::table('questions')->insert([ # pregunta 2
      'nombre'       =>     '¿Qué motivo te gustaria para nuestro proximo evento?',
      'id_poll'      =>     "2", # id de la encuesta
      'created_at'   => now(),
    ]);
    DB::table('responses')->insert([ # respuesta 6
      'nombre'       =>     'Halloween',
      'id_question' =>     "2", # id de la pregunta
      'created_at'   => now(),
    ]);
    DB::table('responses')->insert([ # respuesta 7
      'nombre'       =>     'Dia del niño',
      'id_question' =>     "2", # id de la pregunta
      'created_at'   => now(),
    ]);
    DB::table('responses')->insert([ # respuesta 8
      'nombre'       =>     'Dia de la independencia',
      'id_question' =>     "2", # id de la pregunta
      'created_at'   => now(),
    ]);
    DB::table('responses')->insert([ # respuesta 9
      'nombre'       =>     'Dia de la mujer',
      'id_question' =>     "2", # id de la pregunta
      'created_at'   => now(),
    ]);



    DB::table('polls')->insert([ # encuesta 3
      'nombre'       =>     'Elige el lugar de nuestro próximo evento',
      'id_user'      =>     "1",
      'created_at'   => now(),
    ]);
    DB::table('questions')->insert([ # pregunta 3
      'nombre'       =>     '¿Dónde quisieras que celebremos nuestro próximo evento?',
      'id_poll'      =>     "3", # id de la encuesta
      'created_at'   => now(),
    ]);
    DB::table('responses')->insert([ # respuesta 10
      'nombre'       =>     'C.C. Cumaná plaza ',
      'id_question' =>     "3", # id de la pregunta
      'created_at'   => now(),
    ]);
    DB::table('responses')->insert([ # respuesta 11
      'nombre'       =>     'C.C. Marina plaza',
      'id_question' =>     "3", # id de la pregunta
      'created_at'   => now(),
    ]);
    DB::table('responses')->insert([ # respuesta 12
      'nombre'       =>     'C.C. Hiper galerias',
      'id_question' =>     "3", # id de la pregunta
      'created_at'   => now(),
    ]);




    #encuesta 1

    # usuarios que respondieron encuestas

    DB::table('poll_user')->insert([ #
      'id_user'      =>    '5', # id del usuario
      'id_poll'      =>    "1", # id de la encuesta
    ]);
    DB::table('poll_user')->insert([ #
      'id_user'      =>    '6', # id del usuario
      'id_poll'      =>    "1", # id de la encuesta
    ]);


    DB::table('poll_user')->insert([ # 
      'id_user'      =>    '5', # id del usuario
      'id_poll'      =>    "2", # id de la encuesta
    ]);
    DB::table('poll_user')->insert([ # 
      'id_user'      =>    '6', # id del usuario
      'id_poll'      =>    "2", # id de la encuesta
    ]);


    DB::table('poll_user')->insert([ # 
      'id_user'      =>    '5', # id del usuario
      'id_poll'      =>    "3", # id de la encuesta
    ]);
    DB::table('poll_user')->insert([ # 
      'id_user'      =>    '6', # id del usuario
      'id_poll'      =>    "3", # id de la encuesta
    ]);

    # respuestas de los usuarios

    DB::table('response_user')->insert([ #
      'id_poll_user'      =>    '5', # id del usuario
      'id_response'      =>    "1", # id de la respuesta
    ]);
    DB::table('response_user')->insert([ # 
      'id_poll_user'      =>    '6', # id del usuario
      'id_response'      =>    "2", # id de la respuesta
    ]);




    DB::table('response_user')->insert([ # 
      'id_poll_user'      =>    '5', # id del usuario
      'id_response'      =>    "6", # id de la respuesta
    ]);
    DB::table('response_user')->insert([ # 
      'id_poll_user'      =>    '6', # id del usuario
      'id_response'      =>    "6", # id de la respuesta
    ]);




    DB::table('response_user')->insert([ # 
      'id_poll_user'      =>    '5', # id del usuario
      'id_response'      =>    "10", # id de la respuesta
    ]);
    DB::table('response_user')->insert([ # 
      'id_poll_user'      =>    '6', # id del usuario
      'id_response'      =>    "12", # id de la respuesta
    ]);
  }
}
