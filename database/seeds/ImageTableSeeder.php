<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ImageTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    ///////////////////////////////////////////////////// imagenes de perfil

    DB::table('images')->insert([
      'url'                 => '', # el nombre de la imagen
      'tipo'                => 'GROUPS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => '', # el nombre de la imagen
      'tipo'                => 'EMPRENDIMENTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => '', # el nombre de la imagen
      'tipo'                => 'EMPRENDIMENTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '2', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => '', # el nombre de la imagen
      'tipo'                => 'EMPRENDIMENTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '3', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => '', # el nombre de la imagen
      'tipo'                => 'EMPRENDIMENTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '4', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => '', # el nombre de la imagen
      'tipo'                => 'EMPRENDIMENTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '5', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => '', # el nombre de la imagen
      'tipo'                => 'EMPRENDIMENTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '6', # id del registro que se referencia
    ]);

    ///////////////////////////////////////////////////// imagenes de banner

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'BANNERGROUP', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'BANNER', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'BANNER', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '2', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'BANNER', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '3', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'BANNER', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '4', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'BANNER', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '5', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'BANNER', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '6', # id del registro que se referencia
    ]);


    ///////////////////////////////////////////////////// imagenes de galleria


    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'GALLERY', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'GALLERY', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'GALLERY', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'GALLERY', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'GALLERY', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'GALLERY', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'GALLERY', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'GALLERY', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'GALLERY', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'GALLERY', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'GALLERY', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/imagen-no.png', # el nombre de la imagen
      'tipo'                => 'GALLERY', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);


    ///////////////////////////////////////////////////// imagenes de eventos



    DB::table('images')->insert([
      'url'                 => 'images/event-no-img.png', # el nombre de la imagen
      'tipo'                => 'EVENTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/event-no-img.png', # el nombre de la imagen
      'tipo'                => 'EVENTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '2', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/event-no-img.png', # el nombre de la imagen
      'tipo'                => 'EVENTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '3', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/event-no-img.png', # el nombre de la imagen
      'tipo'                => 'EVENTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '4', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/event-no-img.png', # el nombre de la imagen
      'tipo'                => 'EVENTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '5', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/event-no-img.png', # el nombre de la imagen
      'tipo'                => 'EVENTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '6', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/event-no-img.png', # el nombre de la imagen
      'tipo'                => 'EVENTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '7', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/event-no-img.png', # el nombre de la imagen
      'tipo'                => 'EVENTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '8', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/event-no-img.png', # el nombre de la imagen
      'tipo'                => 'EVENTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '9', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/event-no-img.png', # el nombre de la imagen
      'tipo'                => 'EVENTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '10', # id del registro que se referencia
    ]);



    ///////////////////////////////////////////////////// imagenes de productos


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '1', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '2', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '2', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '2', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '2', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '3', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '3', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '3', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '3', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '4', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '4', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '4', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '4', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '5', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '5', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '5', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '5', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '6', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '6', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '6', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '6', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '7', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '7', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '7', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '7', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '8', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '8', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '8', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '8', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '9', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '9', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '9', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '9', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '10', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '10', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '10', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '10', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '11', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '11', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '11', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '11', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '12', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '12', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '12', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '12', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '13', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '13', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '13', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '13', # id del registro que se referencia
    ]);

    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '14', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '15', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '16', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '17', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '18', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '19', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '20', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '21', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '22', # id del registro que se referencia
    ]);


    DB::table('images')->insert([
      'url'                 => 'images/product-no-img.png', # el nombre de la imagen
      'tipo'                => 'PRODUCTS', # estados posibles: REQUESTS EVENTS EMPRENDIMENTS PRODUCTS GROUPS
      'id_referencia'       => '23', # id del registro que se referencia
    ]);
  }
}
