<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

use App\User;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    User::create([
      'name'      => 'super',
      'email'     => 'super@mail.com',
      'status'    => 'ACTIVO',
      'password'  => bcrypt('123456')
    ])->assignRole('super');

    User::create([
      'name'      => 'admin',
      'email'     => 'admin@mail.com',
      'status'    => 'ACTIVO',
      'password'  => bcrypt('123456')
    ])->assignRole('admin');

    /*User::create([
      'name'      => 'user',
      'email'     => 'user@mail.com',
      'status'    => 'ACTIVO',
      'password'  => bcrypt('123456')
    ])->assignRole('user');*/

    ///////////////////////////////////////////

    User::create([ # emprendimiento 1
      'name'      => 'Dulces delicias', # nombre del emprendimiento
      'email'     => 'dulcesdelicias@gmail.com',
      'status'    => 'ACTIVO',
      'password'  => bcrypt('123456')
    ])->assignRole('emprend');

    User::create([ # emprendimiento 2
      'name'      => 'Mi pajarita perfecta', # nombre del emprendimiento
      'email'     => 'pajaritaperfecta@gmail.com',
      'status'    => 'ACTIVO',
      'password'  => bcrypt('123456')
    ])->assignRole('emprend');

    User::create([ # emprendimiento 3
      'name'      => 'M&R dance', # nombre del emprendimiento
      'email'     => 'danceMR@gmail.com',
      'status'    => 'ACTIVO',
      'password'  => bcrypt('123456')
    ])->assignRole('emprend');

    User::create([ # emprendimiento 4
      'name'      => 'Tarima andante circo teatro', # nombre del emprendimiento
      'email'     => 'tariamandante@gmail.com',
      'status'    => 'ACTIVO',
      'password'  => bcrypt('123456')
    ])->assignRole('emprend');

    User::create([ # emprendimiento 5
      'name'      => 'Creaciones valerias desings', # nombre del emprendimiento
      'email'     => 'valerydesings@gmail.com',
      'status'    => 'ACTIVO',
      'password'  => bcrypt('123456')
    ])->assignRole('emprend');

    /*User::create([ # emprendimiento 6
      'name'      => 'banned', # nombre del emprendimiento
      'email'     => 'banned@mail.com',
      'status'    => 'ACTIVO',
      'password'  => bcrypt('123456')
    ])->assignRole('banned');*/
  }
}
