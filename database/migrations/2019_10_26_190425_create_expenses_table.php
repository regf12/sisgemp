<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('tipo', ['LOGISTICA', 'FESTEJO', 'LOCAL', 'SONIDO', 'OTROS'])->default('LOGISTICA');
            $table->decimal('monto', 15, 2);
            $table->integer('id_transaction')->unsigned();
            $table->integer('id_event')->unsigned();
            $table->timestamps();

            $table->foreign('id_transaction')
                ->references('id')->on('transactions')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_event')
                ->references('id')->on('events')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
