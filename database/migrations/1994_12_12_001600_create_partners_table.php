<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->bigIncrements('id')->unique();
            /////////////////////////////////////////////
            $table->string('nombre');

            $table->string('apellido');

            $table->string('cedula')/*->unsigned()*/;

            $table->date('fecha_nac');

            $table->enum('sexo', ['MASCULINO', 'FEMENINO', 'INDEFINIDO'])->default('INDEFINIDO');

            $table->enum('estado_civil', ['SOLTERO', 'CASADO'])->default('SOLTERO');

            $table->enum('nivel_instruccion', ['SIN ESTUDIO', 'PRIMARIA', 'SECUNDARIA', 'TSU', 'PROFESIONAL', 'SUPERIOR'])->default('SIN ESTUDIO');
            /////////////////////////////////////////////
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }
}
