<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            /////////////////////////////////////////////
            $table->date('fecha');

            $table->decimal('monto', 15, 2);

            $table->string('descripcion', 1000);

            $table->boolean('automatica')->default(false);

            $table->integer('id_referencia')->unsigned()->nullable();
            $table->integer('id_concept')->unsigned();
            $table->integer('id_user')->unsigned();
            $table->enum('tipo', ['EVENTS', 'REQUESTS', ""])->nullable()->default("");
            /////////////////////////////////////////////
            $table->timestamps();

            /*
            $table->foreign('id_type_transaction')
                ->references('id')->on('type_operations')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('id_referencia')
                ->references('id')->on('concepts')
                ->onDelete('cascade')->onUpdate('cascade');
            */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
