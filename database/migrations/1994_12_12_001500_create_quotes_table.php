<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->bigIncrements('id');
            /////////////////////////////////////////////
            $table->date('fecha');
            $table->time('hora');
            $table->string('comentario', 1000)->nullable();

            $table->integer('id_lugar')->unsigned();
            $table->integer('id_request')->unsigned();
            /////////////////////////////////////////////
            $table->timestamps();

            /*$table->foreign('id_request')
                ->references('id')->on('requests')
                ->onDelete('cascade')->onUpdate('cascade');
*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
