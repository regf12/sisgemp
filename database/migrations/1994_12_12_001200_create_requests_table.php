<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            /////////////////////////////////////////////
            $table->string('nombre', 128);
            $table->string('correo', 128);
            $table->string('telefono', 128);
            $table->string('caracteristicas', 1000)->nullable();
            $table->string('descripcion', 1000);

            $table->date('fecha_constitucion');

            $table->integer('id_lugar')->unsigned()->nullable();
            $table->integer('id_category')->unsigned();

            $table->enum('estado', ['PENDIENTE', 'RECHAZADA', 'APROBADA'])->default('PENDIENTE');
            /////////////////////////////////////////////
            $table->timestamps();

            /*
            $table->foreign('id_category')
                ->references('id')->on('categories')
                ->onDelete('cascade')->onUpdate('cascade');
            */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
