<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            /////////////////////////////////////////////
            $table->boolean('visto')->default(false);

            $table->enum('tipo', ['REQUESTS', 'EVENTS', 'EMPRENDIMENTS', 'PRODUCTS', 'POLLS'])->default('REQUESTS');

            $table->integer('id_referencia')->unsigned();
            $table->integer('id_user')->unsigned();
            /////////////////////////////////////////////
            $table->timestamps();

            $table->foreign('id_user')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
