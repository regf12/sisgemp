<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_request', function (Blueprint $table) {
            $table->bigIncrements('id');
            /////////////////////////////////////////////
            $table->integer('id_request')->unsigned();

            $table->integer('id_partner')->unsigned();
            /////////////////////////////////////////////
            $table->timestamps();

            #$table->foreign('id_request')->references('id')->on('requests')
            #    ->onDelete('cascade')
            #    ->onUpdate('cascade');

            #$table->foreign('id_partner')->references('id')->on('partners')
            #    ->onDelete('cascade')
            #    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_request');
    }
}
