<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            ////////////////////////////////////////////
            $table->enum('mes', ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'])->default('ENERO');
            /*$table->enum('mes',['JANUARY','FEBRUARY','MARCH','APRIL','MAY','JUNE','JULY','AUGUST','SEPTEMBER','OCTOBER','NOVEMBER','DECEMBER'])->default('JANUARY');*/

            $table->decimal('monto', 15, 2);

            $table->integer('id_emprendiment')->unsigned();
            $table->integer('id_group')->unsigned();
            $table->integer('id_transaction')->unsigned();
            /////////////////////////////////////////////
            $table->timestamps();

            $table->foreign('id_emprendiment')
                ->references('id')->on('emprendiments')
                ->onDelete('cascade')->onUpdate('cascade');
            /*
            $table->foreign('id_grupo')
                ->references('id')->on('groups')
                ->onDelete('cascade')->onUpdate('cascade');
            */
            $table->foreign('id_transaction')
                ->references('id')->on('transactions')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
