<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->bigIncrements('id');
            ////////////////////////////////////////////
            $table->string('url', 1000);

            $table->enum('tipo', ['REQUESTS', 'EVENTS', 'EMPRENDIMENTS', 'PRODUCTS', 'GROUPS', 'BANNER', 'BANNERGROUP', 'CONTENT', 'GALLERY', 'TRANSACTIONS'])->default('REQUESTS');

            $table->integer('id_referencia')->unsigned();
            ////////////////////////////////////////////
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
