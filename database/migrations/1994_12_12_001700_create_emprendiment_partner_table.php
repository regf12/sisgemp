<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmprendimentPartnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emprendiment_partner', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            /////////////////////////////////////////////
            $table->integer('id_emprendiment')->unsigned();

            $table->integer('id_partner')->unsigned();
            /////////////////////////////////////////////
            $table->timestamps();

            $table->foreign('id_emprendiment')->references('id')->on('emprendiments')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_partner')->references('id')->on('partners')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emprendiment_partner');
    }
}
