<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            /////////////////////////////////////////////
            $table->string('nombre', 128);
            $table->string('descripcion', 1000);

            $table->decimal('precio', 15, 2);

            $table->integer('id_emprendiment')->unsigned();
            $table->enum('estado', ['APPROVED', 'PENDING', 'REJECTED'])->default('PENDING');
            /////////////////////////////////////////////
            $table->timestamps();

            $table->foreign('id_emprendiment')
                ->references('id')->on('emprendiments')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
