<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmprendimentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emprendiments', function (Blueprint $table) {
            $table->bigIncrements('id');
            /////////////////////////////////////////////
            $table->string('nombre', 128)->unique();
            $table->string('caracteristicas', 1000)->nullable();
            $table->string('descripcion', 1000);

            $table->date('fecha_constitucion')->default(now());

            $table->integer('id_places')->unsigned()->nullable();
            $table->integer('id_category')->unsigned();
            $table->integer('id_user')->unsigned();
            /////////////////////////////////////////////
            $table->timestamps();

            /*
            $table->foreign('id_category')
                ->references('id')->on('categories')
                ->onDelete('cascade')->onUpdate('cascade');
            */
            $table->foreign('id_user')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emprendiments');
    }
}
