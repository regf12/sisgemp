<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            /////////////////////////////////////////////
            $table->string('nombre', 128);
            $table->string('motivo', 1000);

            $table->decimal('costo', 15, 2);
            $table->decimal('gastos', 15, 2)->nullable();
            $table->decimal('entradas', 15, 2)->nullable();
            /*$table->decimal('gastos_imp', 15,2)->nullable();*/

            $table->integer('id_lugar')->unsigned()->nullable();
            $table->integer('cupos');

            $table->date('fecha');
            $table->time('hora_inicio');
            $table->time('hora_fin')->nullable();

            $table->enum('estado', ['ACTIVO', 'CANCELADO', 'REALIZADO'])->default('ACTIVO');
            $table->enum('tipo', ['INTERNO', 'EXTERNO'])->default('INTERNO');
            /////////////////////////////////////////////
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
