const path = require('path');
const fs = require('fs-extra');
const mix = require('laravel-mix');
require('laravel-mix-versionhash');

//const VueLoaderPlugin = require('vue-loader/lib/plugin');
//const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

mix
  .js('resources/js/app.js', 'public/dist/js')
  .sass('resources/sass/app.scss', 'public/dist/css')
  .copy('resources/images', 'public/images', false)
  .copy('resources/sass/fonts', 'public/dist/css/fonts', false)

  //.disableNotifications()
  //.sass('src', 'destination', { outputStyle: 'expanded' })

  .options({
    processCssUrls: false
  });

if (mix.inProduction()) {
  mix
    // .extract() // Disabled until resolved: https://github.com/JeffreyWay/laravel-mix/issues/1889
    // .version() // Use `laravel-mix-versionhash` for the generating correct Laravel Mix manifest file.
    .versionHash();
} else {
  mix.sourceMaps();
}

mix.webpackConfig({
  plugins: [
    //new BundleAnalyzerPlugin(),
    //new VueLoaderPlugin()
  ],

  module: {
    /*rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules(?!\/foundation-sites)|bower_components/,
        use: [
          {
            loader: 'babel-loader',
            options: Config.babel()
          }
        ]
      }
    ]*/
    /*loaders: [
      { 
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            scss: 'vue-style-loader!css-loader!sass-loader',
            sass: 'vue-style-loader!css-loader!sass-loader?indentedSyntax',
          }
        }
      }
    ]*/
  },

  resolve: {
    extensions: ['.css', '.scss', '.js', '.json', '.vue'],
    alias: {
      '~': path.join(__dirname, './resources/js'), 
      '#': path.join(__dirname, './resources') 
    }
  },

  output: {
    chunkFilename: 'dist/js/[chunkhash].js',
    path: mix.config.hmr ? '/' : path.resolve(__dirname, './public/build')
  }
});

mix.then(() => {
  if (!mix.config.hmr) {
    process.nextTick(() => publishAseets());
  }
});

function publishAseets () {
  const publicDir = path.resolve(__dirname, './public');

  if (mix.inProduction()) {
    fs.removeSync(path.join(publicDir, 'dist'));
  }

  fs.copySync(path.join(publicDir, 'build', 'dist'), path.join(publicDir, 'dist'));
  fs.removeSync(path.join(publicDir, 'build'));
}
